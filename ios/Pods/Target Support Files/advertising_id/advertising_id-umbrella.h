#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "AdvertisingIdPlugin.h"

FOUNDATION_EXPORT double advertising_idVersionNumber;
FOUNDATION_EXPORT const unsigned char advertising_idVersionString[];

