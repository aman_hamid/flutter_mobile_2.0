// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dashboard_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DashboardResponse _$DashboardResponseFromJson(Map<String, dynamic> json) {
  return DashboardResponse(
    dashboardItemList: (json['sectionDTOList'] as List<dynamic>?)
        ?.map((e) => DashboardItemList.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$DashboardResponseToJson(DashboardResponse instance) =>
    <String, dynamic>{
      'sectionDTOList': instance.dashboardItemList,
    };
