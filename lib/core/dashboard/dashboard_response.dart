import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/dashboard/dashboard_item_list.dart';

part 'dashboard_response.g.dart';

@JsonSerializable()
class DashboardResponse {
  @JsonKey(name: 'sectionDTOList')
  List<DashboardItemList>? dashboardItemList;

  DashboardResponse({this.dashboardItemList});

  factory DashboardResponse.fromJson(Map<String, dynamic> json) => _$DashboardResponseFromJson(json);

  Map<String, dynamic> toJson() => _$DashboardResponseToJson(this);
}
