import 'dart:convert';

import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/user_patient_response.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/locator.dart';

class UserService {
  final HttpApi _api = locator<HttpApi>();
  final DataStoreService dataStore = locator<DataStoreService>();

  final Base64Codec base64 = Base64Codec();

  Future<List<UserPatient>?> getUserPatientList() async {
    if (dataStore.getUserId() == null) {
      return [];
    }
    var response = await _api.getUserPatientList(dataStore.getUserId()!);
    if (response != null) {
      try {
        BaseResponse<UserPatientResponse> userPatientResponse = BaseResponse<UserPatientResponse>.fromJson(response.data);
        if (userPatientResponse.response != null) {
          return userPatientResponse.response!.userPatientList;
        } else {
          throw Exception("failed to load user patient list");
        }
      } catch (ex) {
        FirebaseCrashlytics.instance.log(ex.toString());
      }
    } else {
      throw Exception("failed to load user patient list");
    }
  }
}
