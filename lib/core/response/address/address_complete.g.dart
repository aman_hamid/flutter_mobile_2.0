// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'address_complete.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddressComplete _$AddressCompleteFromJson(Map<String, dynamic> json) {
  return AddressComplete(
    json['province'] as String?,
    json['postalCode'] as String?,
    json['streetAddress'] as String?,
    json['streetAddressLineTwo'] as String?,
    json['city'] as String?,
    json['country'] as String?,
  );
}

Map<String, dynamic> _$AddressCompleteToJson(AddressComplete instance) =>
    <String, dynamic>{
      'province': instance.province,
      'postalCode': instance.postalCode,
      'streetAddress': instance.streetAddress,
      'streetAddressLineTwo': instance.streetAddressLineTwo,
      'city': instance.city,
      'country': instance.country,
    };
