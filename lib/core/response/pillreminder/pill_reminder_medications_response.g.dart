// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pill_reminder_medications_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PillReminderMedicationResponse _$PillReminderMedicationResponseFromJson(
    Map<String, dynamic> json) {
  return PillReminderMedicationResponse(
    (json['pocketPackDetails'] as List<dynamic>)
        .map((e) => PocketPackDetails.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$PillReminderMedicationResponseToJson(
        PillReminderMedicationResponse instance) =>
    <String, dynamic>{
      'pocketPackDetails': instance.pocketPackDetails,
    };
