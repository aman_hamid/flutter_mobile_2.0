import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/patient.dart';
part 'activate_patient_response.g.dart';

@JsonSerializable()
class ActivatePatientResponse {
  @JsonKey(name: 'addedAddress')
  bool addedAddress;

  @JsonKey(name: 'addedInsurance')
  bool addedInsurance;
  @JsonKey(name: 'addedDependents')
  bool addedDependents;
  @JsonKey(name: 'createdUser')
  bool createdUser;

  @JsonKey(name: 'insertedPatient')
  Patient patient;

  ActivatePatientResponse(this.addedAddress, this.patient, this.addedDependents, this.addedInsurance, this.createdUser);

  factory ActivatePatientResponse.fromJson(Map<String, dynamic> json) => _$ActivatePatientResponseFromJson(json);
}
