import 'package:json_annotation/json_annotation.dart';

part 'signup_phonenumber_response.g.dart';

@JsonSerializable()
class SignupPhoneNumberResponse {
  int userId;

  SignupPhoneNumberResponse({required this.userId});

  factory SignupPhoneNumberResponse.fromJson(Map<String, dynamic> json) => _$SignupPhoneNumberResponseFromJson(json);

  Map<String, dynamic> toJson() => _$SignupPhoneNumberResponseToJson(this);
}
