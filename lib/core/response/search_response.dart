import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/medicine.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

part 'search_response.g.dart';

@JsonSerializable()
class SearchResponse {
  @JsonKey(name: 'success')
  bool status;

  @JsonKey(name: 'userMessage')
  String errMessage;

  @JsonKey(name: 'message')
  String apiMessage;

  @JsonKey(name: 'medicines')
  List<Medicine> medicines;

  SearchResponse({required this.status, required this.errMessage, required this.apiMessage, required this.medicines});

  String getErrorMessage() {
    if (errMessage != null && errMessage != "")
      return errMessage;
    else
      return LocalizationUtils.getSingleValueString("common", "common.label.api-error");
  }

  factory SearchResponse.fromJson(Map<String, dynamic> json) => _$SearchResponseFromJson(json);

  Map<String, dynamic> toJson() => _$SearchResponseToJson(this);
}
