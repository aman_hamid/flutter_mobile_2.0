// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'postal_code.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostalCode _$PostalCodeFromJson(Map<String, dynamic> json) {
  return PostalCode(
    json['code'] as String?,
  );
}

Map<String, dynamic> _$PostalCodeToJson(PostalCode instance) =>
    <String, dynamic>{
      'code': instance.code,
    };
