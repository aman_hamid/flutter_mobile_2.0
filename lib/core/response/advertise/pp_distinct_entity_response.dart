import 'package:json_annotation/json_annotation.dart';
part 'pp_distinct_entity_response.g.dart';

@JsonSerializable()
class PPDistinctEntityResponse {
  String id;

  PPDistinctEntityResponse(this.id);

  factory PPDistinctEntityResponse.fromJson(Map<String, dynamic> json) => _$PPDistinctEntityResponseFromJson(json);
}
