import 'dart:core';

import 'package:json_annotation/json_annotation.dart';

part 'localization_update_response.g.dart';

@JsonSerializable()
class LocalizationUpdateResponse {
  String? moduleKey;
  String? updatedDateTimeInUTC;

  LocalizationUpdateResponse(this.moduleKey, this.updatedDateTimeInUTC);

  factory LocalizationUpdateResponse.fromJson(Map<String, dynamic> json) => _$LocalizationUpdateResponseFromJson(json);
  Map<String, dynamic> toJson() => _$LocalizationUpdateResponseToJson(this);
}
