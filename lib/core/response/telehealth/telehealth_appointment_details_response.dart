import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/utils/date_utils.dart';

import 'doctor_details_response.dart';

part 'telehealth_appointment_details_response.g.dart';

@JsonSerializable()
class TelehealthAppointmentResponse {
  String? prescriptionRequestReason;
  bool? prescriptionFilledByExternalPharmacy;
  String? prescriptionRequestCategory;
  @JsonKey(fromJson: PPDateUtils.fromStr, toJson: PPDateUtils.toStr)
  DateTime? appointmentTime;
  String? telehealthRequestedMedications;
  String? prescriptionMedicalConditions;
  String? prescriptionComment;
  String? telehealthClinic;
  DoctorResponse? doctor;

  factory TelehealthAppointmentResponse.fromJson(Map<String, dynamic> json) => _$TelehealthAppointmentResponseFromJson(json);

  Map<String, dynamic> toJson() => _$TelehealthAppointmentResponseToJson(this);
  TelehealthAppointmentResponse(this.prescriptionRequestReason, this.prescriptionRequestCategory, this.appointmentTime, this.prescriptionMedicalConditions,
      this.prescriptionComment, this.telehealthClinic, this.telehealthRequestedMedications, this.prescriptionFilledByExternalPharmacy, this.doctor);
}
