import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/patient.dart';
part 'medicalCondition_suggestion.g.dart';

@JsonSerializable()
class MedicalConditionSuggestion {
  @JsonKey(name: 'symptom')
  String symptom;

  @JsonKey(name: 'restrictedToPrescribe')
  bool restrictedToPrescribe;

  MedicalConditionSuggestion(
    this.symptom,
    this.restrictedToPrescribe,
  );

  factory MedicalConditionSuggestion.fromJson(Map<String, dynamic> json) => _$MedicalConditionSuggestionFromJson(json);
}
