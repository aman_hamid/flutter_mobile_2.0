import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/insurance.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_appointment_details_response.dart';

part 'appointment_first_responce.g.dart';

@JsonSerializable()
class AppointmentFirstResponse {
  TelehealthAppointmentResponse? prescriptionDTO;
  String? timeSlot;
  String? slotDate;

  AppointmentFirstResponse({required this.prescriptionDTO, required this.timeSlot, required this.slotDate});

  factory AppointmentFirstResponse.fromJson(Map<String, dynamic> json) => _$AppointmentFirstResponseFromJson(json);

  Map<String, dynamic> toJson() => _$AppointmentFirstResponseToJson(this);
}
