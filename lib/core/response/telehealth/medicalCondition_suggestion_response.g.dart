// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'medicalCondition_suggestion_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MedicalConditionSuggestionResponse _$MedicalConditionSuggestionResponseFromJson(
    Map<String, dynamic> json) {
  return MedicalConditionSuggestionResponse(
    (json['items'] as List<dynamic>)
        .map((e) =>
            MedicalConditionSuggestion.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$MedicalConditionSuggestionResponseToJson(
        MedicalConditionSuggestionResponse instance) =>
    <String, dynamic>{
      'items': instance.items,
    };
