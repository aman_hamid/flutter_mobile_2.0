import 'dart:core';

import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/response/prescription/prescription.dart';
import 'package:pocketpills/core/models/transfer_prescription.dart';
import 'package:pocketpills/core/response/prescription/success_details.dart';

part 'prescription_create_response.g.dart';

@JsonSerializable()
class PrescriptionCreateResponse {
  int omsPrescriptionId;
  SuccessDetails successDetails;
  double? revenue;
  int? totalPrescriptionsCount;

  PrescriptionCreateResponse(this.omsPrescriptionId, this.successDetails, this.revenue, this.totalPrescriptionsCount);

  factory PrescriptionCreateResponse.fromJson(Map<String, dynamic> json) => _$PrescriptionCreateResponseFromJson(json);
}
