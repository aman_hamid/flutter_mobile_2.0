import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

part 'old_base_response.g.dart';

@JsonSerializable()
class OldBaseResponse {
  @JsonKey(name: 'success')
  bool? status;

  @JsonKey(name: 'userMessage')
  String? errMessage;

  @JsonKey(name: 'message')
  String? apiMessage;

  OldBaseResponse({this.status, this.errMessage, this.apiMessage});

  String getErrorMessage() {
    if (errMessage != null && errMessage != "")
      return errMessage!;
    else
      return LocalizationUtils.getSingleValueString("common", "common.label.api-error");
  }

  factory OldBaseResponse.fromJson(Map<String, dynamic> json) => _$OldBaseResponseFromJson(json);
}
