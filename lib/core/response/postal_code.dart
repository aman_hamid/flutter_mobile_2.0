import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/transfer/location.dart';
import 'package:pocketpills/core/models/transfer/subdivisions.dart';
part 'postal_code.g.dart';

@JsonSerializable()
class PostalCode {
  String? code;

  PostalCode(this.code);
  factory PostalCode.fromJson(Map<String, dynamic> json) => _$PostalCodeFromJson(json);
}
