import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/order.dart';
part 'order_list_response.g.dart';

@JsonSerializable()
class OrderListResponse {
  @JsonKey(name: "total")
  int? totalOrders;

  @JsonKey(name: "data")
  List<Order>? orderList;

  OrderListResponse({this.totalOrders, this.orderList});

  factory OrderListResponse.fromJson(Map<String, dynamic> json) => _$OrderListResponseFromJson(json);

  Map<dynamic, dynamic> toJson() => _$OrderListResponseToJson(this);
}
