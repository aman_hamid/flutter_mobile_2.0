import 'dart:core';
import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/response/signup/transaction_success_field.dart';

part 'success_details.g.dart';

@JsonSerializable()
class SuccessDetails {
  String? title;
  String? description;
  String? image;
  String? helpText;
  List<TransactionSuccessField>? fields;

  SuccessDetails(
      this.title, this.description, this.image, this.helpText, this.fields);

  factory SuccessDetails.fromJson(Map<String, dynamic> json) =>
      _$SuccessDetailsFromJson(json);
}
