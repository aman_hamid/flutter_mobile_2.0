import 'package:json_annotation/json_annotation.dart';
part 'postal_address_response.g.dart';

@JsonSerializable()
class PostalAddressResponse {
  PostalAddressResponse({this.label, this.street, this.city, this.postcode, this.region, this.country});

  String? label, street, city, postcode, region, country;

  Map<dynamic, dynamic> toJson() => <dynamic, dynamic>{
        'label': this.label,
        'street': this.street,
        'city': this.city,
        'postcode': this.postcode,
        'region': this.region,
        'country': this.country,
      };

  factory PostalAddressResponse.fromJson(Map<String, dynamic> json) => _$PostalAddressResponseFromJson(json);
}
