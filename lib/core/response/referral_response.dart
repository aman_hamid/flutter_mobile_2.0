import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';

part 'referral_response.g.dart';

@JsonSerializable()
class ReferralResponse extends BaseRequest {
  int? id;
  bool? disabled;
  int? userId;
  String? appliedReferralCode;
  String? referralCode;

  ReferralResponse(this.id, this.disabled, this.userId, this.appliedReferralCode, this.referralCode);

  factory ReferralResponse.fromJson(Map<String, dynamic> json) => _$ReferralResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ReferralResponseToJson(this);
}
