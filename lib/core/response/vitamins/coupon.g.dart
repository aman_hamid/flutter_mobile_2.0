// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'coupon.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Coupon _$CouponFromJson(Map<String, dynamic> json) {
  return Coupon(
    json['mappedCouponCode'] as String?,
    json['title'] as String?,
    json['description'] as String?,
    json['active'] as bool?,
  );
}

Map<String, dynamic> _$CouponToJson(Coupon instance) => <String, dynamic>{
      'mappedCouponCode': instance.mappedCouponCode,
      'title': instance.title,
      'description': instance.description,
      'active': instance.active,
    };
