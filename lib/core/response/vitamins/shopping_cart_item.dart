import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/medicine.dart';
part 'shopping_cart_item.g.dart';

@JsonSerializable()
class ShoppingCartItem {
  int? patientId;
  bool? disabled;
  int? variantId;
  int? monthsSupply;
  num? quantity;
  String? userSigCode;
  num? totalPrice;
  num? listPrice;
  Medicine? drugDTO;

  ShoppingCartItem({this.patientId, this.disabled, this.variantId, this.monthsSupply, this.quantity, this.userSigCode, this.totalPrice, this.listPrice});

  factory ShoppingCartItem.fromJson(Map<String, dynamic> json) => _$ShoppingCartItemFromJson(json);
}
