import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/response/prescription/prescription.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

part 'oms_prescription_response.g.dart';

@JsonSerializable()
class OMSPrescriptionResponse {
  @JsonKey(name: 'success')
  bool? status;

  @JsonKey(name: 'userMessage')
  String? errMessage;

  @JsonKey(name: 'message')
  String? apiMessage;

  @JsonKey(name: 'error')
  String? error;

  @JsonKey(name: 'data')
  final List<Prescription>? response;

  OMSPrescriptionResponse({this.status, this.errMessage, this.apiMessage, this.response});

  String getErrorMessage() {
    if (errMessage != null && errMessage != "")
      return errMessage!;
    else
      return LocalizationUtils.getSingleValueString("common", "common.label.api-error");
  }

  factory OMSPrescriptionResponse.fromJson(Map<String, dynamic> json) => _$OMSPrescriptionResponseFromJson(json);

  Map<String, dynamic> toJson() => _$OMSPrescriptionResponseToJson(this);
}
