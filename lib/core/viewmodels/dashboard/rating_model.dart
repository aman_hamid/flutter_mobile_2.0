import 'package:pocketpills/core/viewmodels/base_model.dart';

class RatingModel extends BaseModel {
  double _rating = 0;

  double get rating => _rating;

  set rating(double value) {
    _rating = value;
    notifyListeners();
  }
}
