import 'dart:convert';
import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/dashboard/dashboard_item.dart';
import 'package:pocketpills/core/dashboard/dashboard_response.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/medicine.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/request/referral/send_invitation_sms_request.dart';
import 'package:pocketpills/core/request/request_wrapper.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/medicine_response.dart';
import 'package:pocketpills/core/response/old_base_response.dart';
import 'package:pocketpills/core/response/oms_response.dart';
import 'package:pocketpills/core/response/referral_response.dart';
import 'package:pocketpills/core/response/search_response.dart';
import 'package:pocketpills/core/response/telehealth/appointment_first_responce.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/pillreminder/pill_reminder_day_model.dart';
import 'package:pocketpills/core/viewmodels/vitamins/vitamins_catalog_model.dart';
import 'package:pocketpills/core/viewmodels/vitamins/vitamins_subscription_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/appbars/dashboard_appbar.dart';
import 'package:pocketpills/ui/shared/chat_view.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/views/addmember/add_member_signup.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/home/call_chat_bottom_sheet.dart';
import 'package:pocketpills/ui/views/home/dashboard_appointment_detail_view.dart';
import 'package:pocketpills/ui/views/home/dashboard_prescription_detail_view.dart';
import 'package:pocketpills/ui/views/home/medicine_detail_view.dart';
import 'package:pocketpills/ui/views/home/prescription_arguments.dart';
import 'package:pocketpills/ui/views/home/rating_dialog.dart';
import 'package:pocketpills/ui/views/home/schedule_call_bottom_sheet.dart';
import 'package:pocketpills/ui/views/imageupload/upload_prescription.dart';
import 'package:pocketpills/ui/views/order/order_arguments.dart';
import 'package:pocketpills/ui/views/order/order_detail_view.dart';
import 'package:pocketpills/ui/views/orderstepper/order_stepper.dart';
import 'package:pocketpills/ui/views/pillreminder/pill_reminder_medications_view.dart';
import 'package:pocketpills/ui/views/profile/profile_source_arguments.dart';
import 'package:pocketpills/ui/views/profile/profile_view.dart';
import 'package:pocketpills/ui/views/referral/referral_view.dart';
import 'package:pocketpills/ui/views/signup/transfer_arguments.dart';
import 'package:pocketpills/ui/views/signup/transfer_view.dart';
import 'package:pocketpills/ui/views/signup/youtube_bottom_sheet.dart';
import 'package:pocketpills/ui/views/telehealth/AppointmentDateWidget.dart';
import 'package:pocketpills/ui/views/telehealth/telehealth_preference.dart';
import 'package:pocketpills/ui/views/vitamins/vitamins_catalog_filter_widget.dart';
import 'package:pocketpills/ui/views/vitamins/vitamins_order_stepper.dart';
import 'package:pocketpills/ui/views/vitamins/vitamins_widget.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/route/dashboard_route.dart';
import 'package:provider/provider.dart';

class HomeModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  final DataStoreService dataStore = locator<DataStoreService>();

  AsyncMemoizer<DashboardResponse?> _memoizer = AsyncMemoizer();
  AsyncMemoizer<DashboardResponse?> _localDashboardmemoizer = AsyncMemoizer();
  AsyncMemoizer<ReferralResponse?> _refmemoizer = AsyncMemoizer();
  AsyncMemoizer<Map<String, dynamic>?> contentMemoizer = AsyncMemoizer();

  int? prevPatientId;

  Map<String, int> prevPatient = Map();

  late Future<DashboardResponse?> _futureHomeResponse;
  late Future<DashboardResponse?> _futureLocalDashboardResponse;
  late Future<ReferralResponse?> _futureReferralResponse;

  ConnectivityResult? connectivityResult;
  bool copyFaxNumber = false;

  Future<DashboardResponse?> fetchDashboardData(int patientId, BuildContext context) async {
    this._memoizer = AsyncMemoizer();
    _futureHomeResponse = this._memoizer.runOnce(() async {
      if (prevPatient == null) prevPatient = Map();
      prevPatient["home"] = patientId;
      return await this.getHomeData(context);
    });
    if (prevPatient != null && prevPatient["home"] != null && prevPatient["home"] == patientId) return _futureHomeResponse;
    return _futureHomeResponse;
  }

  Future<DashboardResponse> fetchDashboardLocalData(BuildContext context) async {
    String data = getSelectedLanguage() == ViewConstants.languageIdEn
        ? await DefaultAssetBundle.of(context).loadString("graphics/data.json")
        : await DefaultAssetBundle.of(context).loadString("graphics/data_fr.json");
    Map<String, dynamic> jsonResult = json.decode(data);
    DashboardResponse dashboardResponse = DashboardResponse.fromJson(jsonResult);
    return dashboardResponse;
  }

  clearData() {
    prevPatient = Map();
    _memoizer = AsyncMemoizer();
    this.copyFaxNumber = false;
    notifyListeners();
  }

  clearLogout() {
    prevPatient = Map();
    _memoizer = AsyncMemoizer();
    this.copyFaxNumber = false;
  }

  Future<DashboardResponse?> getHomeData(BuildContext context) async {
    connectivityResult = await checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      return null;
    }
    Provider.of<PillReminderDayModel>(context, listen: false).cleanAsyncMemoizer();
    Response response = await _api.getHomeData();
    if (response != null) {
      BaseResponse<DashboardResponse> res = BaseResponse<DashboardResponse>.fromJson(response.data);
      if (!res.status!) {
        return await fetchDashboardLocalData(context);
      }
      return res.response;
    } else {
      return await fetchDashboardLocalData(context);
    }
  }

  Future<List<Medicine>?> searchMedicines(String keyword) async {
    Response response = await _api.searchMedicines(keyword);
    if (response != null) {
      SearchResponse res = SearchResponse.fromJson(response.data);
      if (!res.status) {
        errorMessage = res.getErrorMessage();
        return null;
      }
      return res.medicines;
    } else {
      return null;
    }
  }

  Future<ReferralResponse?> fetchReferralData(int patientId) async {
    if (prevPatient != null && prevPatient["referral"] != null && prevPatient["referral"] == patientId) return _futureReferralResponse;
    this._refmemoizer = AsyncMemoizer();
    _futureReferralResponse = this._refmemoizer.runOnce(() async {
      if (prevPatient == null) prevPatient = Map();
      prevPatient["referral"] = patientId;
      return await this.getReferralCode();
    });
    return _futureReferralResponse;
  }

  Future<ReferralResponse?> getReferralCode() async {
    connectivityResult = await checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      return null;
    }
    Response response = await _api.getReferralCode();
    if (response != null) {
      BaseResponse<ReferralResponse> res = BaseResponse<ReferralResponse>.fromJson(response.data);
      if (!res.status!) {
        return null;
      } else {
        return res.response;
      }
    } else {
      return null;
    }
  }

  Future<Medicine?> getMedicine(String id) async {
    Response response = await _api.getMedicine(id);
    if (response != null) {
      MedicineResponse res = MedicineResponse.fromJson(response.data);
      if (!res.status) {
        errorMessage = res.getErrorMessage();
        return null;
      }
      return res.medicine;
    } else {
      return null;
    }
  }

  Future<bool> trackDashboardCard(int cardId) async {
    Response response = await _api.trackDashboardCard(cardId);
    if (response != null) {
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      if (!res.status!) {
        errorMessage = res.getErrorMessage();
        return false;
      }
      return true;
    } else {
      return false;
    }
  }

  Future<bool> postSendInvitationSMS(String value) async {
    List<String> phoneNumberList = [];
    phoneNumberList.add(value);
    SendInvitationMessageRequest messageRequest = SendInvitationMessageRequest(phoneNumbers: phoneNumberList);
    Response response = await _api.postSendInvitationSMS(RequestWrapper(body: messageRequest));
    if (response != null) {
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      if (!res.status!) {
        return false;
      }
      errorMessage = res.errMessage!;
      return true;
    } else {
      return false;
    }
  }

  handleDashboardRoute(ActionType actionType, BuildContext context, {DashboardItem? dashboardItem}) {
    sendDashboardRouteAnalyticsEvent(actionType);
    switch (actionType) {
      case ActionType.TRANSFER: // transfer
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.transfer);
        Navigator.pushNamed(context, TransferWidget.routeName,
            arguments: TransferArguments(
              source: BaseStepperSource.MAIN_SCREEN,
            ));
        break;
      case ActionType.UPLOAD_PRESCRIPTION: // upload prescription
        Navigator.pushNamed(context, UploadPrescription.routeName);
        break;
      case ActionType.TELEHEALTH: // telehealth Date
        Navigator.pushNamed(context, AppointmentDateWidget.routeName);
        break;
      case ActionType.MEDICATIONS: // Prescription Listing bottom tab
        Provider.of<DashboardModel>(context, listen: false).dashboardIndex = 1;
        break;
      case ActionType.PRESCRIPTION_LIST: // Prescription Listing bottom tab
        Provider.of<DashboardModel>(context, listen: false).dashboardIndex = 2;
        break;
      case ActionType.ORDER_LIST: // Order Listing bottom tab
        Provider.of<DashboardModel>(context, listen: false).dashboardIndex = 3;
        break;
      case ActionType.ADD_MEMBER: // add member flow
        Navigator.pushNamed(context, AddMemberSignupWidget.routeName);
        break;
      case ActionType.CUSTOMER_CARE: // open chat directly
        showCallAndChatBottomSheet(context);
        break;
      case ActionType.MEDICATION_SEARCH: // Search screen with top search bar
        Navigator.pushNamed(context, MedicineDetailWidget.routeName);
        break;
      case ActionType.VITAMINS_LANDING: // open vitamins screen (chat with expert and build your own pack)
        Provider.of<VitaminsSubscriptionModel>(context, listen: false).clearVitaminList();
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_vitamins_pack);
        Navigator.pushNamed(context, VitaminsWidget.routeName);
        break;
      case ActionType.VITAMINS_CATEGORIES: // open vitamins categories and select one of them on the basis of category coming from backed
        Navigator.pushNamed(context, VitaminsCatalogFilterWidget.routeName,
            arguments: BaseStepperArguments(source: BaseStepperSource.VITAMINS_SCREEN, itemId: dashboardItem!.itemId));
        break;
      case ActionType.VITAMINS_CART: // open the vitamins review screen and added all vitamins
        onClickVitaminsCart(context, dashboardItem!);
        break;
      case ActionType.REFERRAL:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_referral_know_more);
        Navigator.pushNamed(context, ReferralView.routeName); // redirect to referral screen --> add referral code in card description
        break;
      case ActionType.RATE_US: // same as current -->  dynamic card handling with backend pending
        ratingDialog(context);
        break;
      case ActionType.ORDER_DETAIL: // Order detail page
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_orders_detail);
        if (dashboardItem!.itemId != null && dashboardItem.itemId != "") {
          try {
            int orderId = int.parse(dashboardItem.itemId!);
            Navigator.pushNamed(context, OrderDetailWidget.routeName, arguments: OrderArguments(orderId: orderId));
          } catch (ex) {
            FirebaseCrashlytics.instance.log(ex.toString());
          }
        } else {
          Provider.of<DashboardModel>(context, listen: false).dashboardIndex = 3;
        }
        break;
      case ActionType.PRESCRIPTION_DETAIL: // Prescription details page
        if (dashboardItem!.itemId != null && dashboardItem.itemId != "") {
          try {
            int prescriptionId = int.parse(dashboardItem.itemId!);
            Navigator.pushNamed(context, DashboardPrescriptionsDetailsView.routeName, arguments: PrescriptionArguments(prescriptionId: prescriptionId));
          } catch (ex) {
            FirebaseCrashlytics.instance.log(ex.toString());
          }
        } else {
          Provider.of<DashboardModel>(context, listen: false).dashboardIndex = 2;
        }
        break;
      case ActionType.OPEN_VIDEO: // play a video in the application in current screen
        if (dashboardItem!.itemId != null) {
          openVideoPlayer(dashboardItem.itemId!, context);
        }
        break;
      case ActionType.OPEN_CHAT: // open bottom sheet with call and chat option
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_vitamins_with_expert_chat);
        Navigator.pushNamed(context, ChatWidget.routeName);
        break;
      case ActionType.ORDER_HEALTH_CARD: // order stepper Insurence screen
        Navigator.pushNamed(context, OrderStepper.routeName, arguments: BaseStepperArguments(startStep: 1));
        break;
      case ActionType.ORDER_INSURANCE: // order stepper Insurence screen
        Navigator.pushNamed(context, OrderStepper.routeName, arguments: BaseStepperArguments(startStep: 0));
        break;
      case ActionType.ORDER_ADDRESS: // order stepper Address screen
        Navigator.pushNamed(context, OrderStepper.routeName, arguments: BaseStepperArguments(startStep: 2));
        break;
      case ActionType.ORDER_PAYMENT: // order stepper payment screen
        Navigator.pushNamed(context, OrderStepper.routeName, arguments: BaseStepperArguments(startStep: 3));
        break;
      case ActionType.ORDER_HEALTH: // order stepper health screen
        Navigator.pushNamed(context, OrderStepper.routeName, arguments: BaseStepperArguments(startStep: 4));
        break;
      case ActionType.SCHEDULE_CALL: // open bottom sheet with time
        showScheduleCallBottomSheet(context, true);
        break;
      case ActionType.RESCHEDULE_CALL: // open bottom sheet with time
        showScheduleCallBottomSheet(context, false);
        break;
      case ActionType.BLANK: // nothing do with this action
        // TODO: Handle this case.
        break;
      case ActionType.REFILL:
        Provider.of<DashboardModel>(context, listen: false).dashboardIndex = 1;
        break;
      case ActionType.PILL_REMINDER:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.pill_pack_medication_view);
        Navigator.pushNamed(context, PillReminderMedicationsView.routeName, arguments: PillReminderMedicationsView());
        break;
      case ActionType.SWITCH_OR_ADD_MEMBER:
        DashboardAppBarState().showBottomSheet(context, Provider.of<DashboardModel>(context, listen: false).patientList);
        break;
      case ActionType.EDIT_ADDRESS:
        Navigator.pushNamed(context, ProfileWidget.routeName, arguments: ProfileSourceArguments(index: 3));
        break;
      case ActionType.FAX:
        if (!this.copyFaxNumber) {
          this.copyFaxNumber = true;
          Clipboard.setData(new ClipboardData(text: ApplicationConstant.fax_number));
          showSnackBar(context, LocalizationUtils.getSingleValueString("app-dashboard", "app-dashboard.fax.copy-clipboard"));
        }
        break;
      case ActionType.APPOINTMENT_DETAIL:
        if (dashboardItem!.itemId != null && dashboardItem.itemId != "") {
          try {
            int prescriptionId = int.parse(dashboardItem.itemId!);
            Navigator.pushNamed(context, DashboardAppointmentDetailsView.routeName, arguments: PrescriptionArguments(prescriptionId: prescriptionId));
          } catch (ex) {
            FirebaseCrashlytics.instance.log(ex.toString());
          }
        } else {
          Provider.of<DashboardModel>(context, listen: false).dashboardIndex = 2;
        }
        break;
    }
  }

  void onClickVitaminsCart(BuildContext context, DashboardItem dashboardItem) async {
    if (dashboardItem.itemId != null && dashboardItem.itemId != "") {
      try {
        int orderId = int.parse(dashboardItem.itemId!);
        setState(ViewState.Busy);
        Response response = await _api.updateVitaminsCartFromDashboard(orderId);
        setState(ViewState.Idle);
        if (response != null) {
          OldBaseResponse oldBaseResponse = OldBaseResponse.fromJson(response.data);
          if (oldBaseResponse.status == true) {
            setState(ViewState.Busy);
            bool result = await Provider.of<VitaminsCatalogModel>(context, listen: false).getDashboardVitaminsReviewCart();
            setState(ViewState.Idle);
            if (result == true) {
              Navigator.pushNamed(context, VitaminsOrderStepper.routeName,
                  arguments: BaseStepperArguments(source: BaseStepperSource.VITAMINS_SCREEN, startStep: 0));
            } else {
              navigateToVitaminCatalogFilter(context);
            }
          } else {
            navigateToVitaminCatalogFilter(context);
          }
        }
      } catch (ex) {
        navigateToVitaminCatalogFilter(context);
        FirebaseCrashlytics.instance.log(ex.toString());
      }
    } else {
      setState(ViewState.Busy);
      bool result = await Provider.of<VitaminsCatalogModel>(context, listen: false).getDashboardVitaminsReviewCart();
      setState(ViewState.Idle);
      if (result == true) {
        Navigator.pushNamed(context, VitaminsOrderStepper.routeName, arguments: BaseStepperArguments(source: BaseStepperSource.VITAMINS_SCREEN, startStep: 0));
      } else {
        navigateToVitaminCatalogFilter(context);
      }
    }
  }

  sendDashboardRouteAnalyticsEvent(ActionType actionType) {
    try {
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.base_dashboard + actionType.toString().split(".")[1].toLowerCase());
    } catch (ex) {
      FirebaseCrashlytics.instance.log(ex.toString());
    }
  }

  void navigateToVitaminCatalogFilter(BuildContext context) {
    Navigator.pushNamed(context, VitaminsCatalogFilterWidget.routeName, arguments: BaseStepperArguments(source: BaseStepperSource.VITAMINS_SCREEN));
  }

  openVideoPlayer(String videoId, BuildContext context) {
    showModalBottomSheet(
        context: context,
        isDismissible: true,
        isScrollControlled: false,
        backgroundColor: Colors.transparent,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SMALL_XXX),
        ),
        builder: (BuildContext bottomSheetContext) {
          return YouTubeSheet(
            context: context,
          );
        });
  }

  showScheduleCallBottomSheet(BuildContext context, bool scheduleCall) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SMALL_XXX),
        ),
        context: context,
        isScrollControlled: true,
        builder: (BuildContext bottomSheetContext) {
          return ScheduleCallBottomSheet(
            scheduleCall: scheduleCall,
            context: context,
          );
        });
  }

  showCallAndChatBottomSheet(BuildContext context) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SMALL_XXX),
        ),
        builder: (BuildContext bottomSheetContext) {
          return CallChatBottomSheet(
            context: context,
          );
        });
  }

  Future<void> ratingDialog(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return RatingAlertDialog();
      },
    );
  }

  Future<Map<String, dynamic>?> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String>? keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic>? localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = <LocalizationRequest>[];
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        if (response != null) {
          BaseResponse<Map> res = BaseResponse<Map>.fromJson(response.data);
          print("Called api  $keys");
          return LocalizationUtils.saveNewPageData2(keys, res.response);
        } else
          return null;
      }
    });
  }

  Future<AppointmentFirstResponse?> telehealthAppointmentDetails() async {
    return await this.getPrescriptionsTelehealth();
  }

  Future<AppointmentFirstResponse?> getPrescriptionsTelehealth() async {
    connectivityResult = await checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      return null;
    }
    Response response = await _api.getPrescriptionsTelehealthAppointment();
    if (response != null) {
      OMSResponse<AppointmentFirstResponse> omsResponse = OMSResponse<AppointmentFirstResponse>.fromJson(response.data);
      if (!omsResponse.status!) {
        errorMessage = omsResponse.getErrorMessage();
        return null;
      }
      print(omsResponse.response);
      return omsResponse.response;
    } else {
      return null;
    }
  }
}
