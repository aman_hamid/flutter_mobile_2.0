import 'dart:collection';

import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/medicine.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/request/request_wrapper.dart';
import 'package:pocketpills/core/request/vitamins/add_medicine_request.dart';
import 'package:pocketpills/core/request/vitamins/complete_checkout_request.dart';
import 'package:pocketpills/core/request/vitamins/update_shopping_cart_request.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/free_medicine_response.dart';
import 'package:pocketpills/core/response/old_base_response.dart';
import 'package:pocketpills/core/response/vitamins/add_medicine_response.dart';
import 'package:pocketpills/core/response/vitamins/coupon.dart';
import 'package:pocketpills/core/response/vitamins/coupon_list_response.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class VitaminsCatalogModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  final DataStoreService dataStore = locator<DataStoreService>();

  AsyncMemoizer<List<Medicine>?> _memoizer = AsyncMemoizer();
  AsyncMemoizer<List<Coupon>?> _couponCodeMemoizer = AsyncMemoizer();
  AsyncMemoizer<Map<String, dynamic>?> contentMemoizer = AsyncMemoizer();

  late Future<List<Medicine>?> _vitaminsCatalogFutureList;

  late Future<List<Coupon>?> _couponCodeFutureList;

  Map<int, Medicine> _vitaminsCatalogList = Map();

  AddMedicineResponse? _addMedicineResponse;

  ConnectivityResult? connectivityResult;

  int? prevUserId;
  int? _medicineId;

  int get medicineId => _medicineId!;

  AddMedicineResponse get addMedicineResponse => _addMedicineResponse!;

  Map<int, Medicine> get vitaminsCatalogList => _vitaminsCatalogList;

  List<Coupon>? coupons;

  Future<List<Medicine>?> fetchVitaminsCatalog(String filterArgu) async {
    _vitaminsCatalogFutureList = this._memoizer.runOnce(() async {
      return await this.getVitaminsData(filterArgu);
    });
    if (_addMedicineResponse == null) {
      await getShoppingCart();
    }
    return _vitaminsCatalogFutureList;
  }

  clearVitaminList() {
    _memoizer = AsyncMemoizer();
    _couponCodeMemoizer = AsyncMemoizer();
    _addMedicineResponse = null;
    _vitaminsCatalogList = Map();
  }

  clearAsyncMemorizer() {
    clearVitaminList();
    notifyListeners();
  }

  void updateDosageTime(Medicine medicine, List<bool> values) {
    String dosageTime = (values[0] == true ? "1" : "0") + (values[1] == true ? "1" : "0") + (values[2] == true ? "1" : "0") + (values[3] == true ? "1" : "0");
    updateMedicineToShoppingCart(medicine, dosageTime, medicine.monthsSupply!);
  }

  List<String?>? dosageToTimes(Medicine medicine) {
    if (medicine.modifyuserSigCode != null) {
      List<String> dosageTime = [];
      if (medicine.modifyuserSigCode![0] == '1') dosageTime.add(LocalizationUtils.getSingleValueString("modal", "modal.timeslot.morning"));
      if (medicine.modifyuserSigCode![1] == '1') dosageTime.add(LocalizationUtils.getSingleValueString("modal", "modal.timeslot.afternoon"));
      if (medicine.modifyuserSigCode![2] == '1') dosageTime.add(LocalizationUtils.getSingleValueString("modal", "modal.timeslot.evening"));
      if (medicine.modifyuserSigCode![3] == '1') dosageTime.add(LocalizationUtils.getSingleValueString("modal", "modal.timeslot.bedtime"));
      return dosageTime;
    } else {
      Map<int, List<String>> retVal = {
        1: [LocalizationUtils.getSingleValueString("modal", "modal.timeslot.morning")],
        2: [LocalizationUtils.getSingleValueString("modal", "modal.timeslot.morning"), LocalizationUtils.getSingleValueString("modal", "modal.timeslot.evening")],
        3: [
          LocalizationUtils.getSingleValueString("modal", "modal.timeslot.morning"),
          LocalizationUtils.getSingleValueString("modal", "modal.timeslot.afternoon"),
          LocalizationUtils.getSingleValueString("modal", "modal.timeslot.evening")
        ],
        4: [
          LocalizationUtils.getSingleValueString("modal", "modal.timeslot.morning"),
          LocalizationUtils.getSingleValueString("modal", "modal.timeslot.afternoon"),
          LocalizationUtils.getSingleValueString("modal", "modal.timeslot.evening"),
          LocalizationUtils.getSingleValueString("modal", "modal.timeslot.bedtime")
        ]
      };
      return retVal[medicine.dosage];
    }
  }

  Future<List<Medicine>?> getVitaminsData(String filterArgu) async {
    connectivityResult = await checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      return null;
    }
    setState(ViewState.Busy);
    Response response = await _api.getVitaminsCatalogList(filterArgu);
    setState(ViewState.Idle);
    if (response != null) {
      FreeMedicineResponse res = FreeMedicineResponse.fromJson(response.data);
      if (!res.status!) {
        errorMessage = res.getErrorMessage();
        return null;
      }
      res.medicines!.forEach((value) => {_vitaminsCatalogList[value.variantId!] = value});
      await getShoppingCart();
      return res.medicines;
    } else {
      return null;
    }
  }

  Future<bool> getDashboardVitaminsReviewCart() async {
    clearVitaminList();
    Response response = await _api.getVitaminsCatalogList("");
    if (response != null) {
      FreeMedicineResponse res = FreeMedicineResponse.fromJson(response.data);
      if (!res.status!) {
        errorMessage = res.getErrorMessage();
        return false;
      }
      res.medicines!.forEach((value) => {_vitaminsCatalogList[value.variantId!] = value});
      bool result = await getShoppingCart();
      if (result == true) {
        await getCouponCodeListAndApply();
        return true;
      }
      return false;
    } else {
      return false;
    }
  }

  Future<bool?> getCouponCodeListAndApply() async {
    List<Coupon>? couponCodeList = await this.getCouponCode();
    if (couponCodeList != null && couponCodeList.length > 0) {
      try {
        updateShoppingCartCoupon(couponCodeList[0].title!);
      } catch (ex) {
        FirebaseCrashlytics.instance.log(ex.toString());
      }
    }
  }

  Future<List<Coupon>?> getCouponCodeList() async {
    _couponCodeFutureList = this._couponCodeMemoizer.runOnce(() async {
      return await this.getCouponCode();
    });

    return _couponCodeFutureList;
  }

  Future<List<Coupon>?> getCouponCode() async {
    Response response = await _api.getCouponCodeList();
    if (response != null) {
      CouponListResponse res = CouponListResponse.fromJson(response.data);
      if (!res.status) {
        errorMessage = res.getErrorMessage();
        return null;
      }
      coupons = res.coupons;
      return res.coupons;
    } else {
      return null;
    }
  }

  Future<bool> getShoppingCart() async {
    Response response = await _api.getShoppingCartList();
    if (response != null) {
      BaseResponse<AddMedicineResponse> res = BaseResponse<AddMedicineResponse>.fromJson(response.data);
      if (!res.status!) {
        errorMessage = res.getErrorMessage();
        return false;
      }

      updateVitaminsCatalogList(res.response!);
      setSelectedMedicineOnTop(res.response!);
      this._addMedicineResponse = res.response;

      notifyListeners();
      return true;
    }
    return false;
  }

  Medicine? getSelectedMedicine() {
    return _vitaminsCatalogList[_medicineId];
  }

  Map<int, Medicine> getSelectedMedicineList() {
    Map<int, Medicine> _selectedVitaminsList = Map();
    _vitaminsCatalogList.forEach((k, v) => {
          if (v.isInShoppingCart == true) {_selectedVitaminsList[v.variantId!] = v}
        });
    return _selectedVitaminsList;
  }

  setSelectedMedicine(id) {
    this._medicineId = id;
  }

  Future<void> updateMedicineToShoppingCart(Medicine medicine, String userSigCode, int monthsSupply) async {
    AddMedicineRequest addMedicineRequest = AddMedicineRequest(variantId: medicine.variantId, monthsSupply: monthsSupply, userSigCode: userSigCode);
    setState(ViewState.Busy);
    Response response = await _api.updateShoppingCartItems(RequestWrapper(body: addMedicineRequest), medicine.variantId!);
    setState(ViewState.Idle);
    if (response != null) {
      BaseResponse<AddMedicineResponse> res = BaseResponse<AddMedicineResponse>.fromJson(response.data);
      errorMessage = res.getErrorMessage();
      if (!res.status!) {
        return null;
      }
      _addMedicineResponse = res.response;
      updateVitaminsCatalogList(res.response!);
      notifyListeners();
      return null;
    } else {
      return null;
    }
  }

  Future<bool> addMedicineToShoppingCart(Medicine medicine) async {
    AddMedicineRequest addMedicineRequest = AddMedicineRequest(variantId: medicine.variantId, monthsSupply: 1, userSigCode: medicine.userSigCode);
    setState(ViewState.Busy);
    Response response = await _api.addMedicineToShoppingCart(RequestWrapper(body: addMedicineRequest));
    setState(ViewState.Idle);
    if (response != null) {
      BaseResponse<AddMedicineResponse> res = BaseResponse<AddMedicineResponse>.fromJson(response.data);
      errorMessage = res.getErrorMessage();
      if (!res.status!) {
        return false;
      }
      updateVitaminsCatalogList(res.response!);
      return true;
    } else {
      return false;
    }
  }

  Future<bool> removeMedicineFromShoppingCart(Medicine medicine) async {
    setState(ViewState.Busy);
    Response response = await _api.removeMedicineFromShoppingCart(medicine.variantId!);
    setState(ViewState.Idle);
    if (response != null) {
      BaseResponse<AddMedicineResponse> res = BaseResponse<AddMedicineResponse>.fromJson(response.data);
      if (!res.status!) {
        errorMessage = res.getErrorMessage();
        return false;
      }
      this._addMedicineResponse = res.response;
      _vitaminsCatalogList[medicine.variantId]!.isInShoppingCart = false;

      notifyListeners();
      return true;
    } else {
      return false;
    }
  }

  Future<bool> updateShoppingCartCoupon(String couponCode) async {
    UpdateShoppingCartRequest shoppingCartRequest = UpdateShoppingCartRequest(coupon: couponCode);
    return updateShoppingCartValues(shoppingCartRequest);
  }

  Future<bool> updateShoppingCartAddress(int addressId) async {
    UpdateShoppingCartRequest shoppingCartRequest = UpdateShoppingCartRequest(addressId: addressId);
    return updateShoppingCartValues(shoppingCartRequest);
  }

  Future<bool> updateShoppingCartPaymentCard(int ccId) async {
    UpdateShoppingCartRequest shoppingCartRequest = UpdateShoppingCartRequest(ccId: ccId);
    return updateShoppingCartValues(shoppingCartRequest);
  }

  Future<bool> updateShoppingCartComment(String comment) async {
    UpdateShoppingCartRequest shoppingCartRequest = UpdateShoppingCartRequest(comment: comment);
    return updateShoppingCartValues(shoppingCartRequest);
  }

  Future<bool> updateShoppingCartRemoveCouponcode(bool removeCouponcode) async {
    UpdateShoppingCartRequest shoppingCartRequest = UpdateShoppingCartRequest(removeCoupon: removeCouponcode);
    return updateShoppingCartValues(shoppingCartRequest);
  }

  Future<bool> updateShoppingCartValues(UpdateShoppingCartRequest shoppingCartRequest) async {
    setState(ViewState.Busy);
    Response response = await _api.updateShoppingCart(RequestWrapper(body: shoppingCartRequest));
    setState(ViewState.Idle);
    if (response != null) {
      BaseResponse<AddMedicineResponse> res = BaseResponse<AddMedicineResponse>.fromJson(response.data);
      if (!res.status!) {
        errorMessage = res.getErrorMessage();
        return false;
      }
      _addMedicineResponse = res.response;
      updateVitaminsCatalogList(res.response!);
      notifyListeners();
      return true;
    } else {
      return false;
    }
  }

  Future<bool> completeShoppingCartCheckOut() async {
    setState(ViewState.Busy);
    CompleteCheckoutRequest checkoutRequest = CompleteCheckoutRequest();
    Response response = await _api.completeShoppingCartCheckOut(RequestWrapper(body: checkoutRequest));
    setState(ViewState.Idle);
    if (response != null) {
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      if (!res.status!) {
        errorMessage = res.getErrorMessage();
        return false;
      }
      return true;
    } else {
      return false;
    }
  }

  void updateVitaminsCatalogList(AddMedicineResponse response) {
    this._addMedicineResponse = response;
    if (_addMedicineResponse!.shoppingCartItems != null && _addMedicineResponse!.shoppingCartItems!.length > 0) {
      _addMedicineResponse!.shoppingCartItems!.forEach((value) => {
            if (_vitaminsCatalogList.containsKey(value.variantId))
              {
                _vitaminsCatalogList[value.variantId]!.isInShoppingCart = true,
                _vitaminsCatalogList[value.variantId]!.monthsSupply = value.monthsSupply,
                _vitaminsCatalogList[value.variantId]!.modifyQuantity = value.quantity,
                _vitaminsCatalogList[value.variantId]!.modifyuserSigCode = value.userSigCode,
                _vitaminsCatalogList[value.variantId]!.drugTotalPrice = value.listPrice
              }
          });
      notifyListeners();
    } else {
      _vitaminsCatalogList.forEach((key, value) => {
            if (_vitaminsCatalogList.containsKey(value.variantId))
              {
                _vitaminsCatalogList[value.variantId]!.isInShoppingCart = false,
                _vitaminsCatalogList[value.variantId]!.monthsSupply = value.monthsSupply,
                _vitaminsCatalogList[value.variantId]!.modifyQuantity = value.quantity,
                _vitaminsCatalogList[value.variantId]!.modifyuserSigCode = value.userSigCode,
                _vitaminsCatalogList[value.variantId]!.drugTotalPrice = value.totalPrice
              }
          });

      notifyListeners();
    }
  }

  void setSelectedMedicineOnTop(AddMedicineResponse response) {
    LinkedHashMap<int, Medicine> selectedVitamins = LinkedHashMap();
    LinkedHashMap<int, Medicine> unSelectedVitamins = LinkedHashMap();
    _vitaminsCatalogList.forEach((key, value) => {
          if (value.isInShoppingCart == true) {selectedVitamins[value.variantId!] = value} else {unSelectedVitamins[value.variantId!] = value}
        });

    _vitaminsCatalogList = LinkedHashMap();
    _vitaminsCatalogList.addAll(selectedVitamins);
    _vitaminsCatalogList.addAll(unSelectedVitamins);

    notifyListeners();
  }

  Future<Map<String, dynamic>?> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String>? keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic>? localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = <LocalizationRequest>[];
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        if (response != null) {
          BaseResponse<Map> res = BaseResponse<Map>.fromJson(response.data);
          print("Called api  $keys");
          return LocalizationUtils.saveNewPageData2(keys, res.response);
        } else
          return null;
      }
    });
  }
}
