// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'signup_transfer_upload_image_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SignupTransferUploadImageRequest _$SignupTransferUploadImageRequestFromJson(
    Map<String, dynamic> json) {
  return SignupTransferUploadImageRequest(
    documentPath: json['documentPath'] as String?,
    documentType: json['documentType'] as String?,
    resizedDocumentPath: json['resizedDocumentPath'] as String?,
    pageNumber: json['pageNumber'] as int?,
  );
}

Map<String, dynamic> _$SignupTransferUploadImageRequestToJson(
        SignupTransferUploadImageRequest instance) =>
    <String, dynamic>{
      'documentPath': instance.documentPath,
      'documentType': instance.documentType,
      'resizedDocumentPath': instance.resizedDocumentPath,
      'pageNumber': instance.pageNumber,
    };
