import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';

part 'add_medicine_request.g.dart';

@JsonSerializable(explicitToJson: true)
class AddMedicineRequest extends BaseRequest {
  int? variantId;
  int? monthsSupply;
  String? userSigCode;

  AddMedicineRequest({this.variantId, this.monthsSupply, this.userSigCode});

  factory AddMedicineRequest.fromJson(Map<String, dynamic> json) => _$AddMedicineRequestFromJson(json);
  Map<String, dynamic> toJson() => _$AddMedicineRequestToJson(this);
}
