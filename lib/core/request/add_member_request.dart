import 'package:pocketpills/core/models/patient.dart';

import 'base_request.dart';

class AddMemberRequest extends BaseRequest {
  late String relation;
  late Patient patient;

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = Map();
    map['relation'] = relation;
    map['patient'] = patient.toJson();
    return map;
  }
}

class MemberAboutYouRequest extends BaseRequest {
  String? province;
  String? gender;
  bool? hasDailyMedication;
  bool? pocketPacks;

  MemberAboutYouRequest({this.province, this.gender, this.hasDailyMedication, this.pocketPacks});

  Map<String, dynamic> toJson() => <String, dynamic>{
        'province': this.province,
        'pocketPacks': this.pocketPacks,
        'gender': this.gender,
        'hasDailyMedication': this.hasDailyMedication.toString(),
      };
}
