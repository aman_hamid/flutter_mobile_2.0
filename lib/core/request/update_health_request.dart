import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';

part 'update_health_request.g.dart';

@JsonSerializable()
class UpdateHealthRequest extends BaseRequest {
  List<String>? allergies;
  int? patientId;
  List<String>? vitamins;

  UpdateHealthRequest({required this.allergies, required this.patientId, required this.vitamins});

  Map<String, dynamic> toJson() => _$UpdateHealthRequestToJson(this);
}
