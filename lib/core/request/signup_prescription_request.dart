import 'package:pocketpills/core/request/base_request.dart';

class SignupTransferPrescriptionRequest extends BaseRequest {
  String? type;
  String? pharmacyName;
  String? pharmacyAddress;
  int? pharmacyPhone;
  bool? isTransferAll;
  String? prescriptionState;

  SignupTransferPrescriptionRequest({
    this.type,
    this.pharmacyName,
    this.pharmacyPhone,
    this.isTransferAll,
    this.prescriptionState,
    this.pharmacyAddress,
  });

  Map<String, dynamic> toJson() => <String, dynamic>{
        'type': this.type,
        'pharmacyName': this.pharmacyName,
        'pharmacyPhone': this.pharmacyPhone.toString(),
        'pharmacyAddress': this.pharmacyAddress,
        'isTransferAll': this.isTransferAll.toString(),
        'prescriptionState': this.prescriptionState
      };
}
