import 'package:pocketpills/core/request/base_request.dart';

class AuthProviderGoogle extends BaseRequest {
  String? idToken;
  String? accessToken;
  String? externalAuthProvider;

  AuthProviderGoogle({this.idToken, this.accessToken, this.externalAuthProvider});

  Map<String, dynamic> toJson() =>
      <String, dynamic>{'idToken': this.idToken, 'accessToken': this.accessToken, 'externalAuthProvider': this.externalAuthProvider};
}
