import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';

part 'update_email_request.g.dart';

@JsonSerializable()
class EmailUpdateRequest extends BaseRequest {
  String? email;

  EmailUpdateRequest({this.email});

  factory EmailUpdateRequest.fromJson(Map<String, dynamic> data) => _$EmailUpdateRequestFromJson(data);

  Map<String, dynamic> toJson() => _$EmailUpdateRequestToJson(this);
}
