import 'package:pocketpills/core/request/base_request.dart';

class AuthProviderApple extends BaseRequest {
  String? idToken;
  String? authorizationCode;
  String? firstName;
  String? lastName;
  String? email;
  String? externalAuthProvider;

  AuthProviderApple({this.idToken, this.authorizationCode, this.firstName, this.lastName, this.email, this.externalAuthProvider});

  Map<String, dynamic> toJson() => <String, dynamic>{
        'idToken': this.idToken,
        'authorizationCode': this.authorizationCode,
        'firstName': this.firstName,
        'lastName': this.lastName,
        'email': this.email,
        'externalAuthProvider': this.externalAuthProvider
      };
}
