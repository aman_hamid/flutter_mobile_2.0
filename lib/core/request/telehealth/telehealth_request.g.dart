// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'telehealth_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TelehealthRequest _$TelehealthRequestFromJson(Map<String, dynamic> json) {
  return TelehealthRequest(
    prescriptionType: json['prescriptionType'] as String?,
    prescriptionFilledByExternalPharmacy:
        json['prescriptionFilledByExternalPharmacy'] as bool?,
    prescriptionRequestReason: json['prescriptionRequestReason'] as String?,
    prescriptionState: json['prescriptionState'] as String?,
    prescriptionRequestCategory: json['prescriptionRequestCategory'] as String?,
    prescriptionMedicalConditions:
        json['prescriptionMedicalConditions'] as String?,
    telehealthRequestedMedications:
        json['telehealthRequestedMedications'] as String?,
    prescriptionComment: json['prescriptionComment'] as String?,
    appointmentTime: PPDateUtils.fromStr(json['appointmentTime'] as String?),
    isPrescriptionEdited: json['isPrescriptionEdited'] as bool?,
  );
}

Map<String, dynamic> _$TelehealthRequestToJson(TelehealthRequest instance) =>
    <String, dynamic>{
      'prescriptionType': instance.prescriptionType,
      'prescriptionFilledByExternalPharmacy':
          instance.prescriptionFilledByExternalPharmacy,
      'prescriptionRequestReason': instance.prescriptionRequestReason,
      'prescriptionState': instance.prescriptionState,
      'prescriptionRequestCategory': instance.prescriptionRequestCategory,
      'prescriptionMedicalConditions': instance.prescriptionMedicalConditions,
      'telehealthRequestedMedications': instance.telehealthRequestedMedications,
      'prescriptionComment': instance.prescriptionComment,
      'appointmentTime': PPDateUtils.toStr(instance.appointmentTime),
      'isPrescriptionEdited': instance.isPrescriptionEdited,
    };
