import 'package:json_annotation/json_annotation.dart';

part 'health_info.g.dart';

@JsonSerializable()
class HealthInfo {
  int? id;
  bool? disabled;
  int? patientId;
  List<String>? allergies;
  List<String>? vitamins;

  HealthInfo({this.patientId, this.id, this.disabled, this.allergies, this.vitamins});

  factory HealthInfo.fromJson(Map<String, dynamic> json) => _$HealthInfoFromJson(json);

  Map<String, dynamic> toJson() => _$HealthInfoToJson(this);
}
