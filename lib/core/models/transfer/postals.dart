import 'package:json_annotation/json_annotation.dart';
part 'postals.g.dart';

@JsonSerializable()
class Postal {
  String? code;

  Postal({this.code});

  factory Postal.fromJson(Map<String, dynamic> json) => _$PostalFromJson(json);
}
