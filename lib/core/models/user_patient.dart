import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/patient.dart';
import 'package:pocketpills/core/models/user.dart';
import 'package:pocketpills/core/request/base_request.dart';

part 'user_patient.g.dart';

@JsonSerializable()
class UserPatient extends BaseRequest {
  int? id;
  bool? disabled;
  int? userId;
  int? patientId;
  bool? primary;
  Patient? patient;
  User? user;
  bool? hasViewedTransferRefills;
  bool? hasProvidedTransferInformation;
  int? defaultCreditCardId;
  String? userRelation;
  bool? consent;
  String? userPatientLocale;

  UserPatient(
      {this.id,
      this.disabled,
      this.userId,
      this.patientId,
      this.primary,
      this.patient,
      this.user,
      this.hasViewedTransferRefills,
      this.hasProvidedTransferInformation,
      this.defaultCreditCardId,
      this.userRelation,
      this.consent,
      this.userPatientLocale});

  factory UserPatient.fromJson(Map<String, dynamic> json) => _$UserPatientFromJson(json);

  Map<String, dynamic> toJson() => _$UserPatientToJson(this);
}
