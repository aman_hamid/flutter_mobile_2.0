// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_verification.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AuthVerification _$AuthVerificationFromJson(Map<String, dynamic> json) {
  return AuthVerification(
    hint: json['hint'] as String?,
    userMessage: json['userMessage'] as String?,
    redirect: json['redirect'] as String?,
    signUpType: json['signUpType'] as String?,
    userId: json['userId'] as int?,
    user: json['user'] == null
        ? null
        : User.fromJson(json['user'] as Map<String, dynamic>),
    patientId: json['patientId'] as int?,
    prescription: json['prescription'] == null
        ? null
        : TransferPrescription.fromJson(
            json['prescription'] as Map<String, dynamic>),
    userIdentifier: json['userIdentifier'] as String?,
  );
}

Map<String, dynamic> _$AuthVerificationToJson(AuthVerification instance) =>
    <String, dynamic>{
      'hint': instance.hint,
      'userMessage': instance.userMessage,
      'redirect': instance.redirect,
      'userIdentifier': instance.userIdentifier,
      'signUpType': instance.signUpType,
      'userId': instance.userId,
      'user': instance.user,
      'patientId': instance.patientId,
      'prescription': instance.prescription,
    };
