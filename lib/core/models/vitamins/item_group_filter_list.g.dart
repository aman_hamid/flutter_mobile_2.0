// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'item_group_filter_list.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ItemGroupFilterList _$ItemGroupFilterListFromJson(Map<String, dynamic> json) {
  return ItemGroupFilterList(
    json['id'] as int?,
    json['itemGroupName'] as String?,
    json['itemGroupIconUrl'] as String?,
    json['isInVitaminFilter'] as bool?,
  );
}

Map<String, dynamic> _$ItemGroupFilterListToJson(
        ItemGroupFilterList instance) =>
    <String, dynamic>{
      'id': instance.id,
      'itemGroupName': instance.itemGroupName,
      'itemGroupIconUrl': instance.itemGroupIconUrl,
      'isInVitaminFilter': instance.isInVitaminFilter,
    };
