// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrderItem _$OrderItemFromJson(Map<String, dynamic> json) {
  return OrderItem(
    quantity: json['quantity'] as num?,
    medicationId: json['medicationId'] as int?,
    din: json['din'] as String?,
    id: json['id'] as int?,
    status: json['status'] as String?,
    lastName: json['lastName'] as String?,
    drugName: json['drugName'] as String?,
    orderId: json['orderId'] as int?,
    plan: json['plan'] as String?,
    refill: json['refill'] == null
        ? null
        : Rx.fromJson(json['refill'] as Map<String, dynamic>),
    refundId: json['refundId'] as int?,
    rx: json['rx'] == null
        ? null
        : Rx.fromJson(json['rx'] as Map<String, dynamic>),
    shippedQuantity: json['shippedQuantity'] as num?,
  );
}

Map<String, dynamic> _$OrderItemToJson(OrderItem instance) => <String, dynamic>{
      'id': instance.id,
      'orderId': instance.orderId,
      'status': instance.status,
      'lastName': instance.lastName,
      'rx': instance.rx,
      'refill': instance.refill,
      'refundId': instance.refundId,
      'quantity': instance.quantity,
      'medicationId': instance.medicationId,
      'shippedQuantity': instance.shippedQuantity,
      'din': instance.din,
      'drugName': instance.drugName,
      'plan': instance.plan,
    };
