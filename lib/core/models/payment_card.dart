import 'package:json_annotation/json_annotation.dart';

part 'payment_card.g.dart';

@JsonSerializable()
class PaymentCard {
  int? id;
  String? cardType;
  bool? disabled;
  String? expiry;
  String? paymentMode;
  bool? isDefault;
  String? maskedNumber;
  int? patientId;
  String? pharmacyCode;
  int? userId;

  PaymentCard({this.id, this.isDefault, this.patientId, this.disabled, this.pharmacyCode, this.userId, this.cardType, this.expiry, this.maskedNumber, this.paymentMode});

  factory PaymentCard.fromJson(Map<String, dynamic> json) => _$PaymentCardFromJson(json);

  Map<String, dynamic> toJson() => _$PaymentCardToJson(this);
}
