class DashboardArguments {
  final String? snackBarMessage;

  DashboardArguments({this.snackBarMessage});
}
