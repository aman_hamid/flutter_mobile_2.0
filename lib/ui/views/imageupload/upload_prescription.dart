import 'dart:io';

import 'package:basic_utils/basic_utils.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/utils/patient_utils.dart';
import 'package:pocketpills/core/viewmodels/image_upload.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/prescription_model.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/bottomsheets/image_bottom_sheet.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/colors.dart' as prefix0;
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/dialogs/exit_dialog.dart';
import 'package:pocketpills/ui/shared/dialogs/permission_dialog.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/contact/prescription_source_arguments.dart';
import 'package:pocketpills/ui/views/contact/source_arguments.dart';
import 'package:pocketpills/ui/views/contact/user_contact_view.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/imageupload/empty_image_container.dart';
import 'package:pocketpills/ui/views/imageupload/full_image_container.dart';
import 'package:pocketpills/ui/views/imageupload/image_viewer.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/views/profile/profile_health_card_view.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/permission_utils.dart';
import 'package:provider/provider.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_transfer_model.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/ui/views/addmember/add_member_complete.dart';
import 'package:pocketpills/ui/views/consent/consent_arguments.dart';
import 'package:pocketpills/ui/views/consent/wait_consent.dart';
import 'package:pocketpills/ui/views/signup/signup_almost_done_view.dart';
import 'package:pocketpills/ui/views/signup/signup_stepper_arguments.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/ui/shared/pp_avatar.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/ui/shared/container/pp_container.dart' as QuebecContainer;

class UploadPrescription extends StatefulWidget {
  static const routeName = 'upload_prescription';

  BaseStepperSource? source;
  String? medicineName;
  String? from;
  UserPatient? userPatient;
  final SignUpTransferModel? model;
  int? quantity;

  UploadPrescription({this.source, this.medicineName, this.quantity, this.from, this.model, this.userPatient});

  @override
  State<StatefulWidget> createState() {
    return UploadPrescriptionState();
  }
}

class UploadPrescriptionState extends BaseState<UploadPrescription> {
  static int noOfImages = 2;
  List<File> prescriptionPages = [];
  bool isUploadViewPresent = true;
  static const atleastOneImageMessage = "Please upload at least one image to proceed";
  InnerAppBar? appBar;
  ImageUploadModel? model;

  @override
  void initState() {
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.upload);
    super.initState();
    model = ImageUploadModel();
  }

  _onBackPressed(BuildContext context) {
    return ExitDialog.show(context) ?? false;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: model!.getLocalization(["signup", "insurance"]),
        builder: (BuildContext context, AsyncSnapshot<Map<String, dynamic>?> snapshot) {
          if (snapshot.hasData != null && snapshot.data != null) {
            return getMainView(context);
          } else if (snapshot.hasError) {
            return ErrorScreen();
          } else {
            return LoadingScreen();
          }
        });
  }

  Widget getMainView(BuildContext context) {
    bool quebecCheck = false;
    return WillPopScope(
      onWillPop: widget.from == 'telehealth'
          ? widget.source == BaseStepperSource.NEW_USER
              ? () async {
                  Navigator.canPop(context) ? Navigator.pop(context) : _onBackPressed(context);
                  return true;
                }
              : () => _onBackPressed(context)
          : () async {
              Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (route) => false);
              return true;
            },
      child: ChangeNotifierProvider(
        create: (_) => ImageUploadModel(),
        child: Consumer<ImageUploadModel>(builder: (BuildContext context, ImageUploadModel model, Widget? child) {
          String name = StringUtils.capitalize(PatientUtils.getYouOrNameTitle(Provider.of<DashboardModel>(context).selectedPatient));
          name = '${name[0].toUpperCase()}${name.substring(1)}';
          return Scaffold(
            appBar: innerAppbar(widget.from, name),
            bottomNavigationBar: Builder(
              builder: (BuildContext context) => PPBottomBars.getButtonedBottomBar(
                child: model.state == ViewState.Busy
                    ? ViewConstants.progressIndicator
                    : Row(
                        children: <Widget>[
                          SecondaryButton(
                            text: widget.from == "telehealth"
                                ? LocalizationUtils.getSingleValueString("common", "common.button.skip")
                                : LocalizationUtils.getSingleValueString("common", "common.button.back"),
                            onPressed: () {
                              if (widget.from == "telehealth") {
                                onSkipClick(widget.model!);
                              } else {
                                Navigator.pop(context);
                              }
                            },
                          ),
                          SizedBox(width: PPUIHelper.HorizontalSpaceMedium),
                          PrimaryButton(
                              butttonColor: brandColor,
                              text: LocalizationUtils.getSingleValueString("common", "common.button.upload"),
                              onPressed: () {
                                onUploadClick(context, model);
                              })
                        ],
                      ),
              ),
            ),
            body: Builder(
              builder: (BuildContext context) {
                return SafeArea(
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Container(padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: SMALL_XXX), child: imageGrid),
                        PPUIHelper.verticalSpaceMedium(),
                        quebecCheck ? QuebecContainer.PPContainer() : Container(),
                      ],
                    ),
                  ),
                );
              },
            ),
          );
        }),
      ),
    );
  }

  onSkipClick(SignUpTransferModel model) async {
    if (widget.source == BaseStepperSource.NEW_USER || widget.source == BaseStepperSource.ADD_PATIENT) {
      bool success = await model.uploadPrescriptionSignUp();
      print("here");
      if (success == true) {
        print("success");
        dataStore.writeBoolean(DataStoreService.TRANSFER_SKIPPED, true);
        goToNextScreen(model, "skip");
      }
    } else
      Navigator.pop(context);
  }

  onUploadClick(var context, ImageUploadModel model) async {
    var connectivityResult = await model.checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      onFail(context, errMessage: model.noInternetConnection);
      return;
    }

    if (prescriptionPages.length > 0) {
      if (widget.from == "telehealth") {
        bool success = await model.uploadPrescriptionSignUp(prescriptionPages);
        if (success) {
          ////
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_upload_prescription);
          int count = model.prescriptionList!.where((element) => element.prescriptionState!.contains('FILED')).length;
          Map<String, dynamic> leadMap = new Map();
          leadMap['lead_created_by'] = 'mobileApp';
          leadMap['lead_new'] = count > 1 ? 0 : 1;
          leadMap['lead_type'] = 'upload';
          leadMap['lead_origin'] = widget.source == BaseStepperSource.NEW_USER ? 'signup' : 'dashboard';

          if (model.revenue) {
            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.lead_event, leadMap);
          }
          ////

          goToNextScreen(widget.model!, "prescription");
        } else
          model.showSnackBar(context, LocalizationUtils.getSingleValueString("common", "common.label.error-message"));
      } else {
        bool success = await model.uploadPrescription(prescriptionPages, widget.medicineName ?? "", widget.quantity ?? 0);
        if (success) {
          Provider.of<PrescriptionModel>(context, listen: false).clearData();

          //////
          Map<String, dynamic> leadMap = new Map();
          leadMap['lead_created_by'] = 'mobileApp';
          leadMap['lead_new'] = model.totalPrescriptionsCount! > 1 ? 0 : 1;
          leadMap['lead_type'] = 'upload';
          leadMap['lead_origin'] = widget.source != null && widget.source == BaseStepperSource.NEW_USER ? 'signup' : 'dashboard';
          if (model.revenue) {
            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.lead_event, leadMap);
          }
          //////

          analyticsEvents.sendInitiatedCheckoutEvent();
          if (widget.source != null && widget.source == BaseStepperSource.COPAY_REQUEST) {
            Navigator.of(context).pushNamed(ProfileHealthCardView.routeName, arguments: SourceArguments(source: widget.source));
          } else {
            Navigator.of(context).pushReplacementNamed(UserContactWidget.routeName,
                arguments: PrescriptionSourceArguments(source: BaseStepperSource.UPLOAD_PRESCRIPTION_SCREEN, successDetails: model.successDetails));
          }
        } else
          model.showSnackBar(context, LocalizationUtils.getSingleValueString("common", "common.label.error-message"));
      }
    } else {
      model.showSnackBar(context, LocalizationUtils.getSingleValueString("signup", "signup.upload.upload-warning"));
    }
  }

  PreferredSizeWidget? innerAppbar(String? from, String name) {
    if (from == "telehealth") {
      return AppBar(
        backgroundColor: Colors.white,
        title: LocalizationUtils.isProvinceQuebec() == false
            ? Image.asset('graphics/logo-horizontal-dark.png', width: MediaQuery.of(context).size.width * 0.45)
            : Image.network(
                "https://static.pocketpills.com/webapp/images/logo/logo-full-quebec.png",
                width: MediaQuery.of(context).size.width * 0.45,
                fit: BoxFit.cover,
              ),
        centerTitle: true,
      );
    } else {
      return InnerAppBar(
          titleText: (widget.source != null && widget.source != BaseStepperSource.COPAY_REQUEST)
              ? LocalizationUtils.getSingleValueString("signup", "signup.transfer.title-copay-request")
              : name,
          appBar: AppBar(),
          leadingBackButton: () {
            Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
          });
    }
  }

  void goToNextScreen(SignUpTransferModel signUpTransferModel, String from) {
    if (widget.source == BaseStepperSource.NEW_USER) {
      dataStore.writeBoolean(DataStoreService.SIGNUP_PRESCRIPTION, true);
      dataStore.writeBoolean(DataStoreService.COMPLETE_TRANSFER_MODULE, true);
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_contact_detail);
      Navigator.pushNamed(context, SignUpAlmostDoneWidget.routeName, arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER, from: from));
    } else if (widget.source == BaseStepperSource.ADD_PATIENT) {
      Navigator.pushReplacementNamed(context, AddMemberCompleteWidget.routeName, arguments: ConsentArguments(userPatient: widget.userPatient));
    } else if (widget.source == BaseStepperSource.CONSENT_FLOW) {
      Navigator.pushReplacementNamed(context, WaitConsentWidget.routeName, arguments: ConsentArguments(userPatient: widget.userPatient));
    } else if (widget.source == BaseStepperSource.MAIN_SCREEN) {
      Navigator.of(context).pushReplacementNamed(UserContactWidget.routeName,
          arguments: PrescriptionSourceArguments(source: BaseStepperSource.TRANSFER_SCREEN, successDetails: signUpTransferModel.successDetails));
    } else if (widget.source == BaseStepperSource.COPAY_REQUEST) {
      Navigator.of(context).pushNamed(ProfileHealthCardView.routeName, arguments: SourceArguments(source: widget.source));
    }
  }

  Widget get imageGrid {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Flexible(
          child: Padding(
            padding: EdgeInsets.only(left: SMALL_XXX, top: SMALL_XXX),
            child: widget.source == BaseStepperSource.NEW_USER
                ? pharmacistFlow()
                : Text(
                    LocalizationUtils.getSingleValueString("signup", "signup.upload.title"),
                    style: REGULAR_XXX_PRIMARY_BOLD,
                  ),
          ),
        ),
        SizedBox(
          height: REGULAR_XXX,
        ),
        Container(
          child: GridView.count(
            physics: NeverScrollableScrollPhysics(),
            mainAxisSpacing: REGULAR_XX,
            shrinkWrap: true,
            crossAxisCount: 2,
            children: new List<Widget>.generate(noOfImages, (index) {
              return new GridTile(
                  child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: SMALL_XXX, vertical: SMALL),
                child: containerSelector(index),
              ));
            }),
          ),
        ),
      ],
    );
  }

  Widget pharmacistFlow() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Center(
          child: Avatar(displayImage: "https://static.pocketpills.com/dashboard/pharmacist/cathy.jpg"),
        ),
        SizedBox(height: PPUIHelper.VerticalSpaceXMedium),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
          child: Text(
            LocalizationUtils.getSingleValueString("signup", "signup.upload.description-pharmacist"),
            style: TextStyle(color: darkPrimaryColor, fontWeight: FontWeight.w500, fontSize: 16.0, height: 1.5, fontFamily: "FSJoeyPro Medium"),
            textAlign: TextAlign.center,
          ),
        )
      ],
    );
  }

  Widget containerSelector(int index) {
    if (index == noOfImages - 1)
      return addImageContainer;
    else {
      if (prescriptionPages.isEmpty || prescriptionPages.length <= index) {
        return emptyImageContainer(index);
      } else {
        return fullImageContainer(index);
      }
    }
  }

  Widget emptyImageContainer(int index) {
    isUploadViewPresent = true;
    return EmptyImageContainer(
      onTap: () async {
        showImagePickerSelector(index);
      },
      iconText: LocalizationUtils.getSingleValueString("signup", "signup.upload.upload-page") + ' ${index + 1}',
    );
  }

  Widget fullImageContainer(int index) {
    isUploadViewPresent = false;
    return FullImageContainer(
        image: FileImage(prescriptionPages[index]),
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (_) => ImageViewer(image: FileImage(prescriptionPages[index]))));
          Image.file(prescriptionPages[index]);
        },
        onDelete: deleteImage(index));
  }

  Widget get addImageContainer {
    Color widgetColor = tertiaryColor;
    Color borderColor = prefix0.borderColor;
    if (!isUploadViewPresent) {
      widgetColor = linkColor;
      borderColor = linkColor;
    }
    return GestureDetector(
      onTap: () {
        if (!isUploadViewPresent) addImage();
      },
      child: Container(
        height: 160,
        width: 160,
        margin: EdgeInsets.symmetric(horizontal: 8),
        decoration: BoxDecoration(border: Border.all(color: borderColor), borderRadius: BorderRadius.all(Radius.circular(4.0))),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
              Icon(
                Icons.add_circle_outline,
                color: widgetColor,
              ),
            ]),
            PPUIHelper.verticalSpaceSmall(),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Flexible(
                  child: Text(
                    LocalizationUtils.getSingleValueString("signup", "signup.upload.add-page"),
                    style: TextStyle(color: widgetColor, fontSize: 16, fontFamily: "FSJoeyPro"),
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  void addImage() {
    setState(() {
      noOfImages++;
    });
  }

  void getImage(int index, var source) async {
    ImagePicker imagePicker = ImagePicker();
    File image;
    var _image = await imagePicker.getImage(source: source);
    if (_image == null) return;
    image = File(_image.path);
    setState(() {
      prescriptionPages.add(image);
    });
  }

  VoidCallback deleteImage(int index) {
    return () {
      setState(() {
        noOfImages--;
        prescriptionPages.removeAt(index);
      });
    };
  }

  showImagePickerSelector(int index) {
    ImageBottomSheet.getBottomSheet(context, onCameraClick: () async {
      bool successCamera = await PermissionUtils().requestPermission(Permission.camera);
      if (successCamera == true) {
        Navigator.pop(context);
        getImage(index, ImageSource.camera);
      } else {
        PermissionDialog.show(context, "camera");
      }
    }, onGalleryClick: () async {
      PermissionStatus status = await Permission.photos.status;
      bool successPhotos = await PermissionUtils().requestPermission(Permission.photos);
      if (successPhotos == true || status.isLimited) {
        Navigator.pop(context);
        getImage(index, ImageSource.gallery);
      } else {
        Platform.isIOS ? PermissionDialog.show(context, "photos") : PermissionDialog.show(context, "storage");
      }
    });
  }

  SecondaryButton buttonSkip() {
    if (widget.from == "telehealth") {
      return SecondaryButton(
        text: LocalizationUtils.getSingleValueString("common", "common.button.skip"),
        onPressed: () {
          Navigator.pop(context);
          goToNextScreen(widget.model!, "skip");
        },
      );
    } else {
      return SecondaryButton(
        text: LocalizationUtils.getSingleValueString("common", "common.button.back"),
        onPressed: () {
          Navigator.pop(context);
        },
      );
    }
  }
}
