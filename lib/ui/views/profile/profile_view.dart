import 'dart:io';

import 'package:flutter/material.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/core/viewmodels/home_model.dart';
import 'package:pocketpills/core/viewmodels/profile/profile_model.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/appbars/appbar_icons.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/profile/profile_about_view.dart';
import 'package:pocketpills/ui/views/profile/profile_address_view.dart';
import 'package:pocketpills/ui/views/profile/profile_health_card_view.dart';
import 'package:pocketpills/ui/views/profile/profile_health_view.dart';
import 'package:pocketpills/ui/views/profile/profile_insurance_view.dart';
import 'package:pocketpills/ui/views/profile/profile_payment_view.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class ProfileWidget extends StatefulWidget {
  static const routeName = 'profile';

  int? activeIndex;

  ProfileWidget({this.activeIndex = 0});

  @override
  State<StatefulWidget> createState() {
    return ProfileState();
  }
}

class ProfileState extends BaseState<ProfileWidget> with SingleTickerProviderStateMixin {
  ProfileHealthView? healthView;
  ProfileAboutView? aboutView;
  ProfileHealthCardView? healthCardView;
  ProfileInsuranceView? insuranceView;
  ProfileAddressView? addressView;
  ProfilePaymentView? paymentView;

  @override
  void initState() {
    super.initState();
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_profile_view);
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_profile_personal_details);
    healthView = ProfileHealthView(key: UniqueKey());
    aboutView = ProfileAboutView(key: UniqueKey());
    insuranceView = ProfileInsuranceView(
      key: UniqueKey(),
      source: BaseStepperSource.UNKNOWN_SCREEN,
    );
    healthCardView = ProfileHealthCardView(
      key: UniqueKey(),
      source: BaseStepperSource.UNKNOWN_SCREEN,
    );
    addressView = ProfileAddressView(key: UniqueKey());
    paymentView = ProfilePaymentView(
      key: UniqueKey(),
      source: null,
    );
  }

  @override
  Widget build(BuildContext context) {
    double deviceWidth = MediaQuery.of(context).size.width;
    return MultiProvider(
      providers: [ChangeNotifierProvider<ProfileModel>(create: (_) => ProfileModel(widget.activeIndex!))],
      child: Consumer<ProfileModel>(
        builder: (BuildContext context, ProfileModel profileModel, Widget? child) {
          return Scaffold(
            appBar: AppBar(
              backgroundColor: brandColor,
              elevation: 1,
              brightness: Platform.isIOS == true ? Brightness.light : null,
              iconTheme: IconThemeData(color: Colors.white60 //change your color here
                  ),
              centerTitle: false,
              actions: AppBarIcons.getAppBarIcons(context, aboutUs: false),
              leading: IconButton(
                icon: Icon(
                  Icons.home,
                  color: whiteColor,
                ),
                onPressed: () {
                  Provider.of<HomeModel>(context, listen: false).clearData();
                  Navigator.pop(context);
                },
              ),
              title: Text(LocalizationUtils.getSingleValueString("common", "common.navbar.profile"),style: TextStyle(fontFamily: "FSJoeyPro Heavy"),),
              bottom: PreferredSize(
                preferredSize: Size.fromHeight(PPUIHelper.mediumHeight),
                child: Material(
                  elevation: 4,
                  color: whiteColor,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    color: whiteColor,
                    child: Center(
                      child: TabBar(
                          controller: profileModel.getTabController(this),
                          isScrollable: true,
                          indicatorColor: brandColor,
                          labelColor: brandColor,
                          labelStyle: TextStyle(
                              fontSize: deviceWidth <= 320.0
                                  ? 10.5
                                  : deviceWidth >= 410
                                      ? MEDIUM_XXX
                                      : MEDIUM_XX,
                              fontFamily: "FSJoeyPro Medium"
                          ),
                          labelPadding: EdgeInsets.only(right: SMALL_XXX, left: SMALL_XXX),
                          unselectedLabelColor: secondaryColor,
                          tabs: [
                            Tab(child: Text(LocalizationUtils.getSingleValueString("common", "common.navbar.personal"),style: TextStyle(fontFamily: "FSJoeyPro Medium"),)),
                            Tab(
                                child: Text(
                              LocalizationUtils.getSingleValueString("common", "common.navbar.healthcard"),
                                  style: TextStyle(fontFamily: "FSJoeyPro Medium"),
                            )),
                            Tab(
                                child: Text(
                              LocalizationUtils.getSingleValueString("common", "common.navbar.insurance"),
                                  style: TextStyle(fontFamily: "FSJoeyPro Medium"),
                            )),
                            Tab(
                                child: Text(
                              LocalizationUtils.getSingleValueString("common", "common.navbar.address"),
                                  style: TextStyle(fontFamily: "FSJoeyPro Medium"),
                            )),
                            Tab(
                                child: Text(
                              LocalizationUtils.getSingleValueString("common", "common.navbar.payment"),
                                  style: TextStyle(fontFamily: "FSJoeyPro Medium"),
                            )),
                            Tab(
                                child: Text(
                              LocalizationUtils.getSingleValueString("common", "common.navbar.health"),
                                  style: TextStyle(fontFamily: "FSJoeyPro Medium"),
                            )),
                          ]),
                    ),
                  ),
                ),
              ),
            ),
            body: Builder(
              builder: (BuildContext context) => Container(
                child: Builder(builder: (BuildContext context) {
                  return TabBarView(
                    controller: profileModel.getTabController(this),
                    children: [aboutView!, healthCardView!, insuranceView!, addressView!, paymentView!, healthView!],
                  );
                }),
              ),
            ),
          );
        },
      ),
    );
  }
}
