import 'dart:async';
import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:html_unescape/html_unescape.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/patient_add_card_response.dart';
import 'package:pocketpills/core/viewmodels/profile/add_credit_card_model.dart';
import 'package:pocketpills/core/viewmodels/profile/profile_payment_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/no_internet_screen.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';
import 'package:webview_flutter/webview_flutter.dart';

class PaymentDetailWidget extends StatefulWidget {
  static const routeName = 'paymentdetail';

  PaymentDetailWidget({
    Key? key,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return PaymentDetailState();
  }
}

class PaymentDetailState extends BaseState<PaymentDetailWidget> {
  late WebViewController? _webViewController;
  bool defaultValue = true;

  final HttpApi _api = locator<HttpApi>();

  Future<void> addMonerisToken(token) async {
    Response response = await _api.addMonerisToken(token);
    if (response != null) {
      if (response.data != null) {
        BaseResponse<PatientAddCardResponse> res = BaseResponse<PatientAddCardResponse>.fromJson(response.data);
        if (res.response != null) {
          if (newCardId == "newCardId") {
            newCardId = res.response!.cardIds![0].toString();
          }
        }
      }
      Navigator.of(context).pop(true);
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [ChangeNotifierProvider<AddCreditCardModel>(create: (_) => AddCreditCardModel())],
      child: Consumer<AddCreditCardModel>(
        builder: (BuildContext context, AddCreditCardModel creditCardModel, Widget? child) {
          return FutureBuilder(
              future: creditCardModel.getLocalization(["payment"]),
              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                if (snapshot.hasData != null && snapshot.data != null) {
                  return getMainView(creditCardModel);
                } else if (snapshot.hasError) {
                  return ErrorScreen();
                } else {
                  return LoadingScreen();
                }
              });
        },
      ),
    );
  }

  Widget getMainView(AddCreditCardModel creditCardModel) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            LocalizationUtils.getSingleValueString("payment", "payment.label.add-card"),
            style: TextStyle(fontFamily: "FSJoeyPro Heavy"),
          ),
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.pop(context);
              }),
        ),
        body: FutureBuilder(
          future: creditCardModel.getMonarisHtml(),
          //ignore: missing_return
          builder: (BuildContext context, AsyncSnapshot<String?> snapshot) {
            if (creditCardModel.connectivityResult == ConnectivityResult.none && snapshot.connectionState == ConnectionState.done) {
              return NoInternetScreen(
                onClickRetry: creditCardModel.clearAsyncMemorizer,
              );
            }

            if (snapshot.hasData == true && creditCardModel.state == ViewState.Idle && creditCardModel.connectivityResult != ConnectivityResult.none) {
              HtmlUnescape unescape = new HtmlUnescape();
              String strHtml = unescape.convert(snapshot.data!);
              return WillPopScope(
                onWillPop: () async {
                  Navigator.pop(context);
                  return true;
                },
                child: Builder(builder: (BuildContext context) {
                  return WebView(
                    initialUrl: Uri.dataFromString(strHtml, mimeType: 'text/html', encoding: Encoding.getByName('utf-8')).toString(),
                    javascriptMode: JavascriptMode.unrestricted,
                    onWebViewCreated: (WebViewController webViewController) {
                      _webViewController = webViewController;
                    },
                    onProgress: (int progress) {
                      print("WebView is loading (progress : $progress%)");
                    },
                    navigationDelegate: (NavigationRequest request) {
                      if (request.url.startsWith('https://www.youtube.com/')) {
                        print('blocking navigation to $request}');
                        return NavigationDecision.prevent;
                      }
                      print('allowing navigation to $request');
                      return NavigationDecision.navigate;
                    },
                    onPageStarted: (String url) {
                      print('Page started loading: $url');
                    },
                    onPageFinished: (String url) {
                      print('Page finished loading: $url');
                      if (url != null && url.startsWith("https://static.pocketpills.com/emails/static/pocketpills_logo.html")) {
                        List<String> splitTokenUrl = url.split("?/");
                        if (splitTokenUrl.length > 0) {
                          var token = splitTokenUrl[1].split(" ");
                          addMonerisToken(token[0]);
                        }
                      }
                    },
                    gestureNavigationEnabled: true,
                  );
                }),
              );
            } else if (snapshot.hasError && creditCardModel.connectivityResult != ConnectivityResult.none) {
              FirebaseCrashlytics.instance.log(snapshot.hasError.toString());
              return ErrorScreen();
            }

            if (snapshot.connectionState == ConnectionState.active || snapshot.connectionState == ConnectionState.waiting) {
              return LoadingScreen();
            } else
              return Container();
          },
        ));
  }
}
