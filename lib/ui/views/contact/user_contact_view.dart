import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/contact.dart';
import 'package:pocketpills/core/response/prescription/success_details.dart';
import 'package:pocketpills/core/response/signup/EDDresponse.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/utils/patient_utils.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/home_model.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_model.dart';
import 'package:pocketpills/core/viewmodels/user_contact_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/shared/success/base_success_views.dart';
import 'package:pocketpills/ui/shared/success/success_doctor_card_view.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/orderstepper/order_stepper.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class UserContactWidget extends StatefulWidget {
  static const routeName = 'usercontact';
  final BaseStepperSource? source;
  final SuccessDetails? successDetails;
  final bool? fromPocketPills;

  UserContactWidget({Key? key, this.source = BaseStepperSource.TRANSFER_SCREEN, this.successDetails, this.fromPocketPills}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return UserContactState();
  }
}

class UserContactState extends BaseState<UserContactWidget> {
  final DataStoreService dataStore = locator<DataStoreService>();
  late String province;
  late String pincode;
  bool quebecCheck = false;

  @override
  void initState() {
    sendAnalyticsEvent(widget.source!);
    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    province = Provider.of<DashboardModel>(context, listen: false).province ?? "";
    pincode = Provider.of<DashboardModel>(context, listen: false).pincode ?? "";
    quebecCheck = Provider.of<DashboardModel>(context, listen: false).quebecCheck;
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<UserContactModel>(create: (_) => UserContactModel()),
      ],
      child: Consumer<UserContactModel>(
        builder: (BuildContext context, UserContactModel userContactModel, Widget? child) {
          return FutureBuilder(
            future: myFutureMethodOverall(userContactModel),
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              return Container(child: Builder(
                builder: (BuildContext context) {
                  if (snapshot.hasData == true && snapshot.data != null) {
                    return _afterFutureResolved(context, userContactModel);
                  } else if (snapshot.hasError) {
                    FirebaseCrashlytics.instance.log(snapshot.hasError.toString());
                    return ErrorScreen();
                  } else
                    return LoadingScreen();
                },
              ));
            },
          );
        },
      ),
    );
  }

  Future myFutureMethodOverall(UserContactModel model) async {
    Future<Contact?> future1 = model.fetchContact();
    Future<bool?> future2 = model.getEDDList(pincode, province);
    return await Future.wait([future1, future2]);
  }

  Widget _afterFutureResolved(BuildContext context, UserContactModel userContactModel) {
    return WillPopScope(
        onWillPop:() async {
          if(widget.source == BaseStepperSource.MAIN_SCREEN) {
            Navigator.pop(context);
          } else {
            Provider.of<DashboardModel>(context, listen: false).dashboardIndex = 0;
            Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
          }
          return true;
        },
      child: Scaffold(
        bottomNavigationBar: getBottomBar(userContactModel),
        appBar: getAppbar(),
        body: Builder(
          builder: (BuildContext context) => SafeArea(
            child: SingleChildScrollView(child: getMainView(userContactModel, widget.successDetails!)),
          ),
        ),
      ),
    );
  }

  Widget getMainView(UserContactModel userContactModel, SuccessDetails successDetails) {
    return Column(
      children: <Widget>[
        SuccessDoctorCardView(successDetails),
        SizedBox(
          height: REGULAR_XXX,
        ),
        BaseSuccessViews(successDetails.fields!, userContactModel,widget.source),
        SizedBox(
          height: REGULAR_XXX,
        ),
      ],
    );
  }

  Widget getBottomBar(UserContactModel userContactModel) {
    return Builder(
        builder: (BuildContext context) => PPTexts.getBottomBarHeading(
              child: Container(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    SizedBox(
                      height: SMALL_XX,
                    ),
                    (widget.successDetails != null && widget.successDetails!.helpText != null && widget.successDetails!.helpText != "")
                        ? Text(
                            widget.successDetails!.helpText!,
                            style: MEDIUM_XX_PRIMARY,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          )
                        : PPContainer.emptyContainer(),
                    SizedBox(
                      height: MEDIUM_X,
                    ),
                    userContactModel.state == ViewState.Busy
                        ? ViewConstants.progressIndicator
                        : PrimaryButton(
                            fullWidth: true,
                            text: LocalizationUtils.getSingleValueString("common", "common.button.continue"),
                            onPressed: () async {
                              onClickContinue(userContactModel);
                            },
                          ),
                    SizedBox(
                      height: MEDIUM_XXX,
                    ),
                  ],
                ),
              ),
            ));
  }

  PreferredSizeWidget? getAppbar() {
    return InnerAppBar(
      titleText: PatientUtils.getPatientName(Provider.of<DashboardModel>(context).selectedPatient),
      appBar: AppBar(),
      backButtonIcon: Icon(
        Icons.home,
        color: whiteColor,
      ),
      leadingBackButton: () {
        Provider.of<DashboardModel>(context, listen: false).dashboardIndex = 0;
        Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
      },
    );
  }

  void sendAnalyticsEvent(BaseStepperSource source) {
    switch (source) {
      case BaseStepperSource.TRANSFER_SCREEN:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.transfer_intermediate);
        break;
      case BaseStepperSource.REFILL_SCREEN:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.refill_intermediate);
        break;
      case BaseStepperSource.VITAMINS_SCREEN:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.vitamins_intermediate);
        break;
      case BaseStepperSource.UPLOAD_PRESCRIPTION_SCREEN:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.upload_prescription);
        break;
      default:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.new_user_intermediate);
        break;
    }
  }

  void onClickContinue(UserContactModel userContactModel) async {
    bool result = await userContactModel.putContact();
    if (widget.source == BaseStepperSource.TELEHEALTH_SCREEN) {
      Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
    } else {
      if (result == true) {
        bool hasViewTransferRefill = dataStore.readBoolean(DataStoreService.TRANSFER_SKIPPED)!;

        if (widget.source == BaseStepperSource.NEW_USER) {
          if (hasViewTransferRefill == true) {
            Navigator.of(context).pushNamedAndRemoveUntil(OrderStepper.routeName, (Route<dynamic> route) => false,
                arguments: BaseStepperArguments(source: BaseStepperSource.NEW_USER, startStep: 0));
          } else {
            Navigator.of(context).pushNamedAndRemoveUntil(OrderStepper.routeName, (Route<dynamic> route) => false,
                arguments: BaseStepperArguments(source: BaseStepperSource.VITAMINS_SCREEN, startStep: 0));
          }
        } else {
          Navigator.pushReplacementNamed(context, OrderStepper.routeName, arguments: BaseStepperArguments(source: widget.source, startStep: 0));
        }
      } else {
        onFail(context, errMessage: userContactModel.errorMessage);
      }
    }
  }
}
