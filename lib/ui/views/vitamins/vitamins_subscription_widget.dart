import 'package:flutter/material.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/medicine.dart';
import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/core/viewmodels/vitamins/vitamins_catalog_model.dart';
import 'package:pocketpills/core/viewmodels/vitamins/vitamins_subscription_model.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/dialogs/call_dialog.dart';
import 'package:pocketpills/ui/shared/pp_divider.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/vitamins/vitamin_filter_arguments.dart';
import 'package:pocketpills/ui/views/vitamins/vitamins_catalog_widget.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/string_utils.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class VitaminsSubcriptionWidget extends StatefulWidget {
  static const routeName = 'vitaminsSubscriptionWidget';

  final BaseStepperSource? source;

  VitaminsSubcriptionWidget({
    Key? key,
    this.source = BaseStepperSource.UNKNOWN_SCREEN,
  }) : super(key: key);

  @override
  _VitaminsSubcriptionWidgetState createState() => _VitaminsSubcriptionWidgetState();
}

class _VitaminsSubcriptionWidgetState extends BaseState<VitaminsSubcriptionWidget> {
  final GlobalKey<_VitaminsSubcriptionWidgetState> _scaffold = new GlobalKey();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<VitaminsSubscriptionModel>(
      builder: (BuildContext context, VitaminsSubscriptionModel vitaminsSubscriptionModel, Widget? child) {
        return FutureBuilder(
            future: vitaminsSubscriptionModel.getLocalization(["vitamins", "modal"]),
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              if (snapshot.hasData != null && snapshot.data != null) {
                return _showSelectedVitamins(context, vitaminsSubscriptionModel);
              } else if (snapshot.hasError) {
                return ErrorScreen();
              } else {
                return LoadingScreen();
              }
            });
      },
    );
  }

  Widget _showSelectedVitamins(BuildContext context, VitaminsSubscriptionModel vitaminsSubscriptionModel) {
    return Scaffold(
      backgroundColor: whiteColor,
      key: _scaffold,
      appBar: InnerAppBar(
        titleText: LocalizationUtils.getSingleValueString("vitamins", "vitamins.subscription.title"),
        appBar: AppBar(),
      ),
      body: Stack(children: <Widget>[
        Container(
          color: whiteColor,
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: MEDIUM_XXX, right: MEDIUM_XXX, top: REGULAR_XXX),
                child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(SMALL_X)),
                    color: pinkColor,
                  ),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: MEDIUM_XXX, vertical: SMALL_X),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            RichText(
                              text: TextSpan(
                                text: LocalizationUtils.getSingleValueString("vitamins", "vitamins.landing.next-delivery") + '\n',
                                style: MEDIUM_X_MAROON_MEDIUM_BOLD,
                                children: <TextSpan>[
                                  TextSpan(
                                    text: StringUtils.getSubscriptionDate(vitaminsSubscriptionModel.vitaminsSubscriptionResponse!.nextFillDate!) != null
                                        ? StringUtils.getSubscriptionDate(vitaminsSubscriptionModel.vitaminsSubscriptionResponse!.nextFillDate!)
                                        : "",
                                    style: MEDIUM_XXX_WHITE_BOLD,
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                                padding: const EdgeInsets.symmetric(vertical: SMALL_XXX),
                                child: RaisedButton(
                                  color: pinkColor,
                                  elevation: 0.0,
                                  disabledTextColor: Colors.white,
                                  child: Text(
                                    LocalizationUtils.getSingleValueString("vitamins", "vitamins.subscription.cancel").toUpperCase(),
                                    style: MEDIUM_XXX_WHITE_MEDUIM_BOLD,
                                  ),
                                  onPressed: () {
                                    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_cancel_subscription);
                                    showCancelSubscriptionBottomSheet();
                                  },
                                ))
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: SMALL_XXX,
              ),
              Padding(
                padding: const EdgeInsets.only(left: MEDIUM_XXX, right: MEDIUM_XXX, top: MEDIUM_XXX),
                child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(SMALL_X)),
                    color: quartiaryColor,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black26,
                        blurRadius: SMALL_XXX,
                      ),
                    ],
                  ),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: MEDIUM_XXX, vertical: SMALL_X),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              LocalizationUtils.getSingleValueString("vitamins", "vitamins.landing.your-pack"),
                              style: MEDIUM_XXX_PRIMARY_BOLD,
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(vertical: SMALL_X),
                              child: RaisedButton(
                                color: quartiaryColor,
                                disabledTextColor: Colors.white,
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(color: secondaryColor),
                                  borderRadius: BorderRadius.circular(SMALL_XX),
                                ),
                                child: Text(
                                  LocalizationUtils.getSingleValueString("vitamins", "vitamins.subscription.modify").toUpperCase(),
                                  style: MEDIUM_XX_LINK,
                                ),
                                onPressed: () {
                                  analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_modify_subscription);
                                  Provider.of<VitaminsCatalogModel>(context, listen: false).clearVitaminList();
                                  Navigator.pushNamed(context, VitaminsCatalogWidget.routeName, arguments: VitaminFilterArguments(source: widget.source, filterArgu: ""));
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                      ListView.builder(
                        primary: false,
                        shrinkWrap: true,
                        itemCount: vitaminsSubscriptionModel.vitaminsSubscriptionResponse?.shoppingCart?.shoppingCartItems?.length,
                        itemBuilder: (BuildContext ctxt, int index) {
                          Medicine? medicine = vitaminsSubscriptionModel.vitaminsSubscriptionResponse?.shoppingCart?.shoppingCartItems![index].drugDTO;
                          return Container(
                            constraints: BoxConstraints(maxWidth: double.infinity),
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(bottomLeft: Radius.circular(SMALL_X), bottomRight: Radius.circular(SMALL_X)),
                                color: whiteColor,
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  PPDivider(),
                                  Padding(
                                    padding: const EdgeInsets.all(MEDIUM_XXX),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          medicine!.itemName == null ? "" : medicine.itemName!,
                                          style: MEDIUM_XX_PRIMARY_MEDIUM_BOLD,
                                        ),
                                        SizedBox(
                                          height: SMALL,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      )
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: REGULAR_XXX, horizontal: MEDIUM_XXX),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Icon(
                      Icons.info_outline,
                      color: secondaryColor,
                    ),
                    SizedBox(
                      width: SMALL_XXX,
                    ),
                    Expanded(
                      child: Text(
                        LocalizationUtils.getSingleValueString("vitamins", "vitamins.subscription.try-modify"),
                        style: MEDIUM_XX_SECONDARY,
                        maxLines: 3,
                        overflow: TextOverflow.clip,
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
        vitaminsSubscriptionModel.state != ViewState.Busy
            ? Container()
            : Container(color: Colors.white30, child: Center(child: SizedBox(height: LARGE_X, width: LARGE_X, child: CircularProgressIndicator())))
      ]),
    );
  }

  showCancelSubscriptionBottomSheet() {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (BuildContext context) {
          return Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(MEDIUM_X),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        LocalizationUtils.getSingleValueString("modal", "modal.vitamins.title"),
                        style: MEDIUM_XXX_PRIMARY_BOLD,
                      ),
                      SizedBox(
                        height: SMALL_X,
                      ),
                      Text(
                        LocalizationUtils.getSingleValueString("modal", "modal.vitamins.subtitle"),
                        style: MEDIUM_XX_SECONDARY,
                      ),
                    ],
                  ),
                ),
                Divider(
                  color: secondaryColor,
                  height: 1,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: MEDIUM_XXX, vertical: MEDIUM_XXX),
                  child: Row(
                    children: <Widget>[
                      SecondaryButton(
                        isExpanded: false,
                        text: LocalizationUtils.getSingleValueString("modal", "modal.all.close").toUpperCase(),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                      SizedBox(width: PPUIHelper.HorizontalSpaceMedium),
                      PrimaryButton(
                        text: LocalizationUtils.getSingleValueString("modal", "modal.all.call") + " 1-833-HELLO-RX ",
                        onPressed: () {
                          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_call_cancel_subscription);
                          launch("tel://" + ViewConstants.PHARMACY_PHONE.toString());
                          Navigator.of(context).pop();
                        },
                      )
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }
}
