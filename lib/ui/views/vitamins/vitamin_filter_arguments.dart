import 'package:pocketpills/ui/base/base_stepper_arguments.dart';

class VitaminFilterArguments {
  final BaseStepperSource? source;
  final String? filterArgu;

  VitaminFilterArguments({this.source, this.filterArgu});
}
