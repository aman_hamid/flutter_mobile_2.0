import 'package:flutter/material.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/chips/chips_input_local.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/core/enums/ChipValue.dart';
import 'dart:developer' as developer;

import 'chip_file.dart';

class PPInputChipListNew extends StatefulWidget {
  final List<String>? chips;
  final List<String>? totalList;
  final Function? suggestions;
  final String? labelText;

  PPInputChipListNew({Key? key, this.chips, this.totalList, this.suggestions, this.labelText}) : super(key: key);
  final PPInputChipListNewState state = PPInputChipListNewState();

  List<String> getCurrentSelection() {
    return state.currentList;
  }

  @override
  PPInputChipListNewState createState() {
    return state;
  }
}

class PPInputChipListNewState extends State<PPInputChipListNew> {
  late List<String> currentList;

  ChipsInput? chipsInputLocal;
  Key key = UniqueKey();
  GlobalKey<ChipsInputState> _chipKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    currentList = widget.chips!;
  }

  @override
  Widget build(BuildContext context) {
    if (chipsInputLocal == null)
      chipsInputLocal = ChipsInput<String>(
        actionLabel: null,
        key: key,
        width: MediaQuery.of(context).size.width,
        separator: ',',
        decoration: PPInputDecor.getDecoration(collapsed: true, labelText: "(e.g. Lipitor, etc)"),
        initialTags: [],
        autofocus: true,
        inputAction: TextInputAction.done,
        // onChanged: (List<String> data) {
        //   currentList = data;
        //   Chips.chips = data;
        // },
        chipTextValidator: (String value) {
          value.contains('!');
          return -1;
        },
        chipBuilder: (context, state, chipText) {
          return InputChip(
            key: ObjectKey(chipText),
            deleteIconColor: secondaryColor,
            backgroundColor: headerBgColor,
            autofocus: true,
            label: Text(chipText,style: TextStyle(fontFamily: "FSJoeyPro Bold"),),
            labelStyle: TextStyle(
              color: primaryColor,
              fontSize: PPUIHelper.FontSizeSmall,
              height: 1.4,
              fontWeight: FontWeight.bold,
                fontFamily: "FSJoeyPro Bold"
            ),
            onDeleted: () => state.deleteChip(chipText),
            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
          );
        },
        // suggestionBuilder: (context, state, chipText) {
        //   return ListTile(
        //     key: ObjectKey(chipText),
        //     title: Text(chipText),
        //     onTap: () => state.selectSuggestion(chipText),
        //   );
        // },
      );
    return chipsInputLocal!;
  }
}
