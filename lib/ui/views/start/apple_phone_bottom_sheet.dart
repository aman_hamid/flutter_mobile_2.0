import 'package:connectivity/connectivity.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pocketpills/core/enums/signup_stepper_state_enums.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/auth_verification.dart';
import 'package:pocketpills/core/models/phone_verification.dart';
import 'package:pocketpills/core/response/signup_phonenumber_response.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/core/viewmodels/login/login_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_transfer_model.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/login/login_arguments.dart';
import 'package:pocketpills/ui/views/login/login_view.dart';
import 'package:pocketpills/ui/views/login/login_view_identifier.dart';
import 'package:pocketpills/ui/views/signup/signup_stepper_arguments.dart';
import 'package:pocketpills/ui/views/signup/signup_verification_otp_view.dart';
import 'package:pocketpills/ui/views/signup/signup_view.dart';
import 'package:pocketpills/ui/views/signup/transfer_arguments.dart';
import 'package:pocketpills/ui/views/start/authentication.dart';
import 'package:pocketpills/utils/chambers/chambers_redirect.dart';
import 'package:pocketpills/utils/http_api_utils.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/utils/navigation_service.dart';
import 'package:pocketpills/utils/string_utils.dart';
import 'package:url_launcher/url_launcher.dart';

class ApplePhoneSheet extends StatefulWidget {
  BuildContext? context;
  String? userIdentifier;

  ApplePhoneSheet({this.context, this.userIdentifier});

  _ApplePhoneSheetState createState() => _ApplePhoneSheetState();
}

class _ApplePhoneSheetState extends BaseState<ApplePhoneSheet> with SingleTickerProviderStateMixin {
  SignUpTransferModel? model;
  late BuildContext context;

  final TextEditingController _phoneNumberController = TextEditingController();
  final FocusNode _phoneNumberFocusNode = FocusNode();

  _ApplePhoneSheetState();

  late LoginModel model2;

  @override
  void initState() {
    super.initState();
    model2 = LoginModel();
    context = widget.context!;
  }

  // @override
  // void dispose() {
  //   super.dispose();
  // }

  Widget build(BuildContext buildContext) {
    return FutureBuilder(
        future: myFutureMethodOverall(model2),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData != null && snapshot.data != null) {
            return getMainView();
          } else if (snapshot.hasError) {
            return ErrorScreen();
          } else {
            return LoadingScreen();
          }
        });
  }

  Future myFutureMethodOverall(LoginModel model) async {
    Future<Map<String, dynamic>?> future1 = model.getLocalization(["app-landing", "common"]);
    return await Future.wait([future1]);
  }

  Widget getMainView() {
    return Padding(
      padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: Wrap(
        children: [
          Container(
            color: Colors.white,
            child: Padding(
              padding: EdgeInsets.all(20.0),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: MEDIUM_X,
                  ),
                  PPTexts.getHeading(
                    LocalizationUtils.getSingleValueString("app-landing", "app-landing.labels.get-started"),
                    textAlign: TextAlign.start,
                  ),
                  SizedBox(
                    height: MEDIUM_X,
                  ),
                  PPTexts.getSecondaryHeading(
                    LocalizationUtils.getSingleValueString("app-landing", "app-landing.labels.phone-desc"),
                    textAlign: TextAlign.start,
                    isBold: false,
                  ),
                  SizedBox(
                    height: MEDIUM,
                  ),
                  Builder(builder: (context) {
                    return Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        SizedBox(
                          height: MEDIUM_X,
                        ),
                        getUsernameField(context, model2),
                        SizedBox(
                          height: MEDIUM_X,
                        ),
                        model2.state == ViewState.Busy
                            ? ViewConstants.progressIndicator
                            : PrimaryButton(
                                fullWidth: true,
                                disabled: true,
                                butttonColor: lightBlueColor,
                                textColor: brandColor,
                                text: LocalizationUtils.getSingleValueString("app-landing", "app-landing.button.button"),
                                iconData: Icons.arrow_forward,
                                onPressed: () {
                                  onClickProceedSecurely(context, model2);
                                },
                              ),
                        SizedBox(
                          height: SMALL_XX,
                        ),
                        getTnCAndPrivacyPolicyView(),
                        SizedBox(
                          height: MEDIUM_X,
                        ),
                        SizedBox(
                          height: SMALL,
                        ),
                      ],
                    );
                  })
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  TextFormField getUsernameField(BuildContext contextm, LoginModel model) {
    return PPFormFields.getNumericFormField(
        keyboardType: TextInputType.phone,
        textInputAction: TextInputAction.done,
        controller: _phoneNumberController,
        maxLength: 10,
        focusNode: _phoneNumberFocusNode,
        decoration: PPInputDecor.getDecoration(
          labelText: LocalizationUtils.getSingleValueString("app-landing", "app-landing.labels.cell-phone"),
          hintText: LocalizationUtils.getSingleValueString("app-landing", "app-landing.labels.phone-info"),
          prefixIcon: Padding(
              padding: EdgeInsets.all(15),
              child: Text(
                '+1 |',
                style: TextStyle(fontWeight: FontWeight.bold, fontFamily: "FSJoeyPro Medium"),
              )),
        ),
        onFieldSubmitted: (value) {
          onClickProceedSecurely(context, model);
        },
        validator: (value) {
          if (value!.isEmpty) return LocalizationUtils.getSingleValueString("common", "common.label.required") + "*";
          if (StringUtils.isNumericUsingRegularExpression(value.toString())) {
            String formattedNumber = value.replaceAll("\(", "").replaceAll("\)", "").replaceAll(" ", "").replaceAll("-", "").replaceAll("+", "");
            if (formattedNumber.length < 10) return LocalizationUtils.getSingleValueString("app-landing", "app-landing.labels.cell-error1");
            if (formattedNumber.length > 10) return LocalizationUtils.getSingleValueString("app-landing", "app-landing.labels.cell-error2");
          } else {
            if (!StringUtils.isEmail(value.toString())) return LocalizationUtils.getSingleValueString("app-landing", "app-landing.labels.phone-error");
          }
          return null;
        });
  }

  Widget getTnCAndPrivacyPolicyView() {
    TapGestureRecognizer _onTapTandC = TapGestureRecognizer()
      ..onTap = () async {
        if (await canLaunch(getTermsAndCondtions())) {
          await launch(getTermsAndCondtions());
        }
      };
    TapGestureRecognizer _onTapPrivacyPolicy = TapGestureRecognizer()
      ..onTap = () async {
        if (await canLaunch(getPrivacyPolicy())) {
          await launch(getPrivacyPolicy());
        }
      };

    List<String> list = LocalizationUtils.getSingleValueString("app-landing", "app-landing.labels.terms-conditions").split("-");
    String first = list[0];
    String second = list[1];
    String third = list[2];
    String fourth = list[3];

    return RichText(
      text: TextSpan(
        text: first + ' ',
        style: MEDIUM_SECONDARY_PRIVACY,
        children: <TextSpan>[
          TextSpan(text: second + ' ', style: MEDIUM_X_SECONDARY_BOLD_MEDIUM_UNDER_LINE, recognizer: _onTapTandC),
          TextSpan(text: third + ' ', style: MEDIUM_SECONDARY_PRIVACY),
          TextSpan(text: fourth, style: MEDIUM_X_SECONDARY_BOLD_MEDIUM_UNDER_LINE, recognizer: _onTapPrivacyPolicy),
        ],
      ),
    );
  }

  onClickProceedSecurely(BuildContext context, LoginModel loginModel) {
    if (_phoneNumberController.text.isEmpty) {
      Fluttertoast.showToast(
          msg: LocalizationUtils.getSingleValueString("app-landing", "app-landing.labels.phoneerror"), timeInSecForIosWeb: 3, fontSize: 14.0);
    } else {
      String formattedNumber =
          _phoneNumberController.text.replaceAll("\(", "").replaceAll("\)", "").replaceAll(" ", "").replaceAll("-", "").replaceAll("+", "");
      if (StringUtils.isNumericUsingRegularExpression(formattedNumber)) {
        if (formattedNumber.length == 10) {
          checkUserIdentifier(context, loginModel, formattedNumber);
        } else if (formattedNumber.length < 10) {
          Fluttertoast.showToast(
              msg: LocalizationUtils.getSingleValueString("app-landing", "app-landing.labels.cell-error1"), timeInSecForIosWeb: 3, fontSize: 14.0);
        } else {
          Fluttertoast.showToast(
              msg: LocalizationUtils.getSingleValueString("app-landing", "app-landing.labels.cell-error2"), timeInSecForIosWeb: 3, fontSize: 14.0);
        }
      } else {
        if (StringUtils.isEmail(_phoneNumberController.text)) {
          checkUserIdentifier(context, loginModel, _phoneNumberController.text);
        } else {
          Fluttertoast.showToast(
              msg: LocalizationUtils.getSingleValueString("app-landing", "app-landing.labels.email-error"), timeInSecForIosWeb: 3, fontSize: 14.0);
        }
      }
    }
  }

  checkUserIdentifier(BuildContext context, LoginModel loginModel, String phone) async {
    var connectivityResult = await loginModel.checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      onFail(context, errMessage: loginModel.noInternetConnection);
      return;
    }

    Map<String, String> map = new Map();
    map["signupType"] = "phone";
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.landing_proceed, map);
    if (dataStore.readBoolean(DataStoreService.CHAMBERS_FLOW) == true && null == dataStore.readString(DataStoreService.PHONE)) {
      dataStore.writeString(DataStoreService.PHONE, _phoneNumberController.text);
      await HttpApiUtils()
          .chambersActivate(dataStore.readString(DataStoreService.GROUP)!, dataStore.readString(DataStoreService.TOKEN)!, _phoneNumberController.text);

      if (ChambersRedirect.routeName()!.contains('signup')) {
        Navigator.of(context).pushNamedAndRemoveUntil(ChambersRedirect.routeName()!, (Route<dynamic> route) => false,
            arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER, chambersFlow: true));
      }
      if (ChambersRedirect.routeName()!.contains('transfer')) {
        Navigator.of(context).pushNamedAndRemoveUntil(ChambersRedirect.routeName()!, (Route<dynamic> route) => false,
            arguments: TransferArguments(
              source: BaseStepperSource.NEW_USER,
              chambersFlow: true,
            ));
      }
      if (ChambersRedirect.routeName()!.contains('signUpAlmostDoneWidget')) {
        Navigator.of(context).pushNamedAndRemoveUntil(ChambersRedirect.routeName()!, (Route<dynamic> route) => false,
            arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER, chambersFlow: true));
      }
      return;
    }

    PhoneVerification? checkPhone = await loginModel.checkUserIdentifierAuth(widget.userIdentifier, phone);
    if (checkPhone != null) {
      checkLoginProcess(context, loginModel, checkPhone);
    } else {
      onFail(context, errMessage: ApplicationConstant.apiError);
    }
  }

  checkLoginProcess(BuildContext buildContext, LoginModel loginModel, PhoneVerification? checkPhone) async {
    dataStore.writeString(DataStoreService.SIGNUP_TYPE, checkPhone!.signUpType!);
    if (checkPhone.redirect == PhoneNumberStateConstant.EMAIL_LOGIN || checkPhone.redirect == PhoneNumberStateConstant.PHONE_LOGIN) {
      locator<NavigationService>().pushNamed(LoginWidget.routeName,
          argument: LoginArguments(phone: _phoneNumberController.text, loginType: checkPhone.redirect!),
          viewPushEvent: AnalyticsEventConstant.landing_login,
          viewPopEvent: AnalyticsEventConstant.carousel1);
    } else if (checkPhone.redirect == PhoneNumberStateConstant.EMAIL_UNVERIFIED_PHONE_LOGIN_EXISTS ||
        checkPhone.redirect == PhoneNumberStateConstant.PHONE_UNVERIFIED_EMAIL_LOGIN_EXISTS ||
        checkPhone.redirect == PhoneNumberStateConstant.PHONE_UNVERIFIED_GOOGLE_LOGIN_EXISTS ||
        checkPhone.redirect == PhoneNumberStateConstant.PHONE_UNVERIFIED_APPLE_LOGIN_EXISTS) {
      locator<NavigationService>().pushNamed(LoginWidgetIdentifier.routeName,
          argument: LoginArguments(hint: checkPhone.hint, phone: _phoneNumberController.text, loginType: checkPhone.redirect),
          viewPushEvent: AnalyticsEventConstant.landing_login,
          viewPopEvent: AnalyticsEventConstant.carousel1);
    } else if (checkPhone.redirect == PhoneNumberStateConstant.SIGNUP) {
      bool successGetUserInfo = await loginModel.getUserInfo(listenable: true);
      if (successGetUserInfo) {
        locator<NavigationService>().pushNamed(
          SignupWidget.routeName,
          argument: SignupStepperArguments(position: SignupStepperStateEnums.SIGN_UP_ABOUT_YOU, source: BaseStepperSource.NEW_USER),
        );
      }
    } else if (checkPhone.redirect == PhoneNumberStateConstant.PHONE_OTP_FORGOT || checkPhone.redirect == PhoneNumberStateConstant.EMAIL_OTP_FORGOT) {
      var success = await loginModel.getOtpByIdentifier(_phoneNumberController.text);
      if (success == true) {
        locator<NavigationService>().pushNamed(SignUpVerificationOtpPasswordWidget.routeName);
      } else
        onFail(context, errMessage: loginModel.errorMessage);
    }
  }
}
