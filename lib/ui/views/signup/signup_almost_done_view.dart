import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/application/pp_application.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/phone_verification.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/home_model.dart';
import 'package:pocketpills/core/viewmodels/signup/about_you_model.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_model.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_transfer_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/base/scaffold_base.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/constants/localized_data.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/dialogs/exit_dialog.dart';
import 'package:pocketpills/ui/shared/formfields/pp_radiogroup.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_divider.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/login/login_arguments.dart';
import 'package:pocketpills/ui/views/login/login_verification_new.dart';
import 'package:pocketpills/ui/views/login/login_view.dart';
import 'package:pocketpills/ui/views/login/loginverification_view.dart';
import 'package:pocketpills/ui/views/signup/signup_otp_password_view.dart';
import 'package:pocketpills/ui/views/signup/signup_stepper_arguments.dart';
import 'package:pocketpills/ui/views/signup/signup_user_contact_view.dart';
import 'package:pocketpills/ui/views/signup/success_view.dart';
import 'package:pocketpills/ui/views/signup/transfer_arguments.dart';
import 'package:pocketpills/ui/views/signup/verification_arguments.dart';
import 'package:pocketpills/ui/views/transfer/transfer_pharmacy_search_view.dart';
import 'package:pocketpills/utils/analytics.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/string_utils.dart';
import 'package:provider/provider.dart';
import 'package:pocketpills/ui/shared/pp_avatar.dart';

class SignUpAlmostDoneWidget extends StatefulWidget {
  static const routeName = 'signUpAlmostDoneWidget';
  final Function? onSuccess;
  final Function(String)? onShowSnackBar;
  final Function? onBack;
  final BaseStepperSource? source;
  final String? from;
  bool? chambersFlow;

  SignUpAlmostDoneWidget({
    Key? key,
    this.onBack,
    this.onSuccess,
    this.onShowSnackBar,
    this.source,
    this.from,
    this.chambersFlow,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return SignUpAlmostDoneState();
  }
}

class SignUpAlmostDoneState extends BaseState<SignUpAlmostDoneWidget> {
  final DataStoreService dataStore = locator<DataStoreService>();

  PPRadioGroup<String>? selfMedicationRadioGroup;
  PPRadioGroup<String>? manageMedicationRadioGroup;
  PPRadioGroup<String>? genderRadioGroup;
  final _aboutUsformKey = GlobalKey<FormState>();
  String? dropDownValue = null;
  String? dropDownError;
  String prevMobileNumber = "";
  String prevEmailAddress = "";
  String phState = '';
  bool manitobaCheck = true;
  bool showReferral = false;
  bool autovalidate = false;
  bool emAutoValidate = false;
  bool phoneAlreadyRegister = false;
  bool eddCardVisible = false;
  bool emailAlreadyRegister = false;
  String? eddDropDownError;
  String? eddDropDownValue = null;
  String? postalCode;
  String? province;
  String visibleField = "";

  bool completeContactDetails = false;
  bool verified = false;
  bool completeHealthInfo = false;

  static bool gender = false, email = false, referralCode = false;

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _invitationCodeController = TextEditingController();

  final TextEditingController _phoneNumberController = TextEditingController();

  final FocusNode _emailFnode = FocusNode();
  final FocusNode _phoneNumberFocusNode = FocusNode();

  static bool selfMedication = false, manageMedication = false, provincial = false, delivery = false;
  late SignUpAboutYouModel model;
  bool initialize = false;

  @override
  void initState() {
    super.initState();
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_almost_done);
    model = SignUpAboutYouModel();
    province = dataStore.getPharmacyProvinceEdd();
    postalCode = dataStore.getPharmacyZipEdd();
    verified = dataStore.readBoolean(DataStoreService.VERIFIED)!;
    _emailController.addListener(emailListener);
    _invitationCodeController.addListener(referralListener);

    if (!["", null].contains(dataStore.readString(DataStoreService.EMAIL)) && _emailController.text.isEmpty) {
      _emailController.text = dataStore.readString(DataStoreService.EMAIL)!;
    }
    if (!["", null].contains(dataStore.readString(DataStoreService.PHONE)) && _phoneNumberController.text.isEmpty) {
      _phoneNumberController.text = dataStore.readString(DataStoreService.PHONE)!;
    }
    String? referralCode = dataStore.readString(DataStoreService.REFERRAL_CODE);
    String? invitationCode = dataStore.readString(DataStoreService.INVITATION_CODE);
    (referralCode != null || invitationCode != null) ? showReferral = true : showReferral = false;
    _invitationCodeController.text = referralCode != null
        ? referralCode
        : invitationCode != null
            ? invitationCode
            : "";
    String val = getProvinceStorage();
    dropDownValue = val.isEmpty ? null : val;
  }

  @override
  void didChangeDependencies() async {
    _phoneNumberController.addListener(phoneNumberListener);
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _emailController.removeListener(emailListener);
    _invitationCodeController.removeListener(referralListener);
    super.dispose();
  }

  bool isFormValid() {
    bool ge = genderRadioGroup!.validate();
    return ge;
  }

  emailListener() {
    if (email == false && _emailController.text.isNotEmpty) {
      email = true;
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_email_entered);
    }
  }

  referralListener() {
    if (referralCode == false && _invitationCodeController.text.isNotEmpty) {
      referralCode = true;
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_referralcode_entered);
    }
  }

  phoneNumberListener() async {
    if (_phoneNumberController.text.length == 10 && prevMobileNumber.length != _phoneNumberController.text.length) {
      prevMobileNumber = _phoneNumberController.text;
      PhoneVerification? checkPhone = await Provider.of<SignUpModel>(context, listen: false).verifyIdentifier(prevMobileNumber);
      if (checkPhone!.redirect == PhoneNumberStateConstant.PHONE_LOGIN || checkPhone.redirect == PhoneNumberStateConstant.PHONE_OTP_FORGOT) {
        setState(() {
          phoneAlreadyRegister = true;
          phState = checkPhone.redirect!;
        });
      }
    } else if (_phoneNumberController.text.length < 10)
      setState(() {
        phoneAlreadyRegister = false;
        phState = "";
      });
    prevMobileNumber = _phoneNumberController.text;

    if (_phoneNumberController.text.length == SignUpModuleConstant.PHONE_NUMBER_LENGTH) {
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.verify_phone_entered);
    }
  }

  checkEmailAddress(BuildContext context) async {
    if (StringUtils.isEmail(_emailController.text)) {
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.verify_email_entered);
      prevEmailAddress = _emailController.text;
      PhoneVerification? checkPhone = await Provider.of<SignUpModel>(context, listen: false).verifyIdentifier(prevEmailAddress);
      if (checkPhone!.redirect == PhoneNumberStateConstant.EMAIL_LOGIN || checkPhone.redirect == PhoneNumberStateConstant.EMAIL_OTP_FORGOT) {
        setState(() {
          emailAlreadyRegister = true;
          phState = checkPhone.redirect!;
        });
      }
    } else if (!StringUtils.isEmail(_emailController.text))
      setState(() {
        emailAlreadyRegister = false;
        phState = "";
      });
    prevEmailAddress = _emailController.text;
  }

  Widget getGenderUI() {
    return genderRadioGroup!;
  }

  Widget getTakeMedicationUI() {
    return selfMedicationRadioGroup!;
  }

  Widget getManageMedicationUI() {
    return manageMedicationRadioGroup!;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: myFutureMethodOverall(model),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData != null && snapshot.data != null) {
            int exitValue = 0;
            if (dataStore.readInteger(DataStoreService.EXIT_COUNT) != null) {
              exitValue = dataStore.readInteger(DataStoreService.EXIT_COUNT)!;
            }
            return WillPopScope(
                child: getProvider(),
                onWillPop: widget.source == BaseStepperSource.NEW_USER
                    ? () async {
                        if (exitValue != 2) {
                          dataStore.writeInt(DataStoreService.EXIT_COUNT, 0);
                          dataStore.writeInt(DataStoreService.EXIT_COUNT, 1);
                          Navigator.canPop(context) ? Navigator.pop(context) : _onBackPressed(context);
                          return true;
                        } else {
                          _onBackPressed(context);
                          return true;
                        }
                      }
                    : () => _onBackPressed(context));
          } else if (snapshot.hasError) {
            return ErrorScreen();
          } else {
            return LoadingScreen();
          }
        });
  }

  Widget getProvider() {
    if (!initialize) {
      initializeContent();
      initialize = true;
    }

    genderRadioGroup!.initialValue = getGenderValue(dataStore.readString(DataStoreService.GENDER) ?? "");
    return ChangeNotifierProvider(
      create: (_) => SignUpAboutYouModel(),
      child: Consumer<SignUpAboutYouModel>(builder: (BuildContext context, SignUpAboutYouModel model, Widget? child) {
        return getMainView(model);
      }),
    );
  }

  Future myFutureMethodOverall(SignUpAboutYouModel model) async {
    Future<Map<String, dynamic>?> future1 = model.getLocalization(["signup", "forgot", "common", "modal", "modals", "copay", "login"]);
    Future<bool?> future2 = model.getUserInfo();
    Future<bool?>? future3;
    if (widget.from == "prescription") {
      String? value = postalCode = dataStore.getUserPostalCode();
      future3 = model.getEDDList(value!, "");
      return await Future.wait([future1, future2, future3]);
    } else if (widget.from == "transfer") {
      future3 = model.getEDDList(postalCode!, province!);
      return await Future.wait([future1, future2, future3]);
    } else {
      return await Future.wait([future1, future2]);
    }
  }

  _onBackPressed(BuildContext context) {
    ExitDialog.show(context);
  }

  bool isProvinceQuebec() {
    return PPApplication.quebecCheck ? true : false;
  }

  Widget getMainView(SignUpAboutYouModel model) {
    return BaseScaffold(
      appBar: widget.source == BaseStepperSource.NEW_USER
          ? AppBar(
              backgroundColor: Colors.white,
              title: isProvinceQuebec() == true
                  ? Image.network(
                      "https://static.pocketpills.com/webapp/images/logo/logo-full-quebec.png",
                      width: MediaQuery.of(context).size.width * 0.45,
                      fit: BoxFit.cover,
                    )
                  : Image.asset('graphics/logo-horizontal-dark.png', width: MediaQuery.of(context).size.width * 0.45),
              centerTitle: true,
            )
          : null,
      body: GestureDetector(
        onTap: () {
          SystemChannels.textInput.invokeMethod('TextInput.hide');
        },
        child: SafeArea(
          child: Builder(
            builder: (BuildContext context) {
              return Column(
                children: <Widget>[
                  Expanded(child: SingleChildScrollView(child: getStartedView(context, model))),
                  Builder(
                      builder: (BuildContext context) => PPBottomBars.getButtonedBottomBar(
                            child: model.state == ViewState.Busy
                                ? ViewConstants.progressIndicator
                                : PrimaryButton(
                                    text: LocalizationUtils.getSingleValueString("signup", "signup.almostdone.next"),
                                    onPressed: () {
                                      onNextClick(context, model);
                                    },
                                    fullWidth: true),
                          ))
                ],
              );
            },
          ),
        ),
      ),
    );
  }

  Widget getStartedView(BuildContext context, SignUpAboutYouModel model) {
    return getBaseDefaultView(context, model);
  }

/*  void onClickCellPhoneAlreadyRegister(SignUpAboutYouModel model) async {
    if (phState == PhoneNumberStateConstant.PHONE_LOGIN ||
        phState == PhoneNumberStateConstant.EMAIL_LOGIN) {
      Navigator.pushNamedAndRemoveUntil(
          context, LoginWidget.routeName, (Route<dynamic> route) => false,
          arguments: LoginArguments(
              loginType: phState,
              phone: _phoneNumberController.text != null
                  ? _phoneNumberController.text
                  : "",
              source: BaseStepperSource.ABOUT_YOU_SCREEN));
    } else if (phState == PhoneNumberStateConstant.PHONE_OTP_FORGOT ||
        phState == PhoneNumberStateConstant.EMAIL_OTP_FORGOT) {
      var success = await model.getOtp(_phoneNumberController.text);
      if (success == true) {
        Navigator.pushNamedAndRemoveUntil(context,
            LoginVerificationWidgetNew.routeName, (Route<dynamic> route) => false,
            arguments: VerificationArguments(
              userIdentifier: _phoneNumberController.text,
            ));
      } else
        onFail(context, errMessage: model.errorMessage);
    }
  }*/

  Widget getBaseDefaultView(BuildContext context, SignUpAboutYouModel model) {
    return Form(
      key: _aboutUsformKey,
      child: Column(
        children: <Widget>[
          Column(
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(left: MEDIUM_XXX, right: MEDIUM_XXX, top: REGULAR_XXX),
                  child: Column(
                    children: <Widget>[
                      widget.source == BaseStepperSource.NEW_USER ? pharmacistFlow() : normalFlow(),
                      dataStore.readBoolean(DataStoreService.NEW_USER_SEARCH) == true && dataStore.getPharmacyName()!.isNotEmpty
                          ? Container(
                              decoration: BoxDecoration(color: lightGray, borderRadius: BorderRadius.circular(4.0)),
                              child: Padding(
                                padding: EdgeInsets.only(left: 12.0, right: 12.0, top: 12.0, bottom: 12.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start, //change here don't //worked
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          width: 210,
                                          child: Text(
                                            dataStore.getPharmacyName()!,
                                            style: TextStyle(color: primaryColor, fontSize: MEDIUM_XXX, height: 1.4, fontWeight: FontWeight.w500, fontFamily: "FSJoeyPro Heavy"),
                                          ),
                                        ),
                                        Container(
                                          width: 210,
                                          child: Text(
                                            getAddressNoCountry(dataStore.getPharmacyAddress()!),
                                            style: TextStyle(color: primaryColor, fontSize: MEDIUM_XXX, height: 1.4, fontWeight: FontWeight.w500, fontFamily: "FSJoeyPro Medium"),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Spacer(),
                                    InkWell(
                                        onTap: () {
                                          //Navigator.pop(context);
                                          onClickPharmacySearchScreen();
                                        },
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                            LocalizationUtils.getSingleValueString("signup", "signup.otp.phone-change"),
                                            style: TextStyle(color: brandColor, fontSize: MEDIUM_XX, fontWeight: FontWeight.w500, fontFamily: "FSJoeyPro Heavy"),
                                          ),
                                        ))
                                  ],
                                ),
                              ),
                            )
                          : Container(),
                      SizedBox(height: MEDIUM_XXX),
                      widget.from == "prescription" || widget.from == "transfer" ? eddCard() : Container(),
                    ],
                  )),
              Padding(padding: EdgeInsets.only(left: MEDIUM_XXX, right: MEDIUM_XXX, top: SMALL_XXX), child: provinceCard()),
              getManitobaCheck(),
              Padding(
                padding: const EdgeInsets.only(left: MEDIUM_XXX, right: MEDIUM_XXX, top: SMALL_XXX),
                child: getGenderUI(),
              ),
              Padding(
                  padding: EdgeInsets.only(
                    left: MEDIUM_XXX,
                    right: MEDIUM_XXX,
                  ),
                  child: Column(children: <Widget>[
                    SizedBox(height: MEDIUM_XX),
                    getDisplayField(),
                    Padding(padding: EdgeInsets.only(top: SMALL), child: SizedBox(height: SMALL)),
                    PPTexts.getFormLabel(
                        dataStore.readString(DataStoreService.PHONE) == null || dataStore.readString(DataStoreService.PHONE)!.isEmpty
                            ? LocalizationUtils.getSingleValueString("signup", "signup.fields.phone-help")
                            : LocalizationUtils.getSingleValueString("signup", "signup.fields.email-hint"),
                        isBold: false),
                    SizedBox(height: MEDIUM_X),
                    dataStore.readBoolean(DataStoreService.SIGNUP_TELEHEALTH) == false
                        ? Container(
                            decoration: BoxDecoration(color: Color(0xfff7f7f7), borderRadius: BorderRadius.circular(4.0)),
                            child: Column(
                              children: <Widget>[
                                Padding(
                                    padding: EdgeInsets.all(12.0),
                                    child: InkWell(
                                        onTap: () {
                                          setState(() {
                                            showReferral == false ? showReferral = true : showReferral = false;
                                          });
                                        },
                                        child: Row(
                                          children: <Widget>[
                                            Text(
                                              LocalizationUtils.getSingleValueString("signup", "signup.fields.have-referral-code"),
                                              style: TextStyle(fontWeight: FontWeight.w600, fontFamily: "FSJoeyPro Bold"),
                                            ),
                                            new Expanded(
                                                child: Align(
                                              alignment: Alignment.topRight,
                                              child: InkWell(
                                                onTap: () {
                                                  setState(() {
                                                    showReferral == false ? showReferral = true : showReferral = false;
                                                  });
                                                },
                                                child: Image.asset(showReferral == false ? 'graphics/icons/ic_round_arrow_down.png' : 'graphics/icons/ic_round_arrow_up.png',
                                                    width: 24.0, height: 24.0),
                                              ),
                                            )),
                                          ],
                                        ))),
                                getReferralCodeView(),
                              ],
                            ),
                          )
                        : Container(),
                  ])),
              SizedBox(height: MEDIUM_XXX),
            ],
          ),
        ],
      ),
    );
  }

  void onClickPharmacySearchScreen() {
    Navigator.pushNamed(context, TransferPharmacySearchView.routeName, arguments: TransferArguments(source: widget.source, PharmacyNamePopular: dataStore.getPharmacyName()!));
  }

  Widget provinceCard() {
    return dataStore.readBoolean(DataStoreService.SIGNUP_TELEHEALTH) == false
        ? Column(
            children: [
              PPTexts.getFormLabel(LocalizationUtils.getSingleValueString("signup", "signup.fields.provience-label"), isBold: true),
              PPUIHelper.verticalSpaceSmall(),
              getDropDown(),
              dropDownError == null ? SizedBox(height: MEDIUM_XXX) : PPTexts.getFormError(dropDownError!, color: errorColor),
            ],
          )
        : Container();
  }

  Widget eddCard() {
    return widget.from == "transfer" || widget.from == "prescription"
        ? Column(
            children: [
              PPTexts.getFormLabel(LocalizationUtils.getSingleValueString("signup", "signup.fields.edd-label"), isBold: true),
              PPUIHelper.verticalSpaceSmall(),
              getEDDDropDown(),
              eddDropDownError == null ? SizedBox(height: MEDIUM_XXX) : PPTexts.getFormError(eddDropDownError!, color: errorColor),
            ],
          )
        : Container();
  }

  Widget pharmacistFlow() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Center(
          child: Avatar(displayImage: "https://static.pocketpills.com/dashboard/pharmacist/cathy.jpg"),
        ),
        SizedBox(height: PPUIHelper.VerticalSpaceXMedium),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
          child: Text(
            LocalizationUtils.getSingleValueString("signup", "signup.almostdone.description"),
            style: TextStyle(color: darkPrimaryColor, fontWeight: FontWeight.w500, fontSize: 16.0, height: 1.5, fontFamily: "FSJoeyPro Medium"),
            textAlign: TextAlign.center,
          ),
        ),
        SizedBox(height: PPUIHelper.VerticalSpaceXLarge),
      ],
    );
  }

  Widget normalFlow() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        PPTexts.getMainViewHeading(LocalizationUtils.getSingleValueString("signup", "signup.almostdone.title")),
        SizedBox(height: SMALL_X),
        PPTexts.getSecondaryHeading(LocalizationUtils.getSingleValueString("signup", "signup.almostdone.description-telehealth"), isBold: false),
        SizedBox(height: REGULAR_XXX),
      ],
    );
  }

  Widget getReferralCodeView() {
    if (showReferral == true) {
      return AnimatedContainer(
        duration: Duration(seconds: 2),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            PPDivider(),
            PPUIHelper.verticalSpaceXSmall(),
            Padding(
                padding: EdgeInsets.only(left: 12.0, right: 12.0, top: 12.0),
                child: PPFormFields.getTextField(
                    controller: _invitationCodeController,
                    decoration: PPInputDecor.getDecoration(
                      labelText: LocalizationUtils.getSingleValueString("signup", "signup.fields.referral-label"),
                      hintText: LocalizationUtils.getSingleValueString("signup", "signup.fields.enter-referral-code"),
                    ),
                    validator: (value) {
                      return null;
                    })),
            SizedBox(
              height: 3,
            ),
            Padding(
                padding: EdgeInsets.only(left: 12.0, right: 12.0),
                child: Text(
                  LocalizationUtils.getSingleValueString("signup", "signup.fields.referral-hint"),
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.w300, fontFamily: "FSJoeyPro Medium"),
                )),
            SizedBox(
              height: 15,
            )
          ],
        ),
      );
    } else {
      return PPContainer.emptyContainer();
    }
  }

  onNextClick(context, SignUpAboutYouModel signUpAboutYouModel) async {
    var connectivityResult = await signUpAboutYouModel.checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      onFail(context, errMessage: signUpAboutYouModel.noInternetConnection);
      return;
    }
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    onClickNextDefaultBaseView(context, signUpAboutYouModel);
  }

  onClickNextDefaultBaseView(context, SignUpAboutYouModel signUpAboutYouModel) async {
    setState(() {
      autovalidate = true;
    });
    bool dropDownSuccess = true, genderSuccess = true;
    if (!_aboutUsformKey.currentState!.validate()) {
      isFormValid();
      onFail(context, errMessage: LocalizationUtils.getSingleValueString("signup", "signup.employer.fill-details"));
    }
    if (dataStore.readBoolean(DataStoreService.SIGNUP_TELEHEALTH) == false) {
      if (dropDownValue == null || (dropDownValue != null && dropDownValue!.isEmpty)) {
        setState(() {
          dropDownSuccess = false;
          dropDownError = LocalizationUtils.getSingleValueString("signup", "signup.fields.select-province-error");
        });
      }
      if ((dataStore.readBoolean(DataStoreService.TRANSFER_SKIPPED) == false && eddDropDownValue == null) && eddCardVisible ||
          ((eddDropDownValue != null && eddDropDownValue!.isEmpty) && dataStore.readBoolean(DataStoreService.TRANSFER_SKIPPED) == false && eddCardVisible)) {
        setState(() {
          dropDownSuccess = false;
          eddDropDownError = LocalizationUtils.getSingleValueString("signup", "signup.fields.error-required");
        });
      }
    }

    if (!genderRadioGroup!.validate()) {
      genderSuccess = false;
    }

    if (visibleField == "phone") {
      if (_phoneNumberController.text.isEmpty) {
        onFail(context, errMessage: LocalizationUtils.getSingleValueString("signup", "signup.fields.error-phone-required"));
      } else if (_phoneNumberController.text.length == 10) {
        //print('AMANPRO:$dropDownValue');
        //dropDownValue!.toLowerCase() == "quebec" ? PPApplication.quebecCheck = true : PPApplication.quebecCheck = false;
        if (genderSuccess && dropDownSuccess) {
          callFutureActions(context, signUpAboutYouModel);
        }
      } else if (_phoneNumberController.text.length < 10) {
        onFail(context, errMessage: LocalizationUtils.getSingleValueString("signup", "signup.fields.cell-error1"));
      } else {
        onFail(context, errMessage: LocalizationUtils.getSingleValueString("signup", "signup.fields.cell-error2"));
      }
    } else {
      if (StringUtils.isEmail(_emailController.text)) {
        if (genderSuccess && dropDownSuccess) {
          callFutureActions(context, signUpAboutYouModel);
        }
      } else {
        showOnSnackBar(context, successMessage: LocalizationUtils.getSingleValueString("signup", "signup.fields.valid-email"));
      }
    }
  }

  callFutureActions(context, SignUpAboutYouModel signUpAboutYouModel) async {
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_almost_done_submit);
    var success = dataStore.readString(DataStoreService.SIGNUP_TYPE) == "EMAIL_BASED"
        ? await signUpAboutYouModel.updateAboutYou(
            province: dropDownValue,
            selfMedication: selfMedicationRadioGroup!.getValue() == LocalizationUtils.getSingleValueString("common", "common.all.yes").toUpperCase() ? true : false,
            isCaregiver: manageMedicationRadioGroup!.getValue() == LocalizationUtils.getSingleValueString("common", "common.all.yes").toUpperCase() ? true : false,
            pocketPacks: dropDownValue == "manitoba" ? manitobaCheck : null,
            gender: getGenderEnglish(genderRadioGroup!.getValue()),
            phone: _phoneNumberController.text,
            referral: _invitationCodeController.text == "" ? null : _invitationCodeController.text,
            latestPrescriptionDeliveryBy: eddDropDownValue)
        : await signUpAboutYouModel.updateAboutYou(
            province: dropDownValue,
            selfMedication: selfMedicationRadioGroup!.getValue() == LocalizationUtils.getSingleValueString("common", "common.all.yes").toUpperCase() ? true : false,
            isCaregiver: manageMedicationRadioGroup!.getValue() == LocalizationUtils.getSingleValueString("common", "common.all.yes").toUpperCase() ? true : false,
            pocketPacks: dropDownValue == "manitoba" ? manitobaCheck : null,
            gender: getGenderEnglish(genderRadioGroup!.getValue()),
            email: _emailController.text,
            referral: _invitationCodeController.text == "" ? null : _invitationCodeController.text,
            latestPrescriptionDeliveryBy: eddDropDownValue);
    if (success) {
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_almost_done_verification);
      if (dataStore.readBoolean(DataStoreService.COMPLETE_OTP_PASSWORD) == false) {
        if (dataStore.readBoolean(DataStoreService.CHAMBERS_FLOW) == true) {
          Navigator.of(context).pushNamedAndRemoveUntil(SignUpOtpPasswordWidget.routeName, (Route<dynamic> route) => false,
              arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER, chambersFlow: true));
        } else {
          Navigator.of(context).pushNamedAndRemoveUntil(SignUpOtpPasswordWidget.routeName, (Route<dynamic> route) => false,
              arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER, chambersFlow: false));
        }
      } else {
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.signup);
        analyticsEvents.logCompleteRegistrationEvent();
        analyticsEvents.mixPanelIdentifier();
        bool transferSkipped = dataStore.readBoolean(DataStoreService.TRANSFER_SKIPPED)!;
        if (transferSkipped == true) {
          Provider.of<DashboardModel>(context, listen: false).dashboardIndex = 0;
          Provider.of<HomeModel>(context, listen: false).clearData();
          Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
        } else {
          if (dataStore.readBoolean(DataStoreService.SIGNUP_TELEHEALTH) != true) {
            dataStore.writeBoolean(DataStoreService.SIGNUP_PRESCRIPTION, true);
          }
          Provider.of<DashboardModel>(context, listen: false).dashboardIndex = 0;
          Provider.of<HomeModel>(context, listen: false).clearData();
          Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
          //Navigator.of(context).pushNamedAndRemoveUntil(SignUpSuccessWidget.routeName, (Route<dynamic> route) => false, arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER));
        }
      }
    } else {
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_almost_done_verification_failed);
      onFail(context, errMessage: signUpAboutYouModel.errorMessage);
    }
  }

  Widget getManitobaCheck() {
    if (dropDownValue == "manitoba") {
      return InkWell(
        onTap: () {
          setState(() {
            manitobaCheck = !manitobaCheck;
          });
        },
        child: Column(children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Align(
                alignment: Alignment.topLeft,
                child: Checkbox(
                    value: manitobaCheck,
                    activeColor: brandColor,
                    onChanged: (bool? value) {
                      setState(() {
                        manitobaCheck = !manitobaCheck;
                      });
                    }),
              ),
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.only(top: SMALL_XXX),
                  child: Text(
                    LocalizationUtils.getSingleValueString("signup", "signup.fields.packpermission-label"),
                    overflow: TextOverflow.clip,
                    style: TextStyle(height: 1.4, color: secondaryColor, fontSize: 14, fontWeight: FontWeight.w400, fontFamily: "FSJoeyPro"),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: MEDIUM),
        ]),
      );
    } else {
      return PPContainer.emptyContainer();
    }
  }

  Widget getDropDown() {
    if (dropDownError == null)
      return PPFormFields.getDropDown(
        labelText: LocalizationUtils.getSingleValueString("signup", "signup.fields.province"),
        fullWidth: true,
        value: dropDownValue,
        items: getStateMap().map((state) => DropdownMenuItem<String>(value: state["key"], child: Text(state["value"]!))).toList(),
        onChanged: (selectedItem) => setState(() {
          FocusScope.of(context).requestFocus(_emailFnode);
          if (provincial == false) {
            provincial = true;
            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_province_entered);
          }
          dropDownValue = selectedItem;
          setState(() {
            dropDownError = null;
          });
        }),
      );
    else
      return PPFormFields.getDropDown(
        labelText: LocalizationUtils.getSingleValueString("signup", "signup.fields.province"),
        fullWidth: true,
        value: dropDownValue,
        isError: true,
        items: getStateMap().map((state) => DropdownMenuItem<String>(value: state["key"], child: Text(state["value"]!))).toList(),
        onChanged: (selectedItem) => setState(() {
          dropDownValue = selectedItem;
          setState(() {
            dropDownError = null;
          });
        }),
      );
  }

  Widget getEDDDropDown() {
    eddCardVisible = true;
    if (eddDropDownError == null)
      return PPFormFields.getDropDown(
        labelText: LocalizationUtils.getSingleValueString("signup", "signup.transfer.select"),
        fullWidth: true,
        value: eddDropDownValue,
        items: model.eddList.map((state) => DropdownMenuItem<String>(value: state.value, child: Text(state.label))).toList(),
        onChanged: (selectedItem) => setState(() {
          FocusScope.of(context).requestFocus(_emailFnode);
          if (delivery == false) {
            delivery = true;
            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_deliveryBy_entered);
          }
          eddDropDownValue = selectedItem;
          setState(() {
            eddDropDownError = null;
          });
        }),
      );
    else
      return PPFormFields.getDropDown(
        labelText: LocalizationUtils.getSingleValueString("signup", "signup.transfer.select"),
        fullWidth: true,
        value: eddDropDownValue,
        isError: true,
        items: model.eddList.map((state) => DropdownMenuItem<String>(value: state.value, child: Text(state.label))).toList(),
        onChanged: (selectedItem) => setState(() {
          eddDropDownValue = selectedItem;
          setState(() {
            eddDropDownError = null;
          });
        }),
      );
  }

  TextFormField getPhoneField() {
    return PPFormFields.getNumericFormField(
        maxLength: 10,
        keyboardType: TextInputType.phone,
        minLength: 10,
        textInputAction: TextInputAction.next,
        controller: _phoneNumberController,
        focusNode: _phoneNumberFocusNode,
        onFieldSubmitted: (value) {
          onNextClick(context, model);
        },
        validator: (value) {
          if (value!.isEmpty) return LocalizationUtils.getSingleValueString("signup", "signup.fields.cell-phone");
          String formattedNumber = value.replaceAll("\(", "").replaceAll("\)", "").replaceAll(" ", "").replaceAll("-", "").replaceAll("+", "");
          if (formattedNumber.length < 10) return LocalizationUtils.getSingleValueString("signup", "signup.fields.cell-error1");
          if (formattedNumber.length > 10) return LocalizationUtils.getSingleValueString("signup", "signup.fields.cell-error2");
          return null;
        });
  }

  TextFormField getEmailField() {
    return PPFormFields.getTextField(
        autovalidate: autovalidate,
        keyboardType: TextInputType.emailAddress,
        focusNode: _emailFnode,
        controller: _emailController,
        textInputAction: TextInputAction.done,
        onErrorStr: LocalizationUtils.getSingleValueString("common", "common.label.required") + "*",
        decoration: PPInputDecor.getDecoration(
            labelText: LocalizationUtils.getSingleValueString("signup", "signup.fields.email-label"),
            hintText: LocalizationUtils.getSingleValueString("signup", "signup.fields.enter-email")),
        onFieldSubmitted: (value) {
          onNextClick(context, model);
        });
  }

  _fieldFocusChange(BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  void initializeContent() {
    selfMedicationRadioGroup = PPRadioGroup(
      radioOptions: [
        LocalizationUtils.getSingleValueString("common", "common.all.yes").toUpperCase(),
        LocalizationUtils.getSingleValueString("common", "common.all.no").toUpperCase(),
      ],
      labelText: LocalizationUtils.getSingleValueString("signup", "signup.almostdone.medications-daily"),
      errorText: LocalizationUtils.getSingleValueString("common", "common.label.required") + "*",
      onChange: (String value) {
        if (selfMedication == false) {
          selfMedication = true;
          final Analytics analyticsEvents = locator<Analytics>();
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_dailymed_entered);
        }
      },
    );

    manageMedicationRadioGroup = PPRadioGroup(
      radioOptions: [
        LocalizationUtils.getSingleValueString("common", "common.all.yes").toUpperCase(),
        LocalizationUtils.getSingleValueString("common", "common.all.no").toUpperCase(),
      ],
      labelText: LocalizationUtils.getSingleValueString("signup", "signup.almostdone.loved-one-medications"),
      errorText: LocalizationUtils.getSingleValueString("common", "common.label.required") + "*",
      onChange: (String value) {
        if (manageMedication == false) {
          manageMedication = true;
          final Analytics analyticsEvents = locator<Analytics>();
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_iscaregiver_entered);
        }
      },
    );

    genderRadioGroup = PPRadioGroup(
      radioOptions: [
        LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-male").toUpperCase(),
        LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-female").toUpperCase(),
        LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-other").toUpperCase()
      ],
      labelText: LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-label"),
      errorText: LocalizationUtils.getSingleValueString("common", "common.label.required") + "*",
      onChange: (String value) {
        if (gender == false) {
          gender = true;
          final Analytics analyticsEvents = locator<Analytics>();
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_gender_entered);
        }
      },
    );
  }

  String getProvinceStorage() {
    if (dataStore.readString(DataStoreService.PROVINCE) != null) {
      return dataStore.readString(DataStoreService.PROVINCE)!.toLowerCase();
    } else if (dataStore.readString(DataStoreService.PHARMACY_PROVINCE_EDD) != null) {
      return dataStore.readString(DataStoreService.PHARMACY_PROVINCE_EDD)!.toLowerCase();
    } else {
      return '';
    }
  }

  Widget getDisplayField() {
    if (dataStore.readString(DataStoreService.PHONE) == null || dataStore.readString(DataStoreService.PHONE)!.isEmpty) {
      visibleField = "phone";
      return getPhoneField();
    } else {
      visibleField = "email";
      return getEmailField();
    }
  }
}

getGenderEnglish(String? value) {
  if (value == LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-male").toUpperCase()) {
    return "MALE";
  } else if (value == LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-female").toUpperCase()) {
    return "FEMALE";
  } else if (value == LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-other").toUpperCase()) {
    return "OTHER";
  }
}

String getAddressNoCountry(String str) {
  if (str != null && str.length >= 8) {
    str = str.substring(0, str.length - 8);
  }
  return str;
}

String getGenderValue(String gender) {
  String? genderValue;
  switch (gender) {
    case "MALE":
      genderValue = LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-male").toUpperCase();
      break;
    case "FEMALE":
      genderValue = LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-female").toUpperCase();
      break;
    case "OTHER":
      genderValue = LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-other").toUpperCase();
      break;

    default:
      genderValue = "";
  }
  return genderValue;
}
