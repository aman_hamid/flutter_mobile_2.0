import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:pocketpills/application/pp_application.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/insurance.dart';
import 'package:pocketpills/core/models/patient.dart';
import 'package:pocketpills/core/utils/patient_utils.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/profile/profile_insurance_model.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/ui/shared/chips/pp_chip.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/dialogs/exit_dialog.dart';
import 'package:pocketpills/ui/shared/dialogs/permission_dialog.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_divider.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/views/base_stateless_widget.dart';
import 'package:pocketpills/ui/views/contact/source_arguments.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/imageupload/empty_image_container.dart';
import 'package:pocketpills/ui/views/imageupload/full_image_container.dart';
import 'package:pocketpills/ui/views/imageupload/image_viewer.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/permission_utils.dart';
import 'package:pocketpills/utils/rotate_compress_image.dart';
import 'package:provider/provider.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'dart:io';
import 'package:pocketpills/ui/views/contact/prescription_source_arguments.dart';
import 'package:pocketpills/ui/views/contact/user_contact_view.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_transfer_model.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/ui/views/addmember/add_member_complete.dart';
import 'package:pocketpills/ui/views/consent/consent_arguments.dart';
import 'package:pocketpills/ui/views/consent/wait_consent.dart';
import 'package:pocketpills/ui/views/profile/profile_health_card_view.dart';
import 'package:pocketpills/ui/views/signup/signup_almost_done_view.dart';
import 'package:pocketpills/ui/views/signup/signup_stepper_arguments.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/ui/shared/pp_avatar.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';

class HealthCardUploadViewSignUp extends StatefulWidget {
  static const routeName = 'healthCardSignUp';
  BaseStepperSource? source;
  SignUpTransferModel? model;
  UserPatient? userPatient;

  HealthCardUploadViewSignUp({this.source, this.model, this.userPatient});

  @override
  State<StatefulWidget> createState() {
    return _HealthCardUploadViewSignUpState();
  }
}

class _HealthCardUploadViewSignUpState extends BaseState<HealthCardUploadViewSignUp> {
  bool front = false, back = false;
  File? frontImage, backImage;
  @override
  void initState() {
    super.initState();
    if (widget.model == null) {
      widget.model = SignUpTransferModel();
    }
    dataStore.writeBoolean(DataStoreService.COMPLETE_TELEHEALTH_HELTHCARD_UPLOAD, true);
  }

  _onBackPressed(BuildContext context) {
    ExitDialog.show(context);
  }

  @override
  void didChangeDependencies() {
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_healthcard);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: widget.source == BaseStepperSource.NEW_USER
          ? () async {
              Navigator.canPop(context) ? Navigator.pop(context) : _onBackPressed(context);
              return true;
            }
          : () => _onBackPressed(context),
      child: MultiProvider(
        providers: [ChangeNotifierProvider<ProfileInsuranceModel>(create: (_) => ProfileInsuranceModel())],
        child: Consumer<ProfileInsuranceModel>(
          builder: (BuildContext context, ProfileInsuranceModel profileInsuranceModel, Widget? child) {
            return FutureBuilder(
                future: myFutureMethodOverall(profileInsuranceModel, context),
                builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                  if (snapshot.hasData != null && snapshot.data != null) {
                    return _profileInsuranceBuild(context, profileInsuranceModel);
                  } else if (snapshot.hasError) {
                    return ErrorScreen();
                  } else {
                    return LoadingScreen();
                  }
                });
          },
        ),
      ),
    );
  }

  bool isProvinceQuebec() {
    return PPApplication.quebecCheck ? true : false;
  }

  Widget _profileInsuranceBuild(BuildContext context, ProfileInsuranceModel profileInsuranceModel) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: isProvinceQuebec() == true
              ? Image.network(
                  "https://static.pocketpills.com/webapp/images/logo/logo-full-quebec.png",
                  width: MediaQuery.of(context).size.width * 0.45,
                  fit: BoxFit.cover,
                )
              : Image.asset('graphics/logo-horizontal-dark.png', width: MediaQuery.of(context).size.width * 0.45),
          centerTitle: true,
        ),
        bottomNavigationBar: getButtons(profileInsuranceModel),
        body: Builder(
          builder: (context) => Container(
              child: Stack(children: <Widget>[
            SingleChildScrollView(
              child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium, vertical: MEDIUM), child: getUploadInsuranceView(context, profileInsuranceModel)),
            ),
            Center(
              child: profileInsuranceModel.state != ViewState.Busy
                  ? Container()
                  : Container(height: double.infinity, width: double.infinity, color: Colors.white30, child: ViewConstants.progressIndicator),
            )
          ])),
        ));
  }

  Future myFutureMethodOverall(ProfileInsuranceModel profileInsuranceModel, BuildContext context) async {
    //Future<Insurance?> future1 = profileInsuranceModel.fetchInsuranceData(dataStore.readInteger(DataStoreService.PATIENTID)!); // will take 1 sec
    Future<Map<String, dynamic>?> future2 = profileInsuranceModel.getLocalization(["insurance", "order-checkout", "common"]); // will take 3 secs
    return await Future.wait([future2]);
  }

  Widget getUploadInsuranceView(BuildContext context, ProfileInsuranceModel profileInsuranceModel) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        PPUIHelper.verticalSpaceMedium(),
        pharmacistFlow(),
        getProvincialCard(context, profileInsuranceModel),
        Padding(
          padding: const EdgeInsets.only(right: MEDIUM_XXX, bottom: REGULAR_XXX),
          child: GestureDetector(
            onTap: () {
              analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_call_cancel_subscription);
              launch("tel://" + ViewConstants.POCKETPILLS_PHONE.toString());
              Navigator.of(context).pop();
            },
            child: Text(
              LocalizationUtils.getSingleValueString("insurance", "insurance.help.note") + " 1-833-Hello-RX",
              style: MEDIUM_XX_SECONDARY,
            ),
          ),
        ),
        PPUIHelper.verticalSpaceMedium(),
      ],
    );
  }

  Widget getButtons(ProfileInsuranceModel profileInsuranceModel) {
    return Builder(
        builder: (BuildContext context) => PPBottomBars.getButtonedBottomBar(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  PrimaryButton(
                    text: LocalizationUtils.getSingleValueString("common", "common.button.next"),
                    onPressed: () {
                      if (profileInsuranceModel.fstHealth != false || profileInsuranceModel.scndHealth != false) {
                        goToNextScreen(widget.model!);
                      } else
                        showOnSnackBar(context, successMessage: LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.card-mandatory"));
                    },
                  )
                ],
              ),
            ));
  }

  Widget pharmacistFlow() {
    return Column(
      children: [
        Center(
          child: Avatar(displayImage: "https://static.pocketpills.com/dashboard/pharmacist/cathy.jpg"),
        ),
        SizedBox(height: PPUIHelper.VerticalSpaceXMedium),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
          child: Text(
            getDescriptionTextImage(PatientUtils.getForGender(Provider.of<DashboardModel>(context, listen: false).selectedPatient)),
            style: TextStyle(color: darkPrimaryColor, fontWeight: FontWeight.w500, fontSize: 16.0, height: 1.5, fontFamily: "FSJoeyPro Medium"),
            textAlign: TextAlign.center,
          ),
        )
      ],
    );
  }

  Widget getProvincialCard(BuildContext context, ProfileInsuranceModel profileInsuranceModel) {
    List<Widget> children = [];

    children.add(PPUIHelper.verticalSpaceMedium());
    children.add(
      Container(
        height: 200,
        child: GridView.count(
          physics: NeverScrollableScrollPhysics(),
          crossAxisCount: 2,
          crossAxisSpacing: PPUIHelper.HorizontalSpaceMedium,
          children: [
            GridTile(
                child: profileInsuranceModel.curImageUploading == "provincialInsuranceHealthFrontImage"
                    ? ViewConstants.progressIndicator
                    : profileInsuranceModel.fstHealth != false
                        ? getFullImageContainer(context, profileInsuranceModel, "provincialInsuranceHealthFrontImage", frontImage!)
                        : getEmptyContainer(context, profileInsuranceModel, "provincialInsuranceHealthFrontImage", "PROVINCIAL")),
            GridTile(
                child: profileInsuranceModel.curImageUploading == "provincialInsuranceHealthBackImage"
                    ? ViewConstants.progressIndicator
                    : profileInsuranceModel.scndHealth != false
                        ? getFullImageContainer(context, profileInsuranceModel, "provincialInsuranceHealthBackImage", backImage!)
                        : getEmptyContainer(context, profileInsuranceModel, "provincialInsuranceHealthBackImage", "PROVINCIAL"))
          ],
        ),
      ),
    );
    return Container(
      child: Column(
        children: children,
      ),
    );
  }

  getFullImageContainer(BuildContext context, ProfileInsuranceModel profileInsuranceModel, String source, File path) {
    return FullImageContainer(
      image: FileImage(path),
      onPressed: () {
        if (front == false && source.contains("FrontImage")) {
          front = true;
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.healthcard_fr_click);
        } else if (back == false && source.contains("BackImage")) {
          back = true;
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.healthcard_bk_click);
        }
        Navigator.push(context, MaterialPageRoute(builder: (_) => ImageViewer(image: FileImage(path))));
        Image.file(path);
      },
      onDelete: deleteImage(context, profileInsuranceModel, source, 0),
      onEdit: () {
        revealBottomSheet(context, profileInsuranceModel, source);
      },
      healthCard: true,
    );
  }

  deleteImage(BuildContext context, ProfileInsuranceModel profileInsuranceModel, String source, int index) {
    return () async {
      /*prescriptionPages.removeAt(index);
      profileInsuranceModel.deleteImage(source);*/
    };
  }

  Future<void> getImage(BuildContext context, ProfileInsuranceModel profileInsuranceModel, String source, var imgSource) async {
    try {
      ImagePicker imagePicker = ImagePicker();
      File image;
      var _image = await imagePicker.pickImage(source: imgSource);
      if (_image == null) return;
      image = File(_image.path);
      var finalImage = await RotateAndCompressImage().rotateAndCompressAndSaveImage(image);
      if (finalImage == null) return;

      bool connectivityResult = await profileInsuranceModel.isInternetConnected();
      if (connectivityResult == false) {
        onFail(context, errMessage: profileInsuranceModel.noInternetConnection);
        return;
      }

      String? res = await profileInsuranceModel.addImage(finalImage, source);
      if (res != null && res != "") {
        try {
          setState(() {
            if (source == "provincialInsuranceHealthFrontImage") {
              frontImage = finalImage;
              showOnSnackBar(context, successMessage: res);
            } else {
              backImage = finalImage;
              showOnSnackBar(context, successMessage: res);
            }
          });
        } catch (ex) {
          FirebaseCrashlytics.instance.log(ex.toString());
        }
      }
    } catch (ex) {
      FirebaseCrashlytics.instance.log(ex.toString());
    }
  }

  getEmptyContainer(BuildContext context, ProfileInsuranceModel profileInsuranceModel, String source, String sourceType) {
    return EmptyImageContainer(
      onTap: () async {
        if (front == false && source.contains("FrontImage")) {
          front = true;
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.healthcard_fr_click);
        } else if (back == false && source.contains("BackImage")) {
          back = true;
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.healthcard_bk_click);
        }
        bool successStorage = await PermissionUtils().requestPermission(Permission.storage);
        if (successStorage == true) {
          List<Widget> tiles = [];
          if (sourceType != "PROVINCIAL") {
            bool result = await profileInsuranceModel.getCopyInsuranceCandidate();

            if (result == true) {
              if (profileInsuranceModel.copySecondaryInsuranceList != null && profileInsuranceModel.copySecondaryInsuranceList.length > 0) {
                for (int i = 0; i < profileInsuranceModel.copySecondaryInsuranceList.length; i++) {
                  Patient? patient = profileInsuranceModel.copySecondaryInsuranceList[i];
                  tiles.add(
                    ListTile(
                      dense: true,
                      title: Row(
                        children: <Widget>[
                          Text(patient!.firstName! + " " + patient.lastName! + " ", style: MEDIUM_XXX_PRIMARY_BOLD),
                          PPChip(label: LocalizationUtils.getSingleValueString("insurance", "insurance.uploadmodal.secondary"))
                        ],
                      ),
                      onTap: () async {
                        Navigator.pop(context);
                        await profileInsuranceModel.getCopyInsuranceCardFromCandidate(patient.id!, "SECONDARY", sourceType);
                      },
                    ),
                  );
                }
              }

              if (profileInsuranceModel.copyPrimaryInsuranceList != null && profileInsuranceModel.copyPrimaryInsuranceList.length > 0) {
                for (int i = 0; i < profileInsuranceModel.copyPrimaryInsuranceList.length; i++) {
                  Patient? patient = profileInsuranceModel.copyPrimaryInsuranceList[i];
                  tiles.add(
                    ListTile(
                      title: Row(
                        children: <Widget>[
                          Text(
                            patient!.firstName! + " " + patient.lastName! + " ",
                            style: MEDIUM_XXX_PRIMARY_BOLD,
                          ),
                          PPChip(label: LocalizationUtils.getSingleValueString("insurance", "insurance.uploadmodal.primary"))
                        ],
                      ),
                      onTap: () async {
                        Navigator.pop(context);
                        bool result = await profileInsuranceModel.getCopyInsuranceCardFromCandidate(patient.id!, "PRIMARY", sourceType);
                        if (result == true) {
                          showOnSnackBar(context, successMessage: profileInsuranceModel.errorMessage);
                        } else {
                          // onFail(context,
                          //     errMessage: profileInsuranceModel.errorMessage);
                        }
                      },
                    ),
                  );
                }
              }
            }
          }
          showModalBottomSheet<void>(
            context: context,
            isScrollControlled: true,
            builder: (BuildContext context) {
              return new Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  ListTile(
                    title: PPTexts.getHeading(LocalizationUtils.getSingleValueString("insurance", "insurance.labels.choose-an-option")),
                  ),
                  PPDivider(),
                  ListTile(
                    leading: new Icon(
                      Icons.photo_camera,
                      color: primaryColor,
                    ),
                    title: new Text(LocalizationUtils.getSingleValueString("insurance", "insurance.labels.take-photo"), style: MEDIUM_XXX_PRIMARY_BOLD),
                    onTap: () async {
                      bool successCamera = await PermissionUtils().requestPermission(Permission.camera);
                      if (successCamera == true) {
                        Navigator.pop(context);
                        getImage(context, profileInsuranceModel, source, ImageSource.camera);
                      } else {
                        PermissionDialog.show(context, "camera");
                      }
                    },
                  ),
                  ListTile(
                    leading: new Icon(Icons.photo_library, color: primaryColor),
                    title: new Text(LocalizationUtils.getSingleValueString("insurance", "insurance.labels.choose-gallery"), style: MEDIUM_XXX_PRIMARY_BOLD),
                    onTap: () async {
                      PermissionStatus status = await Permission.photos.status;
                      bool successPhotos = await PermissionUtils().requestPermission(Permission.photos);
                      if (successPhotos == true || status.isLimited) {
                        Navigator.pop(context);
                        getImage(context, profileInsuranceModel, source, ImageSource.gallery);
                      } else {
                        Platform.isIOS ? PermissionDialog.show(context, "photos") : PermissionDialog.show(context, "storage");
                      }
                    },
                  ),
                  tiles.length > 0
                      ? Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            PPDivider(),
                            Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Text(LocalizationUtils.getSingleValueString("insurance", "insurance.uploadmodal.label-choose"), style: MEDIUM_XXX_SECONDARY_BOLD_MEDIUM),
                            ),
                          ],
                        )
                      : SizedBox(
                          height: 0.0,
                        ),
                  Column(
                    children: tiles,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                ],
              );
            },
          );
        }
      },
      iconText: source.contains("FrontImage")
          ? LocalizationUtils.getSingleValueString("insurance", "insurance.labels.upload-front")
          : LocalizationUtils.getSingleValueString("insurance", "insurance.labels.upload-back"),
    );
  }

  void revealBottomSheet(BuildContext context, ProfileInsuranceModel profileInsuranceModel, String source) {
    List<Widget> tiles = [];
    showModalBottomSheet<void>(
      context: context,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return new Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListTile(
              title: PPTexts.getHeading(LocalizationUtils.getSingleValueString("insurance", "insurance.labels.choose-an-option")),
            ),
            PPDivider(),
            ListTile(
              leading: new Icon(
                Icons.photo_camera,
                color: primaryColor,
              ),
              title: new Text(LocalizationUtils.getSingleValueString("insurance", "insurance.labels.take-photo"), style: MEDIUM_XXX_PRIMARY_BOLD),
              onTap: () async {
                bool successCamera = await PermissionUtils().requestPermission(Permission.camera);
                if (successCamera == true) {
                  Navigator.pop(context);
                  getImage(context, profileInsuranceModel, source, ImageSource.camera);
                } else {
                  PermissionDialog.show(context, "camera");
                }
              },
            ),
            ListTile(
              leading: new Icon(Icons.photo_library, color: primaryColor),
              title: new Text(LocalizationUtils.getSingleValueString("insurance", "insurance.labels.choose-gallery"), style: MEDIUM_XXX_PRIMARY_BOLD),
              onTap: () async {
                PermissionStatus status = await Permission.photos.status;
                bool successPhotos = await PermissionUtils().requestPermission(Permission.photos);
                if (successPhotos == true || status.isLimited) {
                  Navigator.pop(context);
                  getImage(context, profileInsuranceModel, source, ImageSource.gallery);
                } else {
                  Platform.isIOS ? PermissionDialog.show(context, "photos") : PermissionDialog.show(context, "storage");
                }
              },
            ),
            tiles.length > 0
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      PPDivider(),
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Text(LocalizationUtils.getSingleValueString("insurance", "insurance.uploadmodal.label-choose"), style: MEDIUM_XXX_SECONDARY_BOLD_MEDIUM),
                      ),
                    ],
                  )
                : SizedBox(
                    height: 0.0,
                  ),
            Column(
              children: tiles,
            ),
            SizedBox(
              height: 20,
            ),
          ],
        );
      },
    );
  }

  void goToNextScreen(SignUpTransferModel signUpTransferModel) {
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.healthcard_proceed);
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.healthcard_fr_uploaded);
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.healthcard_bk_uploaded);
    if (widget.source == BaseStepperSource.NEW_USER) {
      dataStore.writeBoolean(DataStoreService.COMPLETE_TRANSFER_MODULE, true);
      dataStore.writeBoolean(DataStoreService.COMPLETE_TELEHEALTH_HELTHCARD_UPLOAD, false);
      dataStore.writeInt(DataStoreService.STEPPER_CURRENT_STEP, 1);
      dataStore.writeBoolean(DataStoreService.SIGNUP_TELEHEALTH, true);
      dataStore.writeBoolean(DataStoreService.DASHBOARD_TELEHEALTH, false);
      dataStore.writeBoolean(DataStoreService.SIGNUP_PRESCRIPTION, false);
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_contact_detail);
      Navigator.pushNamed(context, SignUpAlmostDoneWidget.routeName, arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER, from: "telehealth"));
    } else if (widget.source == BaseStepperSource.ADD_PATIENT) {
      Navigator.pushReplacementNamed(context, AddMemberCompleteWidget.routeName, arguments: ConsentArguments(userPatient: widget.userPatient));
    } else if (widget.source == BaseStepperSource.CONSENT_FLOW) {
      Navigator.pushReplacementNamed(context, WaitConsentWidget.routeName, arguments: ConsentArguments(userPatient: widget.userPatient));
    } else if (widget.source == BaseStepperSource.MAIN_SCREEN) {
      Navigator.of(context).pushReplacementNamed(UserContactWidget.routeName,
          arguments: PrescriptionSourceArguments(source: BaseStepperSource.TRANSFER_SCREEN, successDetails: signUpTransferModel.successDetails));
    } else if (widget.source == BaseStepperSource.COPAY_REQUEST) {
      Navigator.of(context).pushNamed(ProfileHealthCardView.routeName, arguments: SourceArguments(source: widget.source));
    }
  }
}

String getDescriptionTextImage(String gender) {
  switch (gender) {
    case "MALE":
      return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.description-healthcard-MALE");
      break;
    case "FEMALE":
      return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.description-healthcard-FEMALE");
      break;
    case "OTHER":
      return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.description-healthcard-other");
      break;
    default:
      return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.description-healthcard");
      break;
  }
}
