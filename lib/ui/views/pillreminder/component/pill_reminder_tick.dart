import 'package:flutter/material.dart';
import 'package:pocketpills/core/response/pillreminder/pill_status_enum.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';

class PillReminderTick extends StatelessWidget {
  PillStatusType pillStatusType;

  PillReminderTick(@required this.pillStatusType);

  @override
  Widget build(BuildContext context) {
    switch (pillStatusType) {
      case PillStatusType.TAKEN:
        return Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(REGULAR_XX)),
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                blurRadius: SMALL_XXX,
              ),
            ],
            color: successColor,
          ),
          child: Padding(
            padding: const EdgeInsets.all(SMALL_X),
            child: Icon(
              Icons.check,
              color: whiteColor,
              size: 16,
            ),
          ),
        );

        break;
      case PillStatusType.MISSED:
        return Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(REGULAR_XX)),
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                blurRadius: SMALL_XXX,
              ),
            ],
            color: secondaryColor,
          ),
          child: Padding(
            padding: const EdgeInsets.all(SMALL_X),
            child: Icon(
              Icons.close,
              color: whiteColor,
              size: 16,
            ),
          ),
        );
      case PillStatusType.FUTURE:
      case PillStatusType.NONE:
        return PPContainer.emptyContainer();
        break;
    }
  }
}
