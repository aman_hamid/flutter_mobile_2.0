import 'package:flutter/material.dart';
import 'package:pocketpills/core/response/pillreminder/pill_status_enum.dart';
import 'package:pocketpills/core/response/pillreminder/pocket_pack_details.dart';

import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/views/base_stateless_widget.dart';
import 'package:pocketpills/ui/views/pillreminder/component/pill_reminder_tick.dart';
import 'package:pocketpills/utils/analytics_event_constant.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class PillReminderStatusUpdateButton extends BaseStatelessWidget {
  Function? updateTakenStatus;
  Function? updateMissedStatus;
  PocketPackDetails? pocketPackDetails;

  PillReminderStatusUpdateButton({this.updateTakenStatus, this.updateMissedStatus, this.pocketPackDetails});

  @override
  Widget build(BuildContext context) {
    switch (pocketPackDetails!.getPillStatusType()!) {
      case PillStatusType.TAKEN:
      case PillStatusType.MISSED:
      case PillStatusType.NONE:
        return Padding(
          padding: const EdgeInsets.only(left: MEDIUM_XXX, right: MEDIUM_XXX, bottom: MEDIUM_XXX, top: SMALL_XXX),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: FlatButton(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(SMALL_XX)),
                  onPressed: () {
                    if (updateMissedStatus != null) updateMissedStatus!();
                    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_pill_missed);
                  },
                  color: headerBgColor,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      PillReminderTick(PillStatusType.MISSED),
                      Padding(
                        padding: const EdgeInsets.only(left: SMALL_XX),
                        child: Text(
                          LocalizationUtils.getSingleValueString("app-dashboard", "app-dashboard.label.missed").toUpperCase(),
                          style: MEDIUM_XX_SECONDARY_BOLD_MEDIUM,
                        ),
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(
                width: MEDIUM_XXX,
              ),
              Expanded(
                child: FlatButton(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(SMALL_XX)),
                  onPressed: () {
                    if (updateTakenStatus != null) updateTakenStatus!();
                    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_pill_taken);
                  },
                  color: headerBgColor,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      PillReminderTick(PillStatusType.TAKEN),
                      Padding(
                        padding: const EdgeInsets.only(left: SMALL_XX),
                        child: Text(
                          LocalizationUtils.getSingleValueString("app-dashboard", "app-dashboard.label.taken").toUpperCase(),
                          style: MEDIUM_XX_GREEN_BOLD_MEDIUM,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        );
      case PillStatusType.FUTURE:
        return Padding(
          padding: const EdgeInsets.only(left: MEDIUM_XXX, right: MEDIUM_XXX, bottom: MEDIUM_XXX, top: SMALL_XXX),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                  child: InkWell(
                onTap: () {},
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(SMALL_XX)),
                    color: headerBgColor,
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: SMALL_XXX, vertical: MEDIUM),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        PillReminderTick(PillStatusType.MISSED),
                        Padding(
                          padding: const EdgeInsets.only(left: SMALL_XX),
                          child: Text(
                            LocalizationUtils.getSingleValueString("app-dashboard", "app-dashboard.label.missed").toUpperCase(),
                            style: MEDIUM_XX_SECONDARY_BOLD_MEDIUM,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              )),
              SizedBox(
                width: MEDIUM_XXX,
              ),
              Expanded(
                  child: InkWell(
                onTap: () {},
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(SMALL_XX)),
                    color: headerBgColor,
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: SMALL_XXX, vertical: MEDIUM),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(REGULAR_XX)),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black12,
                                blurRadius: SMALL_XXX,
                              ),
                            ],
                            color: secondaryColor,
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(SMALL_X),
                            child: Icon(
                              Icons.check,
                              color: whiteColor,
                              size: 16,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: SMALL_XX),
                          child: Text(
                            LocalizationUtils.getSingleValueString("app-dashboard", "app-dashboard.label.taken").toUpperCase(),
                            style: MEDIUM_XX_SECONDARY_BOLD_MEDIUM,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ))
            ],
          ),
        );
        break;
    }
  }
}
