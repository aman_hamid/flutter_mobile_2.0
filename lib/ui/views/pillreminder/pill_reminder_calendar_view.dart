import 'package:connectivity/connectivity.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/response/pillreminder/day_wise_medications.dart';
import 'package:pocketpills/core/response/pillreminder/month_wise_medication_response.dart';
import 'package:pocketpills/core/response/pillreminder/pocket_pack_details.dart';
import 'package:pocketpills/core/viewmodels/pillreminder/pill_reminder_day_model.dart';
import 'package:pocketpills/core/viewmodels/pillreminder/pill_reminder_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_divider.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/no_internet_screen.dart';
import 'package:pocketpills/ui/views/pillreminder/component/dashed_circle.dart';
import 'package:pocketpills/ui/views/pillreminder/pill_reminder_calendar_day_view.dart';
import 'package:pocketpills/ui/views/pillreminder/pill_reminder_day_wise_icon_status.dart';
import 'package:pocketpills/utils/string_utils.dart';
import 'package:provider/provider.dart';

class PillReminderCalendarView extends StatefulWidget {
  static const routeName = 'pillReminderCalendarView';

  @override
  _PillReminderCalendarViewState createState() => _PillReminderCalendarViewState();
}

class _PillReminderCalendarViewState extends BaseState<PillReminderCalendarView> {
  @override
  void initState() {
    super.initState();
  }

  Widget getWeekDays(String day, bool isWeekEnd) {
    return new Expanded(
        child: Center(
      child: Center(
        child: Text(
          day,
          style: isWeekEnd ? MEDIUM_X_PRIMARY : MEDIUM_X_PRIMARY,
        ),
      ),
    ));
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider<PillReminderModel>(create: (_) => PillReminderModel()),
        ],
        child: Consumer2<PillReminderModel, PillReminderDayModel>(
            builder: (BuildContext context, PillReminderModel pillReminderModel, PillReminderDayModel pillReminderDayModel, Widget? child) {
          return FutureBuilder(
            future: pillReminderModel.fetchPillReminderData(),
            builder: (BuildContext context, AsyncSnapshot<MonthWiseMedicationResponse?> snapshot) {
              if (pillReminderModel.connectivityResult == ConnectivityResult.none && snapshot.connectionState == ConnectionState.done) {
                return NoInternetScreen(
                  onClickRetry: () {},
                );
              }

              if (snapshot.hasData && pillReminderModel.connectivityResult != ConnectivityResult.none) {
                return _afterFutureBuild(context, snapshot.data!, pillReminderModel, pillReminderDayModel);
              } else if (snapshot.hasError && pillReminderModel.connectivityResult != ConnectivityResult.none) {
                FirebaseCrashlytics.instance.log(snapshot.hasError.toString());
                return ErrorScreen();
              }

              if (snapshot.connectionState == ConnectionState.active || snapshot.connectionState == ConnectionState.waiting) {
                return LoadingScreen();
              }
              return PPContainer.emptyContainer();
            },
          );
        }));
  }

  Widget _afterFutureBuild(
      BuildContext context, MonthWiseMedicationResponse monthWiseMedicationResponse, PillReminderModel pillReminderModel, PillReminderDayModel pillReminderDayModel) {
    return Stack(
      children: <Widget>[
        Scaffold(
            backgroundColor: Colors.white,
            appBar: InnerAppBar(
              titleText: "Monthly Progress",
              appBar: AppBar(),
            ),
            body: pillReminderModel.state == ViewState.Busy
                ? Container(child: Center(child: ViewConstants.progressIndicator))
                : Stack(
                    children: <Widget>[
                      ListView(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              InkWell(
                                onTap: () {
                                  pillReminderModel.selectDate(context);
                                },
                                child: Row(
                                  children: <Widget>[
                                    SizedBox(
                                      width: MEDIUM_XXX,
                                    ),
                                    Text(
                                      pillReminderModel.formattedDate,
                                      style: MEDIUM_XXX_PRIMARY_BOLD,
                                    ),
                                    SizedBox(
                                      width: SMALL_X,
                                    ),
                                    Icon(
                                      Icons.arrow_drop_down,
                                      color: primaryColor,
                                    )
                                  ],
                                ),
                              ),
                              Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  InkWell(
                                    onTap: pillReminderModel.previousMonthSelected,
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(horizontal: MEDIUM_X, vertical: SMALL_XXX),
                                      child: Icon(
                                        Icons.chevron_left,
                                        color: primaryColor,
                                        size: 32,
                                      ),
                                    ),
                                  ),
                                  InkWell(
                                    onTap: pillReminderModel.nextMonthSelected,
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(horizontal: MEDIUM_X, vertical: SMALL_XXX),
                                      child: Icon(
                                        Icons.chevron_right,
                                        color: primaryColor,
                                        size: 32,
                                      ),
                                    ),
                                  )
                                ],
                              )
                            ],
                          ),
                          Column(
                            children: <Widget>[
                              PPDivider(),
                              Padding(
                                padding: const EdgeInsets.symmetric(vertical: SMALL_XXX),
                                child: Row(
                                  children: <Widget>[
                                    getWeekDays('SUN', true),
                                    getWeekDays('MON', false),
                                    getWeekDays('TUE', false),
                                    getWeekDays('WED', false),
                                    getWeekDays('THU', false),
                                    getWeekDays('FRI', false),
                                    getWeekDays('SAT', true),
                                  ],
                                  mainAxisSize: MainAxisSize.min,
                                ),
                              ),
                              PPDivider(),
                            ],
                          ),
                          Container(
                            child: GridView.count(
                              crossAxisCount: pillReminderModel.numWeekDays,
                              physics: ScrollPhysics(),
                              childAspectRatio: 1,
                              shrinkWrap: true,
                              scrollDirection: Axis.vertical,
                              children: List.generate(
                                  pillReminderModel.getNumberOfDaysInMonth(
                                    pillReminderModel.dateTime.month,
                                  ), (index) {
                                int dayNumber = index + 1;
                                DayWiseMedications dayWiseMedication =
                                    monthWiseMedicationResponse.dayWiseMedications[(dayNumber - pillReminderModel.beginMonthPadding).toString()]!;
                                return GestureDetector(
                                    onTap: () => pillReminderModel.onDateTapped(dayNumber - pillReminderModel.beginMonthPadding),
                                    child: (pillReminderModel.selectedDay == (dayNumber - pillReminderModel.beginMonthPadding))
                                        ? Container(
                                            decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(SMALL_X)), color: brandColor),
                                            child: Center(child: buildDayNumberSelectedWidget(dayNumber, dayWiseMedication, pillReminderModel)))
                                        : Container(
                                            child: Center(child: buildDayNumberUnselectedWidget(dayNumber, dayWiseMedication, pillReminderModel)),
                                          ));
                              }),
                            ),
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.27,
                          )
                        ],
                      ),
                      getDayMedicationDetails(monthWiseMedicationResponse, pillReminderModel)
                    ],
                  )),
        pillReminderModel.showDayView == false ? PPContainer.emptyContainer() : getDayWiseView(monthWiseMedicationResponse, pillReminderModel, pillReminderDayModel)
      ],
    );
  }

  Widget getDayWiseView(MonthWiseMedicationResponse monthWiseMedicationResponse, PillReminderModel pillReminderModel, PillReminderDayModel pillReminderDayModel) {
    DayWiseMedications dayWiseMedications = monthWiseMedicationResponse.dayWiseMedications[pillReminderModel.selectedDay.toString()]!;
    return PillReminderCalendarDayView(
      pillReminderModel: pillReminderModel,
      dayWiseMedications: dayWiseMedications,
    );
  }

  Widget getDayMedicationDetails(MonthWiseMedicationResponse monthWiseMedicationResponse, PillReminderModel pillReminderModel) {
    if (monthWiseMedicationResponse == null) {
      return PPContainer.emptyContainer();
    }

    int selectedDay = pillReminderModel.selectedDay;
    DayWiseMedications medications = monthWiseMedicationResponse.dayWiseMedications[selectedDay.toString()]!;

    if (medications.pocketPackDetails == null || medications.pocketPackDetails!.length <= 0) {
      return PPContainer.emptyContainer();
    }
    return Align(
      alignment: Alignment.bottomCenter,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            color: headerBgColor,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: SMALL),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  InkWell(
                    onTap: pillReminderModel.previousDaySelected,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: MEDIUM_X, vertical: MEDIUM_XX),
                      child: Icon(
                        Icons.chevron_left,
                        color: primaryColor,
                        size: 32,
                      ),
                    ),
                  ),
                  Column(
                    children: <Widget>[
                      Text(
                        pillReminderModel.getFormattedDateDDMMMYYYY(),
                        style: MEDIUM_XXX_PRIMARY_BOLD,
                      ),
                      SizedBox(
                        height: SMALL,
                      ),
                      Text(
                        "${medications.takenCount} Taken, ${medications.missedCount}  Missed",
                        style: MEDIUM_X_PRIMARY,
                      ),
                    ],
                  ),
                  InkWell(
                    onTap: pillReminderModel.nextDaySelected,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: MEDIUM_X, vertical: MEDIUM_XX),
                      child: Icon(
                        Icons.chevron_right,
                        color: primaryColor,
                        size: 32,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          getPillReminderTimes(monthWiseMedicationResponse, pillReminderModel)
        ],
      ),
    );
  }

  Widget getPillReminderTimes(MonthWiseMedicationResponse monthWiseMedicationResponse, PillReminderModel pillReminderModel) {
    List<Widget> reminderTimeView = [];

    int selectedDay = pillReminderModel.selectedDay;
    DayWiseMedications medications = monthWiseMedicationResponse.dayWiseMedications[selectedDay.toString()]!;

    for (int i = 0; i < medications.pocketPackDetails!.length; i++) {
      reminderTimeView.add(InkWell(
          onTap: () {
            pillReminderModel.showDayView = true;
            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_show_calender_medication);
          },
          child: getPillReminderTime(medications.pocketPackDetails![i])));
    }
    // nexus 5x and ios6
    return Container(
      color: whiteColor,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        padding: MediaQuery.of(context).size.height < 684 ? EdgeInsets.fromLTRB(MEDIUM, MEDIUM, MEDIUM, MEDIUM) : EdgeInsets.fromLTRB(MEDIUM, MEDIUM_XXX, MEDIUM, MEDIUM_XXX),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: reminderTimeView,
        ),
      ),
    );
  }

  Widget getPillReminderTime(PocketPackDetails pocketPackDetail) {
    return Container(
      color: whiteColor,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(SMALL_XXX)),
                border: Border.all(color: bghighlight, width: 1),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black12,
                    blurRadius: SMALL,
                  ),
                ],
                color: whiteColor,
              ),
              child: Padding(
                padding: const EdgeInsets.only(left: MEDIUM_X, right: MEDIUM_XXX, top: MEDIUM_XXX, bottom: MEDIUM_XXX),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    PillReminderDayWiseIconStatus(pocketPackDetail),
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          pocketPackDetail.partOfDay ?? "",
                          style: MEDIUM_XX_PRIMARY_MEDIUM_BOLD,
                        ),
                        SizedBox(
                          height: SMALL,
                        ),
                        Text(
                          StringUtils.getFormattedTimeHHMMNN(pocketPackDetail.time!),
                          style: MEDIUM_X_PRIMARY,
                        ),
                      ],
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget buildDayNumberUnselectedWidget(int dayNumber, DayWiseMedications dayWiseMedication, PillReminderModel pillReminderModel) {
    if (dayNumber <= pillReminderModel.beginMonthPadding) {
      return PPContainer.emptyContainer();
    }

    if (dayWiseMedication == null || dayWiseMedication.pocketPackDetails == null) {
      return PPContainer.emptyContainer();
    }
    return DashedCircle(
      dayWiseMedications: dayWiseMedication,
      dateSelected: false,
      child: Padding(
        padding: const EdgeInsets.all(SMALL_XXX),
        child: Container(
          width: REGULAR_XXX,
          height: REGULAR_XXX,
          child: Center(
            child: new Text(
              (dayNumber - pillReminderModel.beginMonthPadding).toString(),
              textAlign: TextAlign.center,
              style: MEDIUM_XX_PRIMARY_MEDIUM_BOLD,
            ),
          ),
        ),
      ),
    );
  }

  Widget buildDayNumberSelectedWidget(int dayNumber, DayWiseMedications dayWiseMedication, PillReminderModel pillReminderModel) {
    if (dayWiseMedication == null || dayWiseMedication.pocketPackDetails == null) {
      return PPContainer.emptyContainer();
    }
    return DashedCircle(
      dateSelected: true,
      dayWiseMedications: dayWiseMedication,
      child: Padding(
        padding: const EdgeInsets.all(SMALL_XXX),
        child: Container(
          width: REGULAR_XXX,
          height: REGULAR_XXX,
          child: Center(
            child: new Text(
              (dayNumber - pillReminderModel.beginMonthPadding).toString(),
              textAlign: TextAlign.center,
              style: MEDIUM_XX_WHITE_BOLD_MEDIUM,
            ),
          ),
        ),
      ),
    );
  }
}
