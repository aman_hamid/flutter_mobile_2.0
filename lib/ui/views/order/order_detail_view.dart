import 'dart:ui';

import 'package:basic_utils/basic_utils.dart';
import 'package:connectivity/connectivity.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/application/pp_application.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/enums/documents_type.dart';
import 'package:pocketpills/core/models/rx.dart';
import 'package:pocketpills/core/models/shipment.dart';
import 'package:pocketpills/core/response/order_response.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/utils/date_utils.dart';
import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/core/utils/utils.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/order_data_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/chips/pp_chip.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_divider.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/imageupload/image_viewer.dart';
import 'package:pocketpills/ui/views/no_internet_screen.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import '../pdf_viewer.dart';

class OrderDetailWidget extends StatefulWidget {
  static const routeName = 'orderdetail';
  final int? orderId;

  OrderDetailWidget({Key? key, this.orderId}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return OrderDetailState();
  }
}

class OrderDetailState extends BaseState<OrderDetailWidget> {
  num paidAmt = 0;
  HttpApi _api = locator<HttpApi>();
  final DataStoreService dataStore = locator<DataStoreService>();

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => OrderDataModel(),
      child: Consumer<OrderDataModel>(builder: (BuildContext context, OrderDataModel orderDataModel, Widget? child) {
        return FutureBuilder(
            future: myFutureMethodOverall(orderDataModel),
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              if (orderDataModel.connectivityResult == ConnectivityResult.none && snapshot.connectionState == ConnectionState.done) {
                return NoInternetScreen(
                  onClickRetry: orderDataModel.clearAsyncMemorizer,
                );
              }

              if (snapshot.hasData && orderDataModel.connectivityResult != ConnectivityResult.none) {
                return afterFutureResolved(context, snapshot.data[1]);
              } else if (snapshot.hasError && orderDataModel.connectivityResult != ConnectivityResult.none) {
                FirebaseCrashlytics.instance.log(snapshot.hasError.toString());
                return ErrorScreen();
              }

              if (snapshot.connectionState == ConnectionState.active || snapshot.connectionState == ConnectionState.waiting) {
                return LoadingScreen();
              }

              return Container();
            });
      }),
    );
  }

  Future myFutureMethodOverall(OrderDataModel orderDataModel) async {
    Future<Map<String, dynamic>?> future1 = orderDataModel.getLocalization(["modals", "common"]);
    Future<OrderResponse?> future3 = orderDataModel.fetchOrderData(widget.orderId!);
    return await Future.wait([future1, future3]);
  }

  Widget afterFutureResolved(BuildContext context, OrderResponse orderData) {
    return Scaffold(
      appBar: InnerAppBar(
        titleText: "Orders",
        appBar: AppBar(),
      ),
      body: Builder(
        builder: (BuildContext context) => Container(
          child: Builder(
            builder: (BuildContext context) {
              List<Widget> orderDetailsChildren = [];
              orderDetailsChildren.add(PPUIHelper.verticalSpaceMedium());
              orderDetailsChildren.add(Padding(
                padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
                child: PPTexts.getHeading(orderData.orderNo ?? ""),
              ));
              orderDetailsChildren.add(Padding(
                padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
                child: PPTexts.getSecondaryHeading(
                    LocalizationUtils.getSingleValueString("modals", "modals.order.created-label") + ":" + PPDateUtils.displayDate(orderData.orderedDateTime),
                    isBold: false),
              ));
              orderDetailsChildren.add(PPUIHelper.verticalSpaceSmall());
              orderDetailsChildren.add(getShipments(orderData));
              orderDetailsChildren.add(PPUIHelper.verticalSpaceMedium());
              orderDetailsChildren.add(getAddress(orderData));
              orderDetailsChildren.add(PPUIHelper.verticalSpaceMedium());
              orderDetailsChildren.add(getMedications(orderData));
              if (orderData.orderReceiptDocument != null &&
                  (dataStore.readString(DataStoreService.PROVINCE) != null &&
                      !dataStore.readString(DataStoreService.PROVINCE)!.toLowerCase().contains('quebec'))) {
                orderDetailsChildren.add(PPUIHelper.verticalSpaceMedium());
                orderDetailsChildren.add(getDocuments(context, orderData));
              } else if (orderData.orderReceiptDocument != null &&
                  (dataStore.readString(DataStoreService.PROVINCE) != null && PPApplication.quebecCheck == false)) {
                orderDetailsChildren.add(PPUIHelper.verticalSpaceMedium());
                orderDetailsChildren.add(getDocuments(context, orderData));
              }
              orderDetailsChildren.add(PPUIHelper.verticalSpaceMedium());
              orderDetailsChildren.add(getPayment(orderData));

              return SingleChildScrollView(
                child: Column(children: orderDetailsChildren),
              );
            },
          ),
        ),
      ),
    );
  }

  Widget getShipments(OrderResponse? orderData) {
    List<Widget> shipmentChildren = [];

    if (orderData != null && orderData.shipments != null && orderData.shipments!.length > 0) {
      shipmentChildren.add(PPTexts.getSectionHeader(text: LocalizationUtils.getSingleValueString("modals", "modals.order.shipment-label").toUpperCase()));
      for (int i = 0; i < orderData.shipments!.length; i++) {
        Shipment shipment = orderData.shipments![i];
        String statusText = "";
        Color? shipmentDateColor;
        List<Widget> shipItems = [PPUIHelper.verticalSpaceXSmall()];
        if (shipment.items != null && shipment.items!.length > 0) {
          for (int j = 0; j < shipment.items!.length; j++) {
            shipItems.add(Padding(
              padding: const EdgeInsets.symmetric(vertical: PPUIHelper.VerticalSpaceSmall),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  PPTexts.getDescription(shipment.items![j].orderItem!.drugName!, dummy: true),
                  PPTexts.getDescription(Utils.removeDecimalZeroFormat(shipment.items![j].quantity!), dummy: true, textAlign: TextAlign.right)
                ],
              ),
            ));
          }
        }

        if (shipment.returnDate != null) {
          statusText = LocalizationUtils.getSingleValueString("modals", "modals.order.returned") +
              ": " +
              PPDateUtils.displayDate(PPDateUtils.fromStr(shipment.returnDate));
          shipmentDateColor = secondaryColor;
        } else if (shipment.deliveryDate != null) {
          statusText = LocalizationUtils.getSingleValueString("modals", "modals.order.delivery-label") +
              ": " +
              PPDateUtils.displayDate(PPDateUtils.fromStr(shipment.deliveryDate));
          shipmentDateColor = secondaryColor;
        } else if (shipment.attemptedDeliveryDate != null) {
          statusText = LocalizationUtils.getSingleValueString("modals", "modals.order.attemted-delivery") +
              ": " +
              PPDateUtils.displayDate(PPDateUtils.fromStr(shipment.attemptedDeliveryDate));
          shipmentDateColor = errorColor;
        } else if (shipment.expectedDeliveryDate != null) {
          statusText = LocalizationUtils.getSingleValueString("modals", "modals.order.expected-delivery") +
              ": " +
              PPDateUtils.displayDate(PPDateUtils.fromStr(shipment.expectedDeliveryDate));
          shipmentDateColor = secondaryColor;
        } else {
          statusText = "";
        }

        Widget shipmentWidget = Padding(
          padding: const EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
          child: Container(
              child: Column(
            children: <Widget>[
              Container(
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Column(
                        children: <Widget>[
                          PPTexts.getTertiaryHeading(shipment.shipmentNo ?? ""),
                          PPTexts.getDescription(statusText,
                              isBold: true, fontSize: PPUIHelper.FontSizeSmall, fontWeight: FontWeight.bold, color: shipmentDateColor!)
                        ],
                      ),
                    ),
                    PPChip(
                        label: StringUtils.capitalize(ViewConstants.getShipmentStatus(shipment.status)),
                        color: ViewConstants.getColorForShipmentStatus(shipment.status))
                  ],
                ),
              ),
              Container(
                child: Column(children: shipItems),
              ),
            ],
          )),
        );
        shipmentChildren.add(shipmentWidget);
        if (shipment.isTrackingAvailable != null && shipment.isTrackingAvailable!) {
          Widget trackShipment = Padding(
              padding: const EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
              child: FutureBuilder(
                future: getOrderTrackingUrl(shipment.id!),
                builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
                  return Container(
                      child: Column(
                    children: <Widget>[
                      Container(
                          child: FlatButton(
                        child: Row(
                          children: <Widget>[
                            new Text(
                              LocalizationUtils.getSingleValueString("modals", "modals.order.track-label").toUpperCase(),
                              style: TextStyle(
                                color: Color(0xFF0277bd),
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            new Icon(Icons.arrow_forward_ios, size: 12, color: Color(0xFF0277bd)),
                          ],
                        ),
                        onPressed: () {
                          try {
                            _launchURL(snapshot.data.toString());
                          } catch (ex) {
                            FirebaseCrashlytics.instance.log(ex.toString());
                          }
                        },
                      )),
                    ],
                  ));
                },
              ));
          shipmentChildren.add(trackShipment);
        }
        Widget divider = Padding(
          padding: const EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium, vertical: PPUIHelper.VerticalSpaceXSmall),
          child: PPDivider(),
        );

        if (orderData.shipments != null && i != orderData.shipments!.length - 1) {
          shipmentChildren.add(PPUIHelper.verticalSpaceSmall());
          shipmentChildren.add(divider);
          shipmentChildren.add(PPUIHelper.verticalSpaceSmall());
        }
      }
    }
    return Container(child: Column(children: shipmentChildren));
  }

  Widget getAddress(OrderResponse? orderData) {
    List<Widget> addressChildren = [];
    if (orderData != null && orderData.address != null) {
      addressChildren.add(PPTexts.getSectionHeader(text: LocalizationUtils.getSingleValueString("modals", "modals.order.address-label").toUpperCase()));
      addressChildren.add(PPUIHelper.verticalSpaceMedium());
      if (orderData.address!.nickname != null)
        addressChildren.add(Padding(
            padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
            child: PPTexts.getDescription(orderData.address!.nickname!, isBold: true)));
      String addressStr = "";
      if (orderData.address!.streetAddress != null) addressStr += orderData.address!.streetAddress! + ", ";
      if (orderData.address!.streetAddressLineTwo != null) addressStr += orderData.address!.streetAddressLineTwo! + ", ";
      if (orderData.address!.city != null) addressStr += orderData.address!.city! + ", ";
      if (orderData.address!.province != null) addressStr += orderData.address!.province! + ", ";
      if (orderData.address!.postalCode != null) addressStr += orderData.address!.postalCode! + ", ";
      if (orderData.address!.country != null) addressStr += "Canada";
      addressChildren.add(
          Padding(padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium), child: PPTexts.getSecondaryHeading(addressStr, isBold: false)));
      addressChildren.add(Padding(
          padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
          child: PPTexts.getSecondaryHeading(
              LocalizationUtils.getSingleValueString("modals", "modals.prescription.phone") + ": " + orderData.caregiver!.phone.toString(),
              isBold: false)));
    }
    return Container(child: Column(children: addressChildren));
  }

  Widget getMedications(OrderResponse? orderData) {
    List<Widget> medicationsChildren = [];
    if (orderData != null && null != orderData.orderItems && orderData.orderItems!.length > 0) {
      medicationsChildren.add(PPTexts.getSectionHeader(text: LocalizationUtils.getSingleValueString("modals", "modals.order.medications").toUpperCase()));
      medicationsChildren.add(PPUIHelper.verticalSpaceMedium());

      for (int i = 0; i < orderData.orderItems!.length; i++) {
        List<Widget> medicinesColumn = [];
        Rx? med = orderData.orderItems!.elementAt(i).rx;
        Rx? refillMed = orderData.orderItems!.elementAt(i).refill;
        if (med == null) continue;
        if (refillMed != null && refillMed.copay != null) paidAmt += refillMed.copay!;
        medicinesColumn.add(PPTexts.getTertiaryHeading(med.drug != null ? med.drug! : "", isBold: true));
        if (refillMed != null && refillMed.quantity != null) {
          medicinesColumn.add(PPTexts.getDescription(
              LocalizationUtils.getSingleValueString("modals", "modals.order.quantity") + ": " + Utils.removeDecimalZeroFormat(refillMed.quantity!)));
        }
        medicinesColumn.add(PPUIHelper.verticalSpaceMedium());
        List<Widget> priceList = [];
        if (refillMed != null) {
          refillMed.pharmaCare != null
              ? priceList.add(Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      LocalizationUtils.getSingleValueString("modals", "modals.order.government").toUpperCase(),
                      style: MEDIUM_XX_SECONDARY,
                    ),
                    Text(
                      r'$' + refillMed.pharmaCare.toString(),
                      style: MEDIUM_XX_PRIMARY_BOLD,
                    )
                  ],
                ))
              : null;
          refillMed.insurance != null
              ? priceList.add(Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      LocalizationUtils.getSingleValueString("modals", "modals.order.insurance").toUpperCase(),
                      style: MEDIUM_XX_SECONDARY,
                    ),
                    Text(
                      r'$' + refillMed.insurance.toString(),
                      style: MEDIUM_XX_PRIMARY_BOLD,
                    )
                  ],
                ))
              : null;

          refillMed.copay != null
              ? priceList.add(Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      LocalizationUtils.getSingleValueString("modals", "modals.order.copay").toUpperCase(),
                      style: MEDIUM_XX_SECONDARY,
                    ),
                    Text(
                      r'$' + refillMed.copay.toString(),
                      style: MEDIUM_XX_PRIMARY_BOLD,
                    )
                  ],
                ))
              : null;
          medicinesColumn.add(
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: priceList),
          );
        }
        if (i != orderData.orderItems!.length - 1) medicinesColumn.add(Divider(height: 24));
        medicationsChildren.add(Padding(
          padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
          child: Container(
            child: Column(
              children: medicinesColumn,
            ),
          ),
        ));
      }
    }
    return Container(child: Column(children: medicationsChildren));
  }

  Widget getPayment(OrderResponse? orderData) {
    List<Widget> shipmentChildren = [];
    if (orderData != null && orderData.orderPayment != null && orderData.orderPayment!.paymentDetails != null && paidAmt != 0) {
      shipmentChildren.add(Row(
        children: <Widget>[
          Expanded(
            child: Column(
              children: <Widget>[
                PPTexts.getDescription(LocalizationUtils.getSingleValueString("modals", "modals.order.total-amount"), isBold: true),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    orderData.orderPayment!.paymentDetails!.paymentMode == PaymentMode.MY_HSA
                        ? Padding(
                            padding: const EdgeInsets.only(top: SMALL),
                            child: Image.asset(
                              Utils.cardTypeToImage(orderData.orderPayment!.paymentDetails!.paymentMode!),
                              width: LARGE_X,
                            ),
                          )
                        : Image.asset(
                            Utils.cardTypeToImage(orderData.orderPayment!.paymentDetails!.cardType),
                            width: REGULAR_X,
                          ),
                    PPUIHelper.horizontalSpaceSmall(),
                    PPTexts.getDescription(
                        LocalizationUtils.getSingleValueString("modals", "modals.order.ending") +
                            " " +
                            orderData.orderPayment!.paymentDetails!.maskedNumber.toString(),
                        justText: true)
                  ],
                ),
              ],
            ),
          ),
          Expanded(
            child: PPTexts.getMainViewHeading("\$" + orderData.orderPayment!.amount.toString(),
                textAlign: TextAlign.right, mainAxisAlignment: MainAxisAlignment.end),
          )
        ],
      ));
    }
    return Padding(
      padding: const EdgeInsets.only(bottom: PPUIHelper.HorizontalSpaceMedium, left: PPUIHelper.HorizontalSpaceMedium, right: PPUIHelper.HorizontalSpaceMedium),
      child: Container(
        child: Column(
          children: shipmentChildren,
        ),
      ),
    );
  }

  Future<String> getOrderTrackingUrl(int orderId) async {
    String? response = await _api.getOrderTrackURl(orderId);
    return response!;
  }

  Widget getDocuments(BuildContext context, OrderResponse? orderData) {
    List<Widget> documentsChildren = [];
    List<Widget> docsChildren = [];
    if (orderData != null && orderData.orderReceiptDocument != null) {
      if (orderData.orderReceiptDocument!.documentType == DocumentsType.IMAGE) {
        docsChildren.add(new GestureDetector(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (_) => ImageViewer(image: NetworkImage(orderData.orderReceiptDocument!.documentPath!))));
            },
            child: Container(height: 100, child: Image.network(orderData.orderReceiptDocument!.documentPath!, height: 100))));
      } else if (orderData.orderReceiptDocument!.documentType == DocumentsType.PDF) {
        print(orderData.orderReceiptDocument!.documentPath);
        docsChildren.add(new GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (_) => PDFViewer(
                            pdfUrl: orderData.orderReceiptDocument!.documentPath,
                            order: true,
                          )));
            },
            child: Container(height: 100, child: new Icon(Icons.picture_as_pdf, size: 100))));
      } else {
        docsChildren.add(new GestureDetector(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (_) => ImageViewer(image: NetworkImage(orderData.orderReceiptDocument!.documentPath!))));
            },
            child: Container(height: 100, child: Image.network(orderData.orderReceiptDocument!.documentPath!, height: 100))));
      }
      documentsChildren.add(PPTexts.getSectionHeader(text: 'ORDER RECEIPT'));
      if (orderData.orderReceiptDocument != null) {
        documentsChildren.add(Padding(
          padding: const EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
          child: Container(
            height: (MediaQuery.of(context).size.width / 3) * ((docsChildren.length / 3).ceil()),
            child: GridView.count(
              //mainAxisSpacing: PPUIHelper.HorizontalSpaceMedium,
              mainAxisSpacing: 0,
              crossAxisCount: 3,
              children: docsChildren,
            ),
          ),
        ));
      }
      return Container(child: Column(children: documentsChildren));
    }
    return PPContainer.emptyContainer();
  }

  _launchURL(String orderTrackurl) async {
    if (orderTrackurl != null) {
      try {
        if (await canLaunch(orderTrackurl)) {
          await launch(orderTrackurl);
        } else {
          throw 'Could not launch $orderTrackurl';
        }
      } catch (ex) {
        FirebaseCrashlytics.instance.log(ex.toString());
      }
    }
  }
}
