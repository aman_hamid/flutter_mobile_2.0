import 'dart:io';

import 'package:basic_utils/basic_utils.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/models/order.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/utils/date_utils.dart';
import 'package:pocketpills/core/utils/patient_utils.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/home_model.dart';
import 'package:pocketpills/core/viewmodels/order_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/cards/pp_card.dart';
import 'package:pocketpills/ui/shared/chips/pp_chip.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/dialogs/exit_dialog.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/empty_screen.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/order/order_arguments.dart';
import 'package:pocketpills/ui/views/order/order_detail_view.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';
import 'package:pocketpills/ui/shared/container/pp_container.dart' as QuebecContainer;

class OrderWidget extends StatefulWidget {
  static const routeName = 'order';
  final int? bottomNavPos;
  OrderWidget(this.bottomNavPos);

  @override
  State<StatefulWidget> createState() {
    return OrderState();
  }
}

class OrderState extends BaseState<OrderWidget> {
  @override
  void initState() {
    if (!Provider.of<DashboardModel>(context, listen: false).tabPosition!.contains(3)) {
      Provider.of<DashboardModel>(context, listen: false).tabPosition!.add(3);
    }
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_orders);
    super.initState();
  }

  _onBackPressed(BuildContext context) {
    List<int>? arrayValue = Provider.of<DashboardModel>(context, listen: false).tabPosition;
    if (arrayValue != null && arrayValue.length != 0) {
      Provider.of<DashboardModel>(context, listen: false).tabPosition!.removeAt(arrayValue.length - 1);
      Provider.of<DashboardModel>(context, listen: false).dashboardIndex = Provider.of<DashboardModel>(context, listen: false).tabPosition!.last;
      Provider.of<HomeModel>(context, listen: false).clearData();
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => _onBackPressed(context),
      child: Consumer<OrderModel>(
        builder: (BuildContext context, OrderModel orderModel, Widget? child) {
          return FutureBuilder(
            future: myFutureMethodOverall(orderModel, context),
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              return Scaffold(
                body: Builder(
                  builder: (BuildContext context) => Container(
                    //margin: const EdgeInsets.fromLTRB(0.0, 24.0, 0.0, 0),
                    child: Builder(builder: (BuildContext context) {
                      dataStore.writeInt(DataStoreService.BOTTOM_NAV_POSITION, 3);
                      if (snapshot.hasData) {
                        return getOrderCards(context, orderModel);
                      } else if (snapshot.hasError) {
                        return ErrorScreen();
                      }
                      return LoadingScreen();
                    }),
                  ),
                ),
              );
            },
          );
        },
      ),
    );
  }

  Future myFutureMethodOverall(OrderModel orderModel, BuildContext context) async {
    Future<List<Order>?> future1 = orderModel.fetchOrders(Provider.of<DashboardModel>(context, listen: false).selectedPatientId);
    Future<Map<String, dynamic>?> future2 = orderModel.getLocalization(["myorder", "prescriptions"]);
    return await Future.wait([future1, future2]);
  }

  Widget getOrderCards(BuildContext context, OrderModel orderModel) {
    List<Widget> ret = [];
    if (orderModel.orderList!.length == 0) {
      return EmptyScreen(
          imageLink: "graphics/empty_orders.png",
          title: LocalizationUtils.getSingleValueString("myorder", "myorder.all.title"),
          description: getDescriptionText(PatientUtils.getForGender(Provider.of<DashboardModel>(context).selectedPatient)),
          showTransfer: true);
    } else {
      for (int i = 0; i < orderModel.orderList!.length; i++) {
        if (i == 0) ret.add(PPUIHelper.verticalSpaceLarge());
        ret.add(getOrderCard(orderModel, context, orderModel.orderList!.elementAt(i)));
        ret.add(PPUIHelper.verticalSpaceMedium());
      }
    }
    return SingleChildScrollView(
      child: Column(children: ret),
    );
  }

  Widget getOrderCard(OrderModel orderModel, BuildContext context, Order order) {
    Widget card = PPCard(
        child: Column(
          children: <Widget>[
            getOrderCardHeading(context, order),
            PPTexts.getDescription(PPDateUtils.displayDate(order.orderedDateTime), isBold: true),
            getOrderShipments(context, order)
          ],
        ),
        onTap: () {
          orderModel.setOrder(order);
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_orders_detail);
          Navigator.pushNamed(context, OrderDetailWidget.routeName, arguments: OrderArguments(orderId: order.id));
        });
    return card;
  }

  Widget getOrderShipments(BuildContext context, Order order) {
    if (order.shipments != null && order.shipments!.length > 1) {
      List<Widget> chi = [PPUIHelper.verticalSpaceMedium()];
      for (int i = 0; i < order.shipments!.length && i < 2; i++) {
        chi.add(Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
                flex: 4,
                child: PPTexts.getDescription(
                    LocalizationUtils.getSingleValueString("myorder", "myorder.label.shipment") + " : " + order.shipments![i].shipmentNo.toString())),
            Expanded(
                child: PPTexts.getDescription(StringUtils.capitalize(ViewConstants.getPrescriptionStatus(order.shipments![i].status!)), color: primaryColor))
          ],
        ));
        chi.add(PPUIHelper.verticalSpaceSmall());
      }
      return Container(
        child: Column(
          children: chi,
        ),
      );
    } else
      return PPContainer.emptyContainer();
  }

  Widget getOrderCardHeading(BuildContext context, Order order) {
    String status = ViewConstants.getOrderStatus(order.status!);
    status = '${status[0].toUpperCase()}${status.substring(1).toLowerCase()}';
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Expanded(child: PPTexts.getHeading(order.orderNo!)),
        order.shipments != null && order.shipments!.length > 1
            ? Container()
            : PPChip(
                label: status,
                color: ViewConstants.getColorForPrescriptionStatus(order.status!),
              )
      ],
    );
  }

  String getDescriptionText(String pronounForGender) {
    switch (pronounForGender) {
      case "MALE":
        return LocalizationUtils.getSingleValueString("myorder", "myorder.all.description-MALE");
      case "FEMALE":
        return LocalizationUtils.getSingleValueString("myorder", "myorder.all.description-FEMALE");
      case "OTHER":
        return LocalizationUtils.getSingleValueString("myorder", "myorder.all.description-OTHER");
      default:
        return LocalizationUtils.getSingleValueString("myorder", "myorder.all.description");
    }
  }
}
