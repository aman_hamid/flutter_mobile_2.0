import 'package:flutter/material.dart';
import 'package:pocketpills/core/enums/telehealthAppointmentDetails.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/tele_health_appointment_time.dart';
import 'package:pocketpills/core/request/telehealth/telehealth_request.dart';
import 'package:pocketpills/core/request/telehealth/telehealth_request_update.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_province.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/dashboard/appointment_dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/dashboard/appointment_dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_divider.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_transfer_model.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/utils/analytics.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

import '../../../locator.dart';

class RescheduleTimeSheet extends StatefulWidget {
  BuildContext? context;
  AppointmentDashboardModel? model;
  bool? prescriptionFilledByExternalPharmacy;
  String? prescriptionMedicalConditions;
  String? prescriptionComment;
  String? appointmentTime;
  String? telehealthRequestedMedications;
  int? prescriptionId;

  RescheduleTimeSheet(
      {this.context,
      this.model,
      this.prescriptionFilledByExternalPharmacy,
      this.prescriptionMedicalConditions,
      this.prescriptionComment,
      this.appointmentTime,
      this.telehealthRequestedMedications,
      this.prescriptionId});

  _RescheduleTimeSheetState createState() => _RescheduleTimeSheetState();
}

class _RescheduleTimeSheetState extends BaseState<RescheduleTimeSheet> with SingleTickerProviderStateMixin {
  AppointmentDashboardModel? models;
  int provinceValue = -1;
  String? selectProvinceError;
  bool autovalidate = false;
  bool errorShow = false;
  ScrollController? _scrollController;
  List<bool> selectedIndexes = [];
  bool itemSelected = false;
  AppointmentTime? selectedItem;
  final Analytics analyticsEvents = locator<Analytics>();
  final DataStoreService dataStore = locator<DataStoreService>();
  bool eventTime = false;
  @override
  void initState() {
    super.initState();
    models = widget.model;
    _scrollController = new ScrollController(
      initialScrollOffset: 0.0, // NEW
      keepScrollOffset: true, // NEW
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext bc) {
    return FutureBuilder(
        future: myFutureMethodOverall(models!),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          Map<String, String> map = new Map();
          if (snapshot.hasData != null && snapshot.data != null) {
            List<AppointmentTime> appointmentTime = [];
            appointmentTime.addAll(snapshot.data[0]);
            for (int i = 0; i < appointmentTime.length; i++) {
              map['slot' + appointmentTime[i].index.toString()] = appointmentTime[i].available.toString();
              selectedIndexes.add(false);
            }
            if (eventTime == false) {
              eventTime = true;
              analyticsEvents.sendAnalyticsEvent("appointment_rescheduled", map);
            }
            return getMainView(models!, snapshot.data[0]);
          } else if (snapshot.hasError) {
            return ErrorScreen();
          } else {
            return LoadingScreen();
          }
        });
  }

  Future myFutureMethodOverall(AppointmentDashboardModel model) async {
    Future<List<AppointmentTime>?> future1 = model.getAppointmentDate();
    Future<Map<String, dynamic>?> future2 = model.getLocalization(["signup", "modal"]); // will take 3 secs
    return await Future.wait([future1, future2]);
  }

  Widget getMainView(AppointmentDashboardModel model, List<AppointmentTime> appointmentTime) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(MEDIUM_X),
            child: PPTexts.getSecondaryHeading(LocalizationUtils.getSingleValueString("modal", "modal.reschedule.title")),
          ),
          Divider(
            color: secondaryColor,
            height: 1,
          ),
          SizedBox(
            height: MEDIUM,
          ),
          ConstrainedBox(
            constraints: new BoxConstraints(
              maxHeight: MediaQuery.of(context).size.height * .6,
            ),
            child: appointmentDate(context, model, appointmentTime),
          ),
          if (itemSelected) doctorView(),
          errorShow == false
              ? Container()
              : Padding(
                  padding: EdgeInsets.only(bottom: 5),
                  child: PPTexts.getFormError(LocalizationUtils.getSingleValueString("signup", "signup.appointment.required"), color: errorColor),
                ),
          PPDivider(),
          Padding(
            padding: const EdgeInsets.all(PPUIHelper.HorizontalSpaceMedium),
            child: models!.state == ViewState.Busy
                ? ViewConstants.progressIndicator
                : Row(
                    children: <Widget>[
                      SecondaryButton(
                        isExpanded: true,
                        text: LocalizationUtils.getSingleValueString("common", "common.button.close"),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                      SizedBox(
                        width: PPUIHelper.HorizontalSpaceSmall,
                      ),
                      PrimaryButton(
                        isExpanded: true,
                        text: LocalizationUtils.getSingleValueString("common", "common.button.reschedule"),
                        onPressed: () {
                          setState(() {
                            if (selectedItem != null) {
                              errorShow = false;
                              onClick(model);
                            } else if (selectedItem == null) {
                              errorShow = true;
                            }
                          });
                        },
                      )
                    ],
                  ),
          ),
        ],
      ),
    );
  }

  onClick(AppointmentDashboardModel model) async {
    AppointmentDetails.AppointmentSelectedItem = selectedItem;
    dataStore.writeString(DataStoreService.DOCTOR_NAME, selectedItem!.doctorName!);
    dataStore.writeString(DataStoreService.APPOINTMENT_TIME, selectedItem!.timeSlot!);
    dataStore.writeString(DataStoreService.APPOINTMENT_DATE, selectedItem!.displayDate!);
    TelehealthRequestUpdate telehealthRequest = TelehealthRequestUpdate(appointmentTime: selectedItem!.actualAppointmentTime, id: widget.prescriptionId!);

    bool success = await model.sendPreferences(telehealthRequest, widget.prescriptionId);
    if (success) {
      Map<String, String> map = new Map();
      map["daysDiffer"] = selectedItem!.appointmentDate.toString();
      map["slotSelected"] = selectedItem!.index.toString();
      analyticsEvents.sendAnalyticsEvent("appointment_rescheduled", map);
      Provider.of<DashboardModel>(context, listen: false).dashboardIndex = 0;
      Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
    } else
      model.showSnackBar(context, LocalizationUtils.getSingleValueString("common", "common.label.error-message"));
  }

  Widget lineDivider = Divider(color: secondaryColor, height: 1);

  Widget getDosageTiles(SignUpTransferModel model) {
    List<Widget> tiles = [];
    for (int i = 0; i < model.provinceArrayList.length; i++) {
      TelehealthProvinceItem suggestedEmployer = model.provinceArrayList[i];
      tiles.add(RadioListTile(
        title: Text(
          suggestedEmployer.value,
          style: MEDIUM_XXX_PRIMARY_BOLD,
        ),
        value: i,
        groupValue: provinceValue,
        onChanged: (int? val) {
          setState(() {
            provinceValue = val!;
            selectProvinceError = null;
            autovalidate = false;
          });
        },
      ));
    }
    return Container(
        child: Column(
      children: tiles,
    ));
  }

  Widget appointmentDate(BuildContext context, AppointmentDashboardModel model, List<AppointmentTime> appointmentTime) {
    return new GridView.builder(
        shrinkWrap: true,
        primary: false,
        controller: _scrollController,
        scrollDirection: Axis.vertical,
        itemCount: appointmentTime.length,
        gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2, childAspectRatio: 2.3),
        itemBuilder: (BuildContext context, int index) {
          AppointmentTime appointmentTimeItem = appointmentTime[index];
          return Container(
            decoration: new BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Color.fromARGB(40, 0, 0, 0),
                  blurRadius: 3,
                  spreadRadius: 0,
                  offset: Offset(
                    0, // horizontal,
                    1.0, // vertical,
                  ),
                )
              ],
              border: Border.all(color: Color(0xFFE3DBFF), width: 1),
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(5.0),
              color: selectedIndexes[index] == true
                  ? brandColor
                  : appointmentTimeItem.available == false
                      ? bgDateColor
                      : Colors.white,
            ),
            margin: EdgeInsets.all(SMALL_XX),
            child: Card(
              color: Colors.white,
              shape: RoundedRectangleBorder(),
              margin: EdgeInsets.all(1.0),
              elevation: 0,
              child: InkWell(
                borderRadius: BorderRadius.circular(4.0),
                child: Container(
                  padding: EdgeInsets.all(0.0),
                  child: Container(
                    color: selectedIndexes[index] == true
                        ? brandColor
                        : appointmentTimeItem.available == false
                            ? bgDateColor
                            : Colors.white,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          child: Padding(
                            padding: EdgeInsets.only(left: SMALL_XX, right: SMALL_XX, top: MEDIUM),
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    appointmentTimeItem.displayDate!,
                                    style: selectedIndexes[index] == true ? MEDIUM_XX_WHITE_BOLD : MEDIUM_XX_PRIMARY_BOLD,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                  ),
                                  SizedBox(
                                    height: SMALL_XX,
                                  ),
                                  Text(
                                    appointmentTimeItem.status!,
                                    style: selectedIndexes[index] == true
                                        ? MEDIUM_XX_WHITE_BOLD
                                        : appointmentTimeItem.available == true
                                            ? MEDIUM_XX_GREEN_BOLD_MEDIUM
                                            : MEDIUM_XX_RED_BOLD_MEDIUM,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 2,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                onTap: () {
                  if (appointmentTimeItem.available == true) {
                    setState(() {
                      //model.updateAppointment(appointmentTimeItem.id);
                      if (selectedIndexes[index] != true) {
                        for (int i = 0; i < appointmentTime.length; i++) {
                          selectedIndexes[i] = false;
                        }
                        itemSelected = true;
                        errorShow = false;
                        selectedItem = appointmentTimeItem;
                        selectedIndexes[index] = true;
                      } else {
                        itemSelected = false;
                        selectedItem = null;
                        selectedIndexes[index] = false;
                      }
                    });
                  }
                },
              ),
            ),
          );
        });
  }

  Widget doctorView() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        height: 100,
        decoration: BoxDecoration(
          color: bgDateColor,
          boxShadow: [
            BoxShadow(
              color: quartiaryColor,
              offset: Offset(0.1, 0.0), //(x,y)
              blurRadius: 6.0,
            ),
          ],
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                selectedItem!.clinicName!,
                style: MEDIUM_X_PRIMARY,
              ),
              PPUIHelper.verticalSpaceXSmall(),
              Text(
                selectedItem!.doctorName!,
                style: MEDIUM_XXX_BLACK_BOLD,
              ),
              SizedBox(
                height: MEDIUM,
              ),
              Row(
                children: [
                  Text(
                    selectedItem!.displayDate!,
                    style: MEDIUM_XX_PRIMARY,
                  ),
                  Text(
                    ", ",
                    style: MEDIUM_XX_PRIMARY,
                  ),
                  Text(
                    selectedItem!.timeSlot!,
                    style: MEDIUM_XX_PRIMARY,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
