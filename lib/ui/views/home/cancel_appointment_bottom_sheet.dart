import 'package:flutter/material.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_transfer_model.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/signup/teleheath_arguments.dart';
import 'package:pocketpills/ui/views/telehealth/telehealth_preference.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/views/contact/prescription_source_arguments.dart';
import 'package:pocketpills/ui/views/contact/user_contact_view.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/ui/views/addmember/add_member_complete.dart';
import 'package:pocketpills/ui/views/consent/consent_arguments.dart';
import 'package:pocketpills/ui/views/consent/wait_consent.dart';
import 'package:pocketpills/ui/views/contact/source_arguments.dart';
import 'package:pocketpills/ui/views/profile/profile_health_card_view.dart';
import 'package:pocketpills/ui/views/signup/signup_almost_done_view.dart';
import 'package:pocketpills/ui/views/signup/signup_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class SelectProvinsSheet extends StatefulWidget {
  BuildContext? context;
  SignUpTransferModel? model;
  BaseStepperSource? source;
  UserPatient? userPatient;

  SelectProvinsSheet({this.context, this.model, this.source, this.userPatient});

  _SelectProvinsSheetState createState() => _SelectProvinsSheetState();
}

class _SelectProvinsSheetState extends BaseState<SelectProvinsSheet> with SingleTickerProviderStateMixin {
  SignUpTransferModel? model;
  int provinceValue = -1;
  String? selectProvinceError;
  bool autovalidate = false;

  @override
  void initState() {
    super.initState();
    model = widget.model;
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext bc) {
    return FutureBuilder(
        future: myFutureMethodOverall(model!),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData != null && snapshot.data != null) {
            return getMainView();
          } else if (snapshot.hasError) {
            return ErrorScreen();
          } else {
            return LoadingScreen();
          }
        });
  }

  Future myFutureMethodOverall(SignUpTransferModel model) async {
    // Future<Map<String, dynamic>> future1 = model.getLocalizationSignUp("signup"); // will take 3 secs
    // Future<Map<String, dynamic>> future2 = model.getLocalizationSignUp("modal"); // will take 3 secs
    return await Future.wait([]);
  }

  Widget getMainView() {
    return Container(
      height: 270,
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(MEDIUM_X),
            child: PPTexts.getSecondaryHeading(LocalizationUtils.getSingleValueString("modal", "modal.selectprovince.title"), textAlign: TextAlign.center),
          ),
          Divider(
            color: secondaryColor,
            height: 1,
          ),
          SizedBox(
            height: MEDIUM,
          ),
          getDosageTiles(),
          selectProvinceError == null ? SizedBox(height: 2) : PPTexts.getFormError(selectProvinceError!, color: errorColor),
          Padding(
            padding: const EdgeInsets.only(left: MEDIUM_X),
            child: SizedBox(
              height: REGULAR_XXX,
            ),
          ),
          model!.state == ViewState.Busy
              ? ViewConstants.progressIndicator
              : Padding(
                  padding: const EdgeInsets.only(left: MEDIUM_XXX, right: MEDIUM_XXX),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      SecondaryButton(
                        isExpanded: true,
                        text: LocalizationUtils.getSingleValueString("signup", "signup.employer.skip"),
                        onPressed: () {
                          onSkipClick(model!);
                        },
                      ),
                      SizedBox(
                        width: PPUIHelper.HorizontalSpaceSmall,
                      ),
                      PrimaryButton(
                        isExpanded: true,
                        text: LocalizationUtils.getSingleValueString("signup", "signup.transfer.current-pharmacy-continue"),
                        onPressed: () {
                          if (provinceValue == 1) {
                            uploadDetails(context, "BRITISH_COLUMBIA");
                            setState(() {
                              autovalidate = true;
                            });
                          } else if (provinceValue == 0) {
                            uploadDetails(context, "ONTARIO");
                            setState(() {
                              autovalidate = true;
                            });
                          } else {
                            setState(() {
                              selectProvinceError = LocalizationUtils.getSingleValueString("signup", "signup.fields.error-required");
                              autovalidate = false;
                            });
                          }
                        },
                      )
                    ],
                  ),
                ),
          SizedBox(
            height: REGULAR_XXX,
          ),
        ],
      ),
    );
  }

  Widget lineDivider = Divider(color: secondaryColor, height: 1);
  List<String> value = [
    LocalizationUtils.getSingleValueString("signup", "signup.fields.province-name-BRITISH_COLUMBIA"),
    LocalizationUtils.getSingleValueString("signup", "signup.fields.province-name-ONTARIO")
  ];

  Widget getDosageTiles() {
    List<Widget> tiles = [];
    for (int i = 0; i < value.length; i++) {
      String suggestedEmployer = value[i];
      tiles.add(RadioListTile(
        title: Text(
          suggestedEmployer,
          style: MEDIUM_XXX_PRIMARY_BOLD,
        ),
        value: i,
        groupValue: provinceValue,
        onChanged: (int? val) {
          setState(() {
            provinceValue = val!;
            selectProvinceError = null;
            autovalidate = false;
          });
        },
      ));
    }
    return Container(
        child: Column(
      children: tiles,
    ));
  }

  void uploadDetails(BuildContext context, String province) async {
    bool connectivityResult = await model!.isInternetConnected();
    if (connectivityResult == false) {
      onFail(context, errMessage: model!.noInternetConnection);
      return;
    }
    bool success = await model!.telehealthProvince(province);
    if (success) {
      Navigator.pushNamed(context, TelehealthPreference.routeName,
          arguments: TelehealthArguments(
            modelSignUp: model,
            source: widget.source,
            userPatient: widget.userPatient,
            from: "telehealth",
          ));
    } else
      model!.showSnackBar(context, LocalizationUtils.getSingleValueString("common", "common.label.error-message"));
  }

  onSkipClick(SignUpTransferModel model) async {
    bool success = await model.uploadPrescriptionSignUp();
    print("here");
    if (success == true) {
      print("success");
      Navigator.pop(context);
      dataStore.writeBoolean(DataStoreService.TRANSFER_SKIPPED, true);
      goToNextScreen(model);
    }
  }

  void goToNextScreen(SignUpTransferModel signUpTransferModel) {
    if (widget.source == BaseStepperSource.NEW_USER) {
      dataStore.writeBoolean(DataStoreService.COMPLETE_TRANSFER_MODULE, true);
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_contact_detail);
      Navigator.of(context)
          .pushNamedAndRemoveUntil(SignUpAlmostDoneWidget.routeName, (Route<dynamic> route) => false, arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER));
    } else if (widget.source == BaseStepperSource.ADD_PATIENT) {
      Navigator.pushReplacementNamed(context, AddMemberCompleteWidget.routeName, arguments: ConsentArguments(userPatient: widget.userPatient));
    } else if (widget.source == BaseStepperSource.CONSENT_FLOW) {
      Navigator.pushReplacementNamed(context, WaitConsentWidget.routeName, arguments: ConsentArguments(userPatient: widget.userPatient));
    } else if (widget.source == BaseStepperSource.MAIN_SCREEN) {
      Navigator.of(context).pushReplacementNamed(UserContactWidget.routeName,
          arguments: PrescriptionSourceArguments(source: BaseStepperSource.TRANSFER_SCREEN, successDetails: signUpTransferModel.successDetails));
    } else if (widget.source == BaseStepperSource.COPAY_REQUEST) {
      Navigator.of(context).pushNamed(ProfileHealthCardView.routeName, arguments: SourceArguments(source: widget.source));
    }
  }
}
