import 'package:pocketpills/core/models/medicine.dart';

class SearchArguments {
  final Medicine? medicine;

  SearchArguments({this.medicine});
}
