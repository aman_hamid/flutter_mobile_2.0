import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/response/prescription/success_details.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/views/base_stateless_widget.dart';

class SuccessTickCardView extends BaseStatelessWidget {
  final SuccessDetails successDetails;

  SuccessTickCardView(this.successDetails);

  @override
  Widget build(BuildContext context) {
    if (successDetails != null) {
      return Container(
        color: bghighlight2,
        width: double.infinity,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: REGULAR_XXX,
            ),
            CircleAvatar(
                radius: 40,
                backgroundColor: whiteColor,
                child: CircleAvatar(
                  radius: 36,
                  backgroundColor: brandColor,
                  backgroundImage: CachedNetworkImageProvider(
                    successDetails.image!,
                  ),
                )),
            Padding(
              padding: const EdgeInsets.only(top: MEDIUM_XXX),
              child: Text(
                successDetails.title!,
                style: REGULAR_XXX_DARK_BLUE_BOLD,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: REGULAR_XXX, top: SMALL_XXX, right: REGULAR_XXX, bottom: MEDIUM_XXX),
              child: Text(
                successDetails.description!,
                style: MEDIUM_XX_LIGHT_BLUE_BOLD_MEDIUM,
                maxLines: 2,
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      );
    } else {
      return PPContainer.emptyContainer();
    }
  }
}
