import 'package:flutter/material.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';

class PPContainer extends StatelessWidget {
  final Widget? child;
  final Color? color;
  final EdgeInsets? margin;
  final EdgeInsets? padding;
  final String? heading = 'Coming to Quebec soon!';
  final String content =
      'We cannot deliver your order at present. We will be able to serve Quebec residents soon.';

  PPContainer({this.child, this.color, this.margin, this.padding});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: margin != null ? margin : EdgeInsets.all(10.0),
          alignment: Alignment.topLeft,
          padding: padding != null ? padding : EdgeInsets.all(10.0),
          decoration: BoxDecoration(
              color: Color(0xFFFCF6E9),
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(4.0),
              border: Border.all(
                  color: Color(0xFFE6A200),
                  width: 1.0,
                  style: BorderStyle.solid)),
          child: Column(
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  heading!,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: PPUIHelper.FontSizeSmall),
                  textAlign: TextAlign.left,
                ),
              ),
              SizedBox(height: PPUIHelper.VerticalSpaceSmall),
              Text(
                content,
                style: TextStyle(fontSize: PPUIHelper.FontSizeSmall),
              ),
            ],
          ),
        )
      ],
    );
  }
}
