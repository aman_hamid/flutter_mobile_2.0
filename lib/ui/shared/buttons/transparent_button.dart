import 'package:flutter/material.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';

class TransparentButton extends StatelessWidget {
  final String? text;
  final void Function()? onPressed;
  final bool? isSmall;
  final bool? disabled;
  final bool? isExpanded;
  final Color bgColor;
  final bool showIcon;
  TransparentButton({@required this.text, @required this.onPressed, this.isSmall, this.showIcon = false, this.disabled, this.isExpanded = false, this.bgColor = Colors.white});

  @override
  Widget build(BuildContext context) {
    double fontSize = isSmall == null ? PPUIHelper.FontSizeMedium : PPUIHelper.FontSizeSmall;
//    double horizontalPadding = isSmall==null ? 20 : 10;
//    double verticalPadding = isSmall==null ? 15 : 10;
    Color textColor = bgColor == Colors.white ? linkColor : Colors.white;
    Widget textChild;
    if (showIcon == true) {
      textChild = Container(
        child: Row(
          children: <Widget>[
            Text(
              this.text!,
              style: TextStyle(fontSize: fontSize,
                  color: textColor,fontFamily: "FSJoeyPro Bold"),
              textAlign: TextAlign.center,

            ),
            Icon(
              Icons.chevron_right,
              color: textColor,
            )
          ],
        ),
      );
    } else {
      textChild = Text(
        this.text!,
        style: TextStyle(fontSize: fontSize, color: textColor,fontFamily: "FSJoeyPro Bold"),
        textAlign: TextAlign.center,
      );
    }
    Widget child = FlatButton(
      color: bgColor == null ? Colors.white : bgColor,
      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
      child: textChild,
      onPressed: this.onPressed,
    );
    if (isExpanded == true)
      return Expanded(
        child: child,
      );
    return child;
  }
}
