import 'package:flutter/material.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';

class PPChip extends StatelessWidget {
  final String? label;
  final Color? color;
  PPChip({Key? key, this.label, this.color = secondaryColor}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: new BoxDecoration(
        color: lightBlueColor,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(12.0),
      ),
      //color: headerBgColor,
      padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceSmall, vertical: SMALL_XXX),
      child: Text(label!,
          style: TextStyle(
            color: this.color,
            fontSize: PPUIHelper.FontSizeSmall,
            fontWeight: FontWeight.w500,
              fontFamily: "FSJoeyPro Bold"
          )),
    );
  }
}
