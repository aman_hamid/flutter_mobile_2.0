import 'package:pocketpills/core/response/signup_response.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

Map<String, String> getPhoneValidation() {
  Map<String, String> phValidationMap = Map<String, String>();
  phValidationMap["SIGNUP"] = LocalizationUtils.getSingleValueString("common", "common.label.cell-register-signup-instead");
  phValidationMap["PHONE_LOGIN"] = LocalizationUtils.getSingleValueString("common", "common.label.cell-registered-msg");
  phValidationMap["PHONE_OTP_FORGOT"] = LocalizationUtils.getSingleValueString("common", "common.label.password-set-msg");
  return phValidationMap;
}

Map<String, String> getEmailValidation() {
  Map<String, String> phValidationMap = Map<String, String>();
  phValidationMap["SIGNUP"] = LocalizationUtils.getSingleValueString("common", "common.label.cell-register-signup-instead");
  phValidationMap["EMAIL_LOGIN"] = LocalizationUtils.getSingleValueString("common", "common.label.cell-registered-msg");
  phValidationMap["EMAIL_OTP_FORGOT"] = LocalizationUtils.getSingleValueString("common", "common.label.password-set-msg");
  return phValidationMap;
}

Map<String, String> getFaxPreferenceMap() {
  Map<String, String> phValidationMap = Map<String, String>();
  phValidationMap["fax_doctor"] = LocalizationUtils.getSingleValueString("common", "common.label.fax-cosign");
  phValidationMap["mail_prescription"] = LocalizationUtils.getSingleValueString("common", "common.label.i-mail-prescription");
  return phValidationMap;
}

Map<String, String> getDayMap() {
  Map<String, String> dayMap = Map<String, String>();
  dayMap["ANY"] = LocalizationUtils.getSingleValueString("modals", "modals.timings.any-day");
  dayMap["WEEKENDS"] = LocalizationUtils.getSingleValueString("modals", "modals.timings.weekends");
  dayMap["WEEKDAYS"] = LocalizationUtils.getSingleValueString("modals", "modals.timings.weekdays");
  return dayMap;
}

Map<String, String> getTimeMap() {
  Map<String, String> timeMap = Map<String, String>();
  timeMap["Any Time"] = LocalizationUtils.getSingleValueString("modals", "modals.timings.any-time");
  timeMap["8 AM - 12 NOON"] = LocalizationUtils.getSingleValueString("modals", "modals.timings.morning");
  timeMap["12 Noon - 4 PM"] = LocalizationUtils.getSingleValueString("modals", "modals.timings.afternoon");
  timeMap["4 PM - 8 PM"] = LocalizationUtils.getSingleValueString("modals", "modals.timings.evening");
  return timeMap;
}

Map<String, String> getPreferenceMap() {
  Map<String, String> preferenceMap = Map<String, String>();
  preferenceMap["NEW_PRESCRIPTION"] = LocalizationUtils.getSingleValueString("consultation", "consultation.label.reason-NEW_PRESCRIPTION");
  preferenceMap["RENEW_EXISTING_PRESCRIPTION"] = LocalizationUtils.getSingleValueString("consultation", "consultation.label.reason-RENEW_EXISTING_PRESCRIPTION");
  preferenceMap["SOMETHING_ELSE"] = LocalizationUtils.getSingleValueString("consultation", "consultation.label.reason-SOMETHING_ELSE");
  return preferenceMap;
}

Map<String, String> getcategoryMap() {
  Map<String, String> categoryMap = Map<String, String>();
  categoryMap["ACNE"] = LocalizationUtils.getSingleValueString("consultation", "consultation.label.category-ACNE");
  categoryMap["ERECTILE_DYSFUNCTION"] = LocalizationUtils.getSingleValueString("consultation", "consultation.label.category-ERECTILE_DYSFUNCTION");
  categoryMap["BIRTH_CONTROL"] = LocalizationUtils.getSingleValueString("consultation", "consultation.label.category-BIRTH_CONTROL");
  categoryMap["HERPES"] = LocalizationUtils.getSingleValueString("consultation", "consultation.label.category-HERPES");
  categoryMap["OTHER"] = LocalizationUtils.getSingleValueString("consultation", "consultation.label.category-OTHERS");
  return categoryMap;
}

List<Map<String, String>> getStateMap() {
  List<Map<String, String>> getStates = [
    {"key": "alberta", "value": LocalizationUtils.getSingleValueString("common", "common.province.alberta")},
    {"key": "british_columbia", "value": LocalizationUtils.getSingleValueString("common", "common.province.british_columbia")},
    {"key": "manitoba", "value": LocalizationUtils.getSingleValueString("common", "common.province.manitoba")},
    {"key": "new_brunswick", "value": LocalizationUtils.getSingleValueString("common", "common.province.new_brunswick")},
    {"key": "newfoundland_and_labrador", "value": LocalizationUtils.getSingleValueString("common", "common.province.newfoundland_and_labrador")},
    {"key": "northwest_territories", "value": LocalizationUtils.getSingleValueString("common", "common.province.northwest_territories")},
    {"key": "nova_scotia", "value": LocalizationUtils.getSingleValueString("common", "common.province.nova_scotia")},
    {"key": "nunavut", "value": LocalizationUtils.getSingleValueString("common", "common.province.nunavut")},
    {"key": "ontario", "value": LocalizationUtils.getSingleValueString("common", "common.province.ontario")},
    {"key": "prince_edward_island", "value": LocalizationUtils.getSingleValueString("common", "common.province.prince_edward_island")},
    {"key": "quebec", "value": LocalizationUtils.getSingleValueString("common", "common.province.quebec")},
    {"key": "saskatchewan", "value": LocalizationUtils.getSingleValueString("common", "common.province.saskatchewan")},
    {"key": "yukon", "value": LocalizationUtils.getSingleValueString("common", "common.province.yukon")}
  ];

  return getStates;
}

List<String> renewMedicalConditions = [
  "Herpes",
  "Attention deficit disorder",
  "Attention deficit hyperactivity disorder",
  "Yeast infections",
  "Ivermectin",
  "Overweight",
  "Sexual disease",
  "STD",
  "Sexually transmitted disease",
  "STI",
  "Sexually transmitted Infections",
  "Chlamydia",
  "Gonarrhea",
  "Hepatitis",
  "Renew Prescription",
  "Renouveler la Prescription",
  "ADD",
  "AIDS",
  "HIV",
  "ADHD",
  "Syphilis"
];

Map<String, String> setProvince = {
  "Alberta": "Alberta",
  "British Columbia": "Colombie-Britannique",
  "Manitoba": "Manitoba",
  "New Brunswick": "Nouveau Brunswick",
  "Newfoundland and Labrador": "Terre-Neuve et Labrador",
  "Northwest Territories": "Territoires du Nord-Ouest",
  "Nova Scotia": "Nouvelle-Écosse",
  "Nunavut": "Nunavut",
  "Ontario": "Ontario",
  "Prince Edward Island": "Île-du-Prince-Édouard",
  "Quebec": "Québec",
  "Saskatchewan": "Saskatchewan",
  "Yukon": "Yukon",
};

String? getDropDownValue(String? value) {
  return getSelectedLanguage() == ViewConstants.languageIdEn ? value : setProvince[value];
}

String getServerSendingValue(String value) {
  var key = setProvince.keys.firstWhere((k) => setProvince[k] == value, orElse: () => "");
  return getSelectedLanguage() == ViewConstants.languageIdEn ? value : key;
}
