import 'package:flutter/material.dart';
import 'package:pocketpills/core/viewmodels/profile/profile_address_model.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class AddressDeleteDialog extends StatelessWidget {
  final addressId;
  final Function(String)? showSnackBar;
  final ProfileAddressModel? profileAddressModel;

  AddressDeleteDialog({Key? key, this.addressId, this.showSnackBar, this.profileAddressModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Container(
          child: Row(
        children: <Widget>[
//              Icon(Icons.delete),
//              SizedBox(
//                  width: PPUIHelper.HorizontalSpaceMedium),
          Text(LocalizationUtils.getSingleValueString("address", "address.labels.delete-title"), style: TextStyle(fontFamily: "FSJoeyPro Bold")),
        ],
      )),
      content: Text(LocalizationUtils.getSingleValueString("address", "address.labels.delete-description"), style: TextStyle(fontFamily: "FSJoeyPro Medium")),
      actions: <Widget>[
        // usually buttons at the bottom of the dialog
        FlatButton(
          child: Text(LocalizationUtils.getSingleValueString("common", "common.button.cancel").toUpperCase(), style: TextStyle(fontFamily: "FSJoeyPro Bold")),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        FlatButton(
          child: Text(LocalizationUtils.getSingleValueString("common", "common.button.delete").toUpperCase(), style: TextStyle(fontFamily: "FSJoeyPro Bold")),
          onPressed: () async {
            deleteAddress(context);
          },
        ),
      ],
    );
  }

  void deleteAddress(BuildContext context) async {
    bool connectivityResult = await profileAddressModel!.isInternetConnected();
    if (connectivityResult == false) {
      showSnackBar!(profileAddressModel!.noInternetConnection);
      return;
    }
    bool result = await profileAddressModel!.deletePatientAddress(addressId);
    if (result == true) {
      showSnackBar!(LocalizationUtils.getSingleValueString("address", "address.labels.delete-success"));
    }
    Navigator.of(context).pop();
  }

  static void Function()? show(ProfileAddressModel profileAddress, BuildContext context, int addressId, {Function(String)? showSnackBar}) {
    return () {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AddressDeleteDialog(
            addressId: addressId,
            showSnackBar: showSnackBar,
            profileAddressModel: profileAddress,
          );
        },
      );
    };
  }
}
