import 'dart:io';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/viewmodels/signup/about_you_model.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class ExitDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => SignUpAboutYouModel(),
      child: Consumer<SignUpAboutYouModel>(builder: (BuildContext context, SignUpAboutYouModel model, Widget? child) {
        return FutureBuilder(
            future: model.getLocalization(["common"]),
            builder: (BuildContext context, AsyncSnapshot<Map<String, dynamic>?> snapshot) {
              if (snapshot.hasData != null && snapshot.data != null) {
                return getMainView(model, context);
              } else if (snapshot.hasError) {
                return ErrorScreen();
              } else {
                return LoadingScreen();
              }
            });
      }),
    );
  }

  static Function? show(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return ExitDialog();
      },
    );
  }

  Widget getMainView(SignUpAboutYouModel model, BuildContext context) {
    return AlertDialog(
      title: Text(LocalizationUtils.getSingleValueString("common", "common.exit.title"),style: TextStyle(fontFamily: "FSJoeyPro Bold")),
      titlePadding: EdgeInsets.all(20),
      content: Text(LocalizationUtils.getSingleValueString("common", "common.exit.description"),style: TextStyle(fontFamily: "FSJoeyPro Medium")),
      actions: <Widget>[
        FlatButton(
          onPressed: () {
            exit(0);
          },
          child: Text(LocalizationUtils.getSingleValueString("common", "common.label.yes"),style: TextStyle(fontFamily: "FSJoeyPro Bold")),
        ),
        FlatButton(
          onPressed: () {
            Navigator.of(context).pop(false);
          },
          child: Text(LocalizationUtils.getSingleValueString("common", "common.label.no"),style: TextStyle(fontFamily: "FSJoeyPro Bold")),
        )
      ],
    );
  }
}
