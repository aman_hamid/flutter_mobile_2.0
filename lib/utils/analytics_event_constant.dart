class AnalyticsEventConstant {
  // Sign up events

  static const String au_landing_form_submit = "au_landing_form_submit";
  static const String au_about_submit = "au_about_submit";
  static const String dropoff_popup_open = "dropoff_popup_open";
  static const String au_upload = "au_upload";
  static const String au_upload_prescription = "au_upload_prescription";
  static const String au_deliveryBy_entered = "au_deliveryBy_entered";
  static const String au_almost_done_submit = "au_almost_done_submit";
  static const String au_verification = "au_verification";
  static const String signup_success = "signup_success";
  static const String dropoff_popup_telehealth = "dropoff_popup_telehealth";
  static const String au_provincelist_entered = "au_provincelist_entered";
  static const String au_appointment_province_submit = "au_appointment_province_submit";
  static const String au_allowdoctor_entered = "au_allowdoctor_entered";
  static const String au_consultation_submit = "au_consultation_submit";
  static const String au_healthcard = "au_healthcard";
  static const String healthcard_fr_click = "healthcard_fr_click";
  static const String healthcard_bk_click = "healthcard_bk_click";
  static const String healthcard_fr_uploaded = "healthcard_fr_uploaded";
  static const String healthcard_bk_uploaded = "healthcard_bk_uploaded";
  static const String healthcard_proceed = "healthcard_proceed";
  static const String consultation_signup_success = "consultation_signup_success";
  static const String consultation_signup_complete_profile_clicked = "consultation_signup_complete_profile_clicked";
  static const String insurance_fr_click = "insurance_fr_click";
  static const String insurance_bk_click = "insurance_bk_click";
  static const String insurance_proceed = "insurance_proceed";
  static const String insurance_fr_uploaded = "insurance_fr_uploaded";
  static const String insurance_bk_uploaded = "insurance_bk_uploaded";
  static const String verification_resend_modal_open = "verification_resend_modal_open";
  static const String verification_resend_click = "verification_resend_click";
  static const String verification_call_click = "verification_call_click";
  static const String health_allergies_yes = "health_allergies_yes";
  static const String health_proceed = "health_proceed";

  static const String click_resend_sms = "click_resend_sms";
  static const String click_get_call = "click_get_call";
  static const String click_edit_phone_number = "click_edit_phone_number";
  static const String phone_number_updated = "phone_number_updated";
  static const String email_updated = "email_updated";

  static const String account_verification = "au_verification";

  static const String account_about_you = "au_about_you";
  static const String account_about_you_verification = "au_about_you_verification";
  static const String account_about_you_verification_failed = "au_about_you_verification";

  static const String account_almost_done = "au_almost_done";
  static const String account_contact_detail = "au_contact_detail";
  static const String account_almost_done_verification = "au_almost_done_verification";
  static const String account_almost_done_verification_failed = "au_almost_done_verification_failed";

  static const String account_user_contact_view = "au_user_contact_view";
  static const String account_user_contact_done = "au_user_contact_done";
  static const String user_contact_day_change = "user_contact_day_change";
  static const String user_contact_time_change = "user_contact_time_change";

  static const String account_signup_details = "au_signup_details";
  static const String account_signupdetails_verification = "au_signupdetails_verification";
  static const String account_signupdetails_verification_failed = "au_signupdetails_verification_failed";

  static const String account_transfer_new = "au_transfer_refills";
  static const String account_transfer_new_formfilled = "au_transfer_refills_formfilled";
  static const String account_transfer_skipped = "au_transfer_refills_skipped";
  static const String account_congratulations = "au_congratulations";

  static const String signup = "signup";

  static const String account_otp_verification = "au_otp_verification";
  static const String account_otp_verification_failed = "au_otp_verification_failed"; //

  // Add member events
  static const String member_add = "am_add";
  static const String member_contact_details = "am_contact";
  static const String member_about_them = "am_about_you";
  static const String member_transfer_new = "am_transfer_refills";
  static const String member_transfer_refills_formfilled = "am_transfer_refills_formfilled";
  static const String member_congratulations = "am_congratulations";

  // Transfer refill //steper
  static const String transfer = "transfer";
  static const String upload_transfer_image = "upload_transfer_image";
  static const String transfer_intermediate = "transfer_refills";
  static const String transfer_health = "transfer_health";
  static const String transfer_insurance = "transfer_insurance";
  static const String transfer_address = "transfer_address";
  static const String transfer_payment = "transfer_payment";
  static const String transfer_const = "transfer_const";
  static const String click_transfer_remove_pharmacy = "click_transfer_remove_pharmacy";

  static const String au_transfer_nearby_selected = "au_transfer_nearby_selected";
  static const String transfer_nearby_selected = "transfer_nearby_selected";
  static const String au_transfer_nearby_shown = "au_transfer_nearby_shown";
  static const String transfer_nearby_shown = "transfer_nearby_shown";
  static const String au_transfer_search = "au_transfer_search";
  static const String transfer_search = "transfer_search";
  static const String transfer_search_select = "transfer_search_select";
  static const String au_transfer_search_select = "au_transfer_search_select";

  //Fax prescription from clinic
  static const String fax = "fax";

  // Upload prescription
  static const String upload = "upload";
  static const String upload_prescription = "upload_prescription";
  static const String upload_health = "upload_health";
  static const String upload_insurance = "upload_insurance";
  static const String upload_address = "upload_address";
  static const String upload_payment = "upload_payment";
  static const String upload_const = "upload_const";

  //order - cases where we dont know where user came from
  static const String order_health = "order_health";
  static const String order_insurance = "order_insurance";
  static const String order_address = "order_address";
  static const String order_payment = "order_payment";
  static const String order_const = "order_const";

  //telehealth - cases where we dont know where user came from
  static const String telehealth_health = "telehealth_health";
  static const String telehealth_insurance = "telehealth_insurance";
  static const String telehealth_address = "telehealth_address";
  static const String telehealth_payment = "telehealth_payment";
  static const String telehealth_const = "telehealth_const";

  // Refill order
  static const String medications = "medications";
  static const String refill = "refill";
  static const String refill_intermediate = "refill_intermediate";
  static const String refill_health = "refill_health";
  static const String refill_insurance = "refill_insurance";
  static const String refill_address = "refill_address";
  static const String refill_payment = "refill_payment";
  static const String refill_const = "refill_const";

  // Free vitamin for 30 days
  static const String vitamins = "vitamins";
  static const String vitamins_time = "vitamins_time";
  static const String vitamins_intermediate = "vitamins_intermediate";
  static const String vitamins_health = "vitamins_health";
  static const String vitamins_insurance = "vitamins_insurance";
  static const String vitamins_address = "vitamins_address";
  static const String vitamins_payment = "vitamins_payment";
  static const String vitamins_const = "vitamins_const";

  // App review
  static const String click_app_review_yes = "click_app_review_yes";
  static const String click_app_review_no = "click_app_review_no";

  // Start view
  static const String open_login = "login_view";
  static const String open_signup = "open_signup"; //

  static const String click_login = "click_login";
  static const String click_signup = "click_signup";
  static const String click_logout = "click_logout";

  static const String click_forget_password = "click_forget_password";

  static const String success_login = "success_login";
  static const String click_reset_password = "click_reset_password";

  static const String failed_login = "failed_login";
  static const String failed_login_validation = "failed_login_validation";

  // Start view
  static const String click_start_view_login = "click_start_view_login";
  static const String click_start_view_signup = "click_start_view_signup";

  static const String click_signup_login_view = "click_signup_login_view";

  // Carousel
  static const String carousel1 = "landing_page";
  static const String carousel2 = "carousel2";
  static const String carousel3 = "carousel3";
  static const String carousel4 = "carousel4";
  static const String carousel5 = "carousel5";

  // Main applicaton view
  static const String dashboard = "home_view";

  // Referral order
  static const String referral_code_applied = "referral_code_applied_success";
  static const String click_referral_know_more = "click_referral_know_more";
  static const String click_referral_code_share = "click_referral_code_share";
  static const String click_referral_code_copy = "click_referral_code_copy";
  static const String click_referral_sync_contact = "click_referral_sync_contact";
  static const String click_referral_refer = "click_referral_refer";

  // Profile view
  static const String click_home_profile_card = "home_profile_card_click";
  static const String click_profile_view = "profile_view";
  static const String profile_user_contact_day_change = "profile_user_contact_day_change";
  static const String profile_user_contact_time_change = "profile_user_contact_time_change";

  static const String click_profile_personal_details = "personal_details_view";
  static const String click_profile_health_information = "health_view";
  static const String click_profile_insurance = "insurance_view";
  static const String click_profile_health_card = "click_profile_health_card";
  static const String click_profile_address = "address_list_view";
  // Main appbar view
  static const String click_call_icon = "call_click";

  static const String click_profile_payment = "credit_card_list_view";
  static const String click_conformation_call = "call_confirm";
  static const String click_chat_icon = "chat_view";
  static const String click_about_us = "about_us_view";
  static const String click_call_chat = "click_call_chat";

  // Click on order
  static const String click_orders = "orders_view";
  static const String click_orders_detail = "orders_detail";

  // Click on prescription
  static const String click_prescriptions = "prescriptions_view";
  static const String click_prescriptions_details = "prescriptions_detail";

  // Click on search
  static const String click_searchbox = "search_view";
  static const String show_search_result = "search_result_view";
  static const String search_breakup_click = "search_breakup_click";

  // Member
  static const String click_member = "member_list";
  static const String click_member_item = "member_select";

  // New Signup flow
  static const String edit_phone_click = "edit_phone_click";
  static const String edit_email_click = "edit_email_click";
  static const String transfer_name_entered = "au_transfer_name_entered";
  static const String transfer_search_name_entered = "au_transfer_search_name_entered";

  static const String verification_view = "verification_view";
  static const String transfer_phone_entered = "au_transfer_phone_entered";
  static const String transfer_address_entered = "au_transfer_address_entered";
  static const String transfer_comment_entered = "au_transfer_comment_entered";
  static const String transfer_all_medication_entered = "au_transfer_all_medication_entered";
  static const String click_dont_know_pharmacy = "click_dont_know_pharmacy";

  static const String phone_entered = "phone_entered";
  static const String email_entered = "email_entered";
  static const String verify_phone_entered = "verify_phone_entered";
  static const String verify_email_entered = "verify_email_entered";
  static const String password_entered = "password_entered";

  static const String landing_forgot = "landing_forgot";
  static const String landing_login = "landing_login";
  static const String landing_signup = "landing_signup";
  static const String landing_proceed = "landing_proceed";
  static const String login_google = "login_google";
  static const String login_apple = "login_apple";

  static const String au_firstname_entered = "au_firstName_entered";
  static const String au_lastname_entered = "au_lastName_entered";
  static const String au_dob_month_entered = "au_dob_month_entered";
  static const String au_dob_date_entered = "au_dob_date_entered";
  static const String au_dob_year_entered = "au_dob_year_entered";
  static const String au_gender_entered = "au_gender_entered";
  static const String au_email_entered = "au_email_entered";
  static const String au_otp_entered = "au_otp_entered";
  static const String au_newpassword_entered = "au_newpassword_entered";
  static const String au_referralcode_entered = "au_referralCode_entered";

  static const String au_province_entered = "au_province_entered";
  static const String au_iscaregiver_entered = "au_isCaregiver_entered";
  static const String au_dailymed_entered = "au_dailyMed_entered";
  static const String au_packpermission_entered = "au_packpermission_entered";

  //UNKNOWN EVENT
  static const String new_user_health = "new_user_health";
  static const String new_user_insurance = "new_user_insurance";
  static const String new_user_payment = "new_user_payment";
  static const String new_user_address = "new_user_address";
  static const String new_user_const = "new_user_const";
  static const String new_user_intermediate = "new_user_intermediate";

  //Vitmains
  static const String click_vitamins_pack = "click_vitamins_pack";
  static const String click_vitamins_with_expert = "click_vitamins_with_expert";
  static const String click_vitamins_own_pack = "click_vitamins_own_pack";
  static const String click_vitamins_manage_subscription = "click_vitamins_manage_subscription";
  static const String click_cancel_subscription = "click_cancel_subscription";
  static const String click_call_cancel_subscription = "click_call_cancel_subscription";
  static const String click_modify_subscription = "click_modify_subscription";

  static const String click_vitamins_with_expert_call = "click_vitamins_with_expert_call";
  static const String click_vitamins_with_expert_chat = "click_vitamins_with_expert_chat";

  static const String click_add_to_cart = "click_add_to_cart";
  static const String click_remove_from_cart = "click_remove_from_cart";

  static const String click_item_details = "click_item_details";

  static const String click_vitamins_catalog_back = "click_vitamins_catalog_bottom_back";
  static const String Transfer_search_popular_selected = "Transfer_search_popular_selected";

  static const String click_review_cart_edit_dosage = "click_review_cart_edit_dosage";
  static const String click_review_cart_apply_coupon = "click_review_cart_apply_coupon";
  static const String click_review_cart_remove_coupon = "click_review_cart_remove_coupon";

  static const String click_review_cart_review = "click_review_cart_review";
  static const String click_review_cart_review_back = "click_review_cart_review_back";
  static const String click_review_cart_address = "click_review_address";
  static const String click_review_cart_address_back = "click_review_address_back";
  static const String click_review_cart_payment = "click_review_cart_payment";
  static const String click_review_cart_payment_back = "click_review_cart_payment_back";

  static const String click_review_cart_complete = "click_review_cart_complete";
  static const String click_review_cart_complete_error = "click_review_cart_complete_error";

  static const String click_vitmains_order_success_view_order = "click_vitmains_order_success_view_order";
  static const String click_vitmains_order_success_home = "click_vitmains_order_success_home";
  static const String click_vitmains_order_success_call = "click_vitmains_order_success_call";
  static const String click_vitmains_order_success_chat = "click_vitmains_order_success_chat";

  static const String click_add_vitamin_category = "click_add_vitamin_category";
  static const String click_remove_vitamin_category = "click_remove_vitamin_category";
  static const String click_show_all_vitamins = "click_show_all_vitamins";
  static const String click_show_filter_vitamins = "click_show_filter_vitamins";

  static const String account_employer_list = "au_employer_list";
  static const String account_employer_entered = "au_employer_entered";
  static const String account_employer_submit = "au_employer_submit";
  static const String account_employer_skip = "au_employer_skip";
  static const String account_employer_onboard_success = "au_employer_onboard_success";
  static const String google_success = "google_success";
  static const String landing_proceed_google = "landing_proceed_google";
  static const String landing_proceed_apple = "landing_proceed_apple";
  static const String apple_success = "apple_success";

  static const String click_copy_insurance = "click_copy_insurance";
  static const String click_copy_address = "click_copy_address";

  static const String click_profile_employer_updated = "click_profile_employer_updated";

  static const String transfer_final = "transfer_final";
  static const String refill_final = "refill_final";
  static const String vitamins_final = "vitamins_final";
  static const String upload_final = "upload_final";
  static const String new_user_final = "new_user_final";
  static const String telehealth_final = "telehealth_final";

  static const String click_zero_copay = "click_zero_copay";
  static const String click_zero_copay_continue = "click_zero_copay_continue";
  static const String click_zero_copay_cancel = "click_zero_copay_cancel";

  static const String click_add_a_card = "click_add_a_card";
  static const String click_add_an_address = "click_add_an_address";
  static const String click_update_application = "click_update_application";
  static const String show_ontario_discount_text = "show_ontario_discount_text";

  static const String click_rate_us_not_now = "click_rate_us_not_now";
  static const String click_rate_us_yes = "click_rate_us_yes";

  static const String base_dashboard = "dashboard_";

  static const String notification_received = "notification_received";
  static const String notification_open = "notification_open";
  static const String inapp_message_clicked = "inapp_message_clicked";

  //Pill reminder
  static const String click_pill_taken = "click_pill_taken";
  static const String click_pill_missed = "inapp_message_clicked";
  static const String pill_pack_medication_view = "pill_pack_medication_view";
  static const String pill_pack_calender_view = "pill_pack_calender_view";
  static const String click_show_daily_medication = "click_show_daily_medication";
  static const String click_show_calender_medication = "click_show_calender_medication";

  //Telehealth preference
  static const String telehealth_preference = "telehealth_preference";

  //Lead Conversion Event
  static const String lead_event = "lead_event";
}
