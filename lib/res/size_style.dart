
  /// Size style small
  const double SMALL = 2.0;
  const double SMALL_X = 4.0;
  const double SMALL_XX = 6.0;
  const double SMALL_XXX = 8.0;

  /// Size style medium
  const double MEDIUM = 10.0;
  const double MEDIUM_X = 12.0;
  const double MEDIUM_XX = 14.0;
  const double MEDIUM_XXX = 16.0;

  /// Size style regular
  const double REGULAR = 18.0;
  const double REGULAR_X = 20.0;
  const double REGULAR_XX = 22.0;
  const double REGULAR_XXX = 24.0;

  /// Size style large
  const double LARGE = 28.0;
  const double LARGE_X = 32.0;
  const double LARGE_XX = 48.0;
  const double LARGE_XXX = 64.0;

  // Text line height
  const double TEXT_LINE_HEIGHT = 1.4;
